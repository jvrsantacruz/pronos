#!/usr/bin/python
import os
import re
import shutil
import argparse
import collections

Entry = collections.namedtuple('Entry', ['n', 'name', 'path'])

class Entry(object):
    def __init__(self, n, name, path):
        self.n = n
        self.name = name
        self.path = path

    @classmethod
    def from_dir(cls, path):
        paths = (os.path.join(path, name) for name in os.listdir(path))
        entries = (cls.from_path(path) for path in paths)
        return [entry for entry in entries if entry]

    @classmethod
    def from_path(cls, path):
        m = match_file(os.path.basename(path))
        if m:
            return cls(int(m.group('n')), m.group('name'), path)

    def format(self, n, name):
        return '{:02d}_{}.md'.format(n, name)

    def renumbered(self, plus=1):
        next = self.n + plus
        dirname = os.path.dirname(self.path)
        filename = self.format(next, self.name)
        return Entry(next, self.name, os.path.join(dirname, filename))

    def touch(self):
        if not os.path.exists(self.path):
            with open(self.path, 'w'):
                pass


def match_file(name):
    return re.match('^(?P<n>\d+)_(?P<name>[a-zA-Z0-9.-_]+)\.md$', name)


def entry_or_error(path):
    entry = Entry.from_path(path)
    if not entry:
        raise Exception('{} is not in number_name.md format'.format(path))
    return entry


def renumber(n, entries, plus=1):
    for entry in (e for e in entries if e.n >= n):
        renum = entry.renumbered(plus)
        shutil.move(entry.path, renum.path)


def add_file(args):
    entry = entry_or_error(args.name)
    entries = Entry.from_dir(os.path.dirname(entry.path))
    renumber(entry.n, entries)
    entry.touch()


def rm_file(args):
    entry = entry_or_error(args.name)
    os.unlink(entry.path)
    entries = Entry.from_dir(os.path.dirname(entry.path))
    renumber(entry.n, entries, -1)


def rename_file(args):
    entry = entry_or_error(args.name)
    if not os.path.isabs(args.dest):
        args.dest = os.path.join(os.path.dirname(args.name), args.dest)
    dest = entry_or_error(args.dest)
    import pdb; pdb.set_trace()
    backup = entry.path + '.bk'

    shutil.move(entry.path, backup)

    # Logical removal of current entry
    entries = Entry.from_dir(os.path.dirname(entry.path))
    renumber(entry.n, entries, -1)

    # Logical insertion of new entry
    entries = Entry.from_dir(os.path.dirname(dest.path))
    renumber(dest.n, entries)
    shutil.move(backup, dest.path)


def main(args):
    actions = {'add': add_file, 'rm': rm_file, 'rename': rename_file}
    actions[args.action](args)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    subparser = parser.add_subparsers()

    parser_add = subparser.add_parser('add')
    parser_add.set_defaults(action='add')
    parser_add.add_argument('name')

    parser_rm = subparser.add_parser('rm')
    parser_rm.set_defaults(action='rm')
    parser_rm.add_argument('name')

    parser_rename = subparser.add_parser('rename')
    parser_rename.set_defaults(action='rename')
    parser_rename.add_argument('name')
    parser_rename.add_argument('dest')

    args = parser.parse_args()
    main(args)
