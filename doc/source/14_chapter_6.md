# Análisis y Diseño

## Requisitos

Esta sección recoge los requisitos funcionales y no funcionales obtenidos durante la especificación
del sistema a desarrollar. Algunos requisitos (como el análisis de partidos futuros) fueron
añadidos en iteraciones posteriores, tras realizar progresos durante el análisis.

### Requisitos funcionales

Los requisitos funcionales de la aplicación establecen el comportamiento interno del sistema,
definiendo qué debe hacer exactamente. Los casos de uso, desarrollados en una sección posterior,
definen la funcionalidad necesaria para el cumplimiento de estos requisitos. Cada uno de ellos
incluye el nombre que lo identifica y un texto explicativo que resume su contenido.

--------------------------------------------------------------------------------------------
Nombre                   Resumen
-------------------      -------------------------------------------------------------------
**Obtención datos        Mecanismo a partir del cual obtener datos históricos sobre eventos,
históricos**             torneos, partidos y jugadores de tenis.

**Base de datos          Persistencia y almacenamiento del archivo histórico de manera que
histórica**              sea posible ser actualizada y consultada posteriormente.

**Modelo predictivo**    Generación de un modelo predictivo a partir de los datos existentes
                         en la base de conocimiento con el archivo histórico.

**Reutilización modelo   El modelo debe poder emplearse posteriormente sin necesidad de generarlo
predictivo**             de nuevo

**Comparación de         Debe permitirse la comparación de modelos predictivos entre si.
modelos predictivos**

**Predicción de          Predicción sobre partidos aún no jugados.
partidos futuros**

**Análisis de datos**    Permitir obtener información sobre la base de datos histórica

--------------------------------------------------------------------------------------------

Tabla: \label{table-functional-requirements} Lista de requisitos funcionales.

La *obtención de datos históricos* implica la construcción de un sistema que obtenga esos datos, bien
de alguna base de datos existente, o extrayendo estos de algún servicio que los ofrezca. En
cualquier caso, el sistema resultante debe obtener acceso a esos datos, aún en formatos diversos y
no relacionados.

La *base de datos histórica* implica la persistencia y organización de estos datos en una fuente
única que sea posible consultar y actualizar.

La generación del *modelo predictivo* debe ser posible para el usuario a partir de la base de
conocimiento disponible. Esto debe permitir al usuario generar uno o más modelos. Estos deben ser
*comparables* entre si, de manera que sea posible elegir el mejor entre ellos. Una vez generados,
estos *modelos deben ser reutilizables* y generalizar suficientemente bien como para realizar
predicciones sobre múltiples entradas, y no únicamente sobre la base de conocimiento sobre la que
se construyó. Esto alude a la *predicción de partidos futuros* que aún no han sido realizados y
para los que se tienen todos los datos a excepción del resultado.

### Requisitos no funcionales

Requisitos no funcionales de la aplicación. Determinan el comportamiento específico de la
aplicación en determinados aspectos y aportan indicaciones sobre la utilización del sistema sin
definir funcionalidades concretas.

--------------------------------------------------------------------------------------------
Nombre                   Resumen
-------------------      -------------------------------------------------------------------
**Registros              La base de conocimiento debe contener únicamente registros completos
completos**              a partir de los cuales pueda realizarse el análisis.

**Respetar               Empleo de buenas prácticas a la hora de extraer los datos de servicios
servicios externos**     externos, de manera que no se altere su funcionamiento ni provoque
                         inconvenienes al mismo.

**Datos                  Almacenar la base de conocimiento y todos sus datos derivados de manera
estructurados**          estructurada, siguiendo un esquema fijo, de manera que sea posible su
                         consulta y tratamiento desde otras herramientas sin necesidad de código
                         específico.

**Base de datos          Construir el sistema de manera que la base de conocimiento sea actualizable
actualizable**           con los últimos datos de manera sencilla.

**Servicio               Realizar la arquitectura de manera que admita la distribución del servicio
distribuido**            entre diferentes máquinas de ser necesario.

**Generación modelo      Permitir la generación del modelo estadístico de manera paramétrica según
bajo demanda**           los requerimientos del usuario.

--------------------------------------------------------------------------------------------

Tabla: \label{table-non-functional-requirements} Tabla de requisitos no funcionales.

A la hora de realizar la *obtención de datos históricos*, habilidad que se encuentra registrada
como requisito funcional, es preciso establecer el modo. Para ello los datos obtenidos deben quedar
almacenados en una *base de datos* de manera *estructurada* conteniendo únicamente *registros
completos* que puedan emplearse para realizar el análisis. Estos datos deben poder ser *actualizados*
y complementados en cualquier momento.

El sistema final debe ser *respetuoso con los servicios* a los que se conecta, evitando
provocar una sobrecarga, caída del servidor o una carga que resulte en un perjuicio a los
mantenedores del sistema remoto.

La arquitectura del sistema debe ser construida de tal manera que la carga de trabajo pueda ser
*distribuida* entre múltiples servidores actualizando únicamente la tecnología pero sin alterar el
diseño actual del sistema.


## Análisis del sistema

### Casos de uso

Entendemos un caso de uso como una lista de acciones y pasos que definen las interacciones entre un
usuario o actor y el sistema que implementamos, con la intención de conseguir algún objetivo.

Estos casos recogen la funcionalidad básica a implementar y definen las acciones que debe ser capaz
de realizar la aplicación con el fin de cumplir con los requisitos del sistema. En esta sección
podremos encontrar la lista completa de casos de uso, sus descripciones y el diagrama general
(ver figura \ref{use-cases-diagram}).

La lista de casos de uso a implementar:

- Obtener histórico
- Generar análisis estadístico
- Entrenar modelo predictivo
- Predecir nuevos partidos
- Incorporar nuevos resultados

![Diagrama de casos de uso. Presenta la funcionalidad disponible y la relación de los usuarios del sistema con la misma. En el se distingue el usuario del sistema y actividades iniciadas periódicamente por el sistema \label{use-cases-diagram}](source/figures/use-cases-diagram.pdf){ width=100% }

A continuación se listarán y describirán detalladamente los casos de uso mencionados en el diagrama
de casos de uso de la figura \ref{use-cases-diagram}, que muestra los casos de uso a implementar
como parte del sistema *Pronos*.

#### Caso de uso: Obtener histórico

**Descripción**: Se inicia la recopilación de datos desde uno o más servicios externos.   
**Actor principal**: *Usuario*    
**Precondiciones**: Ninguna   

**Flujo principal**:

1. El *Usuario* ejecuta el comando de recopilación de datos.
1. El *Usuario* indica qué servicios van a consultarse.
1. El *Sistema* toma los nombres de servicio y los valida.
1. El *Usuario* indica cuántos *scrapers* van a emplearse para cada servicio.
1. El *Sistema* toma los scrapers y valida su número.
1. El *Usuario* indica la *ruta* al fichero de salida para la base de datos.
1. El *Sistema* toma la *ruta* y la valida.
1. El *Sistema* crea las colas de comunicación entre los *crawlers* y el sistema.
1. El *Sistema* inicializa los *crawlers* e inicia la comunicación por red.
1. El *Sistema* muestra los registros obtenidos y los almacena en el fichero en la *ruta*.

**Postcondiciones**: Una nueva base de datos ha sido creada en *ruta* conteniendo los datos de
archivo histórico recopilados por los *crawlers*.

**Flujo alternativo**: Nombre de servicio inválido  
**Descripción**: El valor provisto por el usuario al elegir los nombres de servicio es inválido.  
**Precondiciones**:  

1. El nombre de servicio no existe
2. El flujo alternativo comienza en el paso `2` del flujo principal.
3. El *Sistema* indica al usuario que el servicio no tiene un *spider* asociado disponible.

**Postcondiciones**: 

El *Sistema* aborta la ejecución del programa y no se inicia la recopilación de datos.

**Flujo alternativo**: No se indica ningún *scraper*  
**Descripción**: No se especifican servicios de los que obtener datos  
**Precondiciones**:  

1. El usuario no introduce cuántos *scrapers* van a emplearse.
2. El flujo alternativo comienza en el paso  `4` del flujo principal.
3. El *Sistema* inicializa solo el *MainSpider*.
3. El *Sistema* no crea las colas para comunicación interna.

#### Caso de uso: Actualizar histórico

**Descripción**: Se inicia la recopilación de datos desde uno o más servicios externos.   
**Actor principal**: *Usuario*    
**Precondiciones**: El flujo extiende el caso de uso *Obtener histórico* a partir del paso 7 de su
flujo principal  

**Flujo principal**:

1. El *Sistema* toma la *ruta* y comprueba que es una base de datos existente.
1. El *Sistema* crea las colas de comunicación entre los *crawlers* y el sistema.
1. El *Sistema* inicializa los *crawlers* e inicia la comunicación por red.
1. El *Sistema* muestra los registros obtenidos y los almacena en el fichero en la *ruta*.

**Postcondiciones**:

Existe una base de datos en **ruta** que contiene los datos actualizados.

**Flujo alternativo**: Ruta a base de conocimiento inválida  
**Descripción**: El valor provisto por el usuario como base de conocimiento es inválido  
**Precondiciones**:  

1. La base de datos no existe o tiene un formato desconocido.
2. El flujo alternativo comienza en el paso `3` del flujo principal.
3. El *Sistema* indica al usuario que *ruta* no es una base de datos válida.

#### Caso de uso: Generar análisis estadístico

**Descripción**: Se realiza un análisis sobre todas las variables contenidas en la base de datos   
**Actor principal**: *Usuario*   
**Precondiciones**: Ninguna   

**Flujo principal**:

1. El *Usuario* ejecuta el comando de generación de análisis.
1. El *Usuario* introduce una *ruta* a un directorio donde guardar los resultados.
1. El *Sistema* obtiene los datos y ejecuta el análisis.
1. El *Sistema* almacena las imágenes en el directorio *ruta*

**Postcondiciones**: Una nueva base de datos ha sido creada en *ruta* conteniendo los datos de
archivo histórico recopilados por los *crawlers*.

**Flujo alternativo**: Ruta a directorio de reporte inválida  
**Descripción**: El valor provisto por el usuario como directorio para el reporte es inválido  
**Precondiciones**:  

2. El flujo alternativo comienza en el paso `2` del flujo principal.
3. El *Sistema* indica al usuario que *ruta* no es un directorio válido

#### Caso de uso: Generar modelo predictivo

**Descripción**: Se genera un modelo estadístico que permite predecir partidos  
**Actor principal**: *Usuario*   
**Precondiciones**: Ninguna   

**Flujo principal**:

1. El *Usuario* ejecuta el comando de generación de modelo.
1. El *Usuario* indica el *nombre* de los modelos va a entrenar.
1. El *Usuario* indica la *ruta* a la base de conocimiento a emplear.
1. El *Sistema* genera un set de entrenamiento y de prueba para cada algoritmo.
1. El *Sistema* crea un modelo entrenado empleando cada algoritmo.
1. El *Sistema* muestra las características de eficiencia y resultados de cada modelo.
1. El *Sistema* persiste el modelo en la *ruta* proporcionada.

**Postcondiciones**: Un nuevo modelo ha sido generado y se persiste en la ruta proporcionada.

**Flujo alternativo**: Nombre de algoritmo para el modelo inválido  
**Descripción**: El valor provisto por el usuario como algoritmo de predicción no existe  
**Precondiciones**:  

2. El flujo alternativo comienza en el paso `2` del flujo principal.
3. El *Sistema* indica al usuario que *nombre* no es un algoritmo de predicción válido

**Flujo alternativo**: Ruta a base de conocimiento inválida  
**Descripción**: El valor provisto por el usuario como base de conocimiento es inválido  
**Precondiciones**:  

1. La base de datos no existe o tiene un formato desconocido.
2. El flujo alternativo comienza en el paso `3` del flujo principal.
3. El *Sistema* indica al usuario que *ruta* no es una base de datos válida.


#### Caso de uso: Predecir nuevos partidos

**Descripción**: Se calcula el resultado probable para partidos aún no jugados  
**Actor principal**: *Usuario*   
**Precondiciones**: Existe una lista de partidos no jugados con sus datos  

**Flujo principal**:

1. El *Usuario* ejecuta el comando de predicción de partidos no jugados.
1. El *Usuario* proporciona la *ruta* a la base de datos que contiene los partidos.
1. El *Usuario* proporciona la *ruta* al modelo predictivo a emplear para realizar la predicción.
1. El *Sistema* muestra la predicción obtenida para cada partido disponible.

**Flujo alternativo**: Ruta a base de conocimiento inválida  
**Descripción**: El valor provisto por el usuario como base de conocimiento es inválido  
**Precondiciones**:  

1. La base de datos no existe o tiene un formato desconocido.
2. El flujo alternativo comienza en el paso `2` del flujo principal.
3. El *Sistema* indica al usuario que *ruta* no es una base de datos válida.

**Postcondiciones**: 

El *Sistema* aborta la ejecución del programa.

**Flujo alternativo**: Ruta a modelo predictivo inválido  
**Descripción**: El valor provisto por el usuario como modelo predictivo es inválido  
**Precondiciones**:  

1. El modelo predictivo no existe o tiene un formato desconocido.
2. El flujo alternativo comienza en el paso `4` del flujo principal.
3. El *Sistema* indica al usuario que *ruta* no es un modelo predictivo válida.

**Postcondiciones**: 

El *Sistema* aborta la ejecución del programa.

#### Caso de uso: Incorporar nuevos resultados

**Descripción**: Se actualiza la base de datos de partidos no jugados con resultados  
**Actor principal**: *Usuario*   
**Precondiciones**: Ninguna   

**Flujo principal**:

1. El *Usuario* ejecuta el comando de obtención de resultados.
1. El *Usuario* indica la *ruta* a la base de conocimiento a emplear.
1. El *Sistema* lanza un *crawler* específico para buscar resultados de estos partidos.

**Postcondiciones**: La base de datos de conocimiento actualiza la lista de partidos no jugados e
incorpora los nuevos resultados.

**Flujo alternativo**: Ruta a base de conocimiento inválida 
**Descripción**: El valor provisto por el usuario como base de conocimiento es inválido  
**Precondiciones**:  

1. La base de datos no existe o tiene un formato desconocido.
2. El flujo alternativo comienza en el paso `2` del flujo principal.
3. El *Sistema* indica al usuario que *ruta* no es una base de datos válida.

**Postcondiciones**: 

El *Sistema* aborta la ejecución del programa.

### Modelo de datos

Tras la exploración de las diversas fuentes de datos, nos fue posible obtener una idea general de
qué información se encontraba disponible y qué ofrecían exactamente los distintos servicios.

Inicialmente se seleccionaron los datos que la literatura indicaba que habían sido relevantes en
otros intentos de predicción. Adicionalmente se incluyeron unos más, algunos con propósito
identificativo, es decir, para diferenciar un partido de otro, y el resto con vistas a comprobar en
el mismo estudio si efectivamente no aportaban nada a la predicción.

![Modelo de datos para la base de conocimiento. En el pueden verse las principales entidades distinguidas, sus atributos y relaciones. \label{data-model-diagram}](source/figures/data-model-diagram.pdf){ width=100% }

En la figura \ref{data-model-diagram} podemos ver las diferentes entidades que componen la base de
conocimiento:

- *Event*: Representación de un torneo, competición o evento deportivo de alguna clase.
- *Match*: Partido o encuentro que se realiza como parte de una competicion.
- *Player*: Jugador o participante en un partido.
- *Result*: Resultado de un partido que implica a jugadores.

A partir de estas clases, es posible modelar los resultados de las competiciones de tenis. Se
considera este modelo lo suficientemente flexible como para poder ser adaptado sin demasiada
dificultad a juegos más generales, simplemente aumentando el número de jugadores disponibles.

El modelo de datos de la figura \ref{data-model-diagram} presenta las siguientes restricciones y
aclaraciones, que se han omitido de su representación en UML, bien por simplificar el diagrama, o
porque el lenguaje no lo permite:

2. Un *Event* debe tener al menos el campo `prize`.
2. Un *Event* debe tener al menos el campo `surface`.
2. Un *Event* puede no tener ningún *Match* asociado.
1. Un *Match* debe tener exactamente dos jugadores siempre.
2. Un *Match* puede no tener ningún resultado.
2. Un *Match* si tiene, tiene un único resultado.
2. Un *Match* siempre se encuentra asociado a un *Event*.
2. Un *Match* debe tener al menos el campo `round`.
2. Un *Player* debe tener un nombre único.
2. Un *Player* debe tener al menos el campo `gender`.
2. Un *Player* debe tener al menos el campo `ranking`.
2. Un *Player* debe tener al menos el campo `born`.

### Diagramas de secuencia

Las operaciones más complejas merecen desgranar su secuencia de operaciones, con el objetivo de
aclarar su funcionamiento, descrito con mayor nivel de detalle en el capítulo de `Implementación`.

![Diagrama de secuencia para la operación de obtención de archivo histórico.  \label{scraping-sequence-diagram}](source/figures/scraping-sequence-diagram.pdf){ width=100% }

En el diagrama de secuencia de la figura \ref{scraping-sequence-diagram} podemos observar una
versión simplificada del comportamiento de la operación más compleja: la de obtención de archivo
histórico a partir de fuentes externas.

En el diagrama podemos observar como el usuario inicia el proceso indicándole a la operación que
comience con la obtención de datos. Para esto se crean uno o más objetos *Crawler*, los cuales a su
vez mantienen una o más *Spider*, las cuales cada una se encuentra especializada en un sitio web.

Las *Spider* realizan peticiones HTTP a través de la red a los servidores web que albergan las
fuentes de datos de la que se quiere extraer la información. Cuando estas responden, estas mismas
arañas extraen y parsean la información, obteniendo una serie de objetos o *Entities*. Estos
objetos contienen los datos extraidos de las web consultadas y necesitan de un proceso de
homogenización y limpiado, que se realiza una vez que son enviadas al *Pipeline*. 

El sistema de *Pipeline* también se encarga de pasar las entidades ya limpias al *Updater*, el cual
se encarga de persistir toda la información de manera estructurada a través de la
*DatabaseSession*, que mantiene la conexión a la base de datos.

Las peticiones que realiza el *Spider* se repiten contínuamente hasta que se agote la base de datos
o el proceso se vea interrumpido. El sistema está diseñado de manera que a partir de una respuesta
por parte del servidor, se inician otras peticiones nuevas, realizando así un bucle de
actualización contínuo.
