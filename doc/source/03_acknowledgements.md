# Agradecimientos {.unnumbered}

<!-- This is for acknowledging all of the people who helped out -->

- A *Manuel Palomo Duarte* por el apoyo recibido durante toda mi carrera y su confiaza en mi trabajo.
- A *Íñigo* por las ideas, el entusiasmo y toda esa información sin la cual no podría haber empezado.
- A *Marta* por Madrid, París, Milán, Roma, Nueva York y todo, todo lo demás.
- A *Miguel y Margarita*, que se las arreglan para estar ahí siempre.
- Y en especial, a mi familia, que dejará de mirarme preocupada.

<!-- Use the \newpage command to force a new page -->

\newpage



