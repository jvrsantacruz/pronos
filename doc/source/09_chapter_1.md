# Introducción

La rápida evolución de las redes de comunicación, infraestructura, protocolos y estándares que dan
forma a Internet, nos permiten el acceso a grandes recursos de información disponible públicamente.
Tanto organizada en forma de repositorios o bases de datos como presentada para su consumo humano
en formatos web, estos datos pueden ser obtenidos, clasificados e integrados entre si. Es nuestro
trabajo el conseguir comprender lo que dicen, y a través de su estudio, detectar tendencias y
patrones presentes, permitiendo la creación de modelos que nos permitan observar las relaciones
entre los diferentes aspectos, su valor e importancia.

Esto supone nuevos retos en el tratamiento de esta información, en tanto que todos esos recursos
deben ser obtenidos, procesados, almacenados y analizados para poder extraer conclusiones y
*aprender de ellos*. Ingentes cantidades de datos son generadas en muchos campos de la sociedad, lo
que supone que todos los esfuerzos y avances realizados sobre los métodos que permiten su
procesamiento y posterior análisis pueden tener un impacto significativo.

En ese contexto, es por tanto el objetivo de este proyecto el desarrollar un sistema específico que
de una solución sencilla para un problema concreto en uno de estos campos, permitiendo aplicar las
técnicas disponibles para obtener esa información de manera distribuida y analizarla empleando
modelos estadísticos basados en el aprendizaje automático.

## Estructura del documento

Este documento se encuentra organizado en los siguientes capítulos y secciones:

TODO: Descripción de los capítulos

- **Introducción**: Resumen de la motivación, alcance y principales decisiones de este proyecto.
- **Fundamentos**: Descripción del objeto de estudio y sus características.
- **Predicción**: Introducción a los conceptos estadísticos empleados.
- **Planificación**: Sobre las labores de planificación, hitos del proyecto y estrategia empleada para el desarrollo.
- **Análisis y Diseño**: Especificación formal de la aplicación basada en UML 2.0. Establece y
  justifica el uso de diferentes recursos y describe la arquitectura general.
- **Implementación**: Detalla y justifica la elección de las diferentes tecnologías y describe la
  implementación técnica del proyecto. Incluye el proceso de desarrollo, las ténicas empleadas,
  problemas encontrados y otros detalles.
- **Conclusiones**: Breve resumen del estado del proyecto, logros conseguidos, experiencia personal
  y futuras líneas de trabajo.
- **Apéndices**: Incluyen información adicional

## Eventos deportivos

Los eventos deportivos generan grandes cantidades de información que se publica de forma continua.
Cada día se celebran en el mundo miles de eventos, torneos y partidos de toda clase de deportes.
Las competiciones deportivas profesionales tienen un impacto enorme en todo el mundo. Por ejemplo,
se estima que el evento de la *Super Bowl*, organizado anualmente por la  *National Football
League's (NFL)* en los Estados Unidos llega a más de 160 millones de personas, mientras que la
*Champions League* en Europa mejora ese número en más de 50 millones, sin contar que los cientos de
millones que ven la *Rugby World Cup*, la *Cricket World Cup* y la *NBA Finals*[^1.1] [@GamingWorld].

Este gran interés mundial por los deportes se traduce en un gran impacto económico de esta
actividad en las economías de estos países. En Europa, las ligas profesionales de fútbol nacionales
consiguen grandes beneficios; teniendo en cuenta solo los cinco principales equipos de cada país en
la temporada 2015/2016, estos obtuvieron cerca de 14.000 millones de euros[^1.2]
[@FootballReview2017] en un año.

Además de los beneficios directos o indirectos que proporcionan las competiciones deportivas,
existe precisamente un mercado millonario basado en el análisis sistemático de la información
disponible sobre estos eventos y la predicción de su resultado: el mercado de apuesta. La
facturación del mercado del juego a nivel mundial alcanzó los 283.000 millones de euros en 2012, y
desde entonces se estima que el volumen de apuestas se ha incrementado en un 45%.[^1.3] [@OnlineGamblingStudy].

Las casas de apuestas se especializan en la realización de predicciones sobre el resultado de estos
eventos. Para esto combinan conocimiento experto sobre el tipo de deporte que se estudia con la
recolección sistemática de datos y el uso de modelos predictivos construidos con el propósito de
valorar con una probabilidad las diferentes situaciones que pueden darse como resultado.

Dado que existe un campo que genera gran cantidad de información de dominio público, de interés
para gran parte de la población, que además mantiene asociado un creciente negocio millonario
basado en la interpretación de esos datos, se consideró como idóneo para el estudio de este tipo de
sistemas predictivos basados en grandes colecciones de datos.

## Aprendizaje Automático

El *Aprendizaje Estadístico* juega un papel clave en muchas áreas de la ciencia y la industria,
aportando a los campos de la Estadística, Inteligencia Artificial, Minería de Datos así como a
diferentes áreas de la ingeniería y otras disciplinas. Típicamente el sistema consiste en realizar
*predicciones* basadas en un conjunto de *propiedades* que tienen los sujetos de estudio; aparte se
cuenta con un conjunto de datos de *entrenamiento*, a partir del cual se construye un modelo
predictivo que permite predecir el resultado de sujetos desconocidos [@Elements].

Problemas clásicos para los que el aprendizaje automático se encuentra especialmente capacitado
incluyen la detección de *spam*, el reconocimiento de texto manuscrito o el diagnóstico de
enfermedades a partir de analíticas o imágenes de resonancia. Todos estos problemas se basan en la
presentación de muchos casos conocidos al modelo predictivo (*entrenamiento*) de manera que sea
capaz de reconocer, con un margen de error limitado, otros casos parecidos que no ha visto antes.

Las características de estos modelos los hacen especialmente aptos para abordar el reto de predecir
resultados de eventos aún no sucedidos, de los cuales se tiene poca información en el momento pero
del que se cuenta con un amplio histórico. Es por tanto en esta familia de la estadística en la que
nos enfocaremos a la hora de realizar el análisis de los datos obtenidos de competiciones y
eventos.

## Objetivos

Con el desarrollo de este proyecto, comprendiendo la obtención de datos, su análisis y procesado se
pretende:

1. **Evaluar datos públicos**: Como hemos mencionado anteriormente, se generan y publican gran
   cantidad de datos relacionados con las actividades deportivas, sin embargo, inicialmente no
   estamos seguros de su calidad, fiabilidad y completitud. Es por tanto nuestro objetivo el
   construir una base de datos de datos con un mínimo de calidad sobre los que poder trabajar.

1. **Plantear un modelo predictivo**: ¿Es posible obtener información sobre eventos aún no
   realizados? ¿Predecir el resultado de las competiciones? ¿Con qué margen de error? Creación de
   un modelo estadístico que aprenda a partir de los datos obtenidos.

1. **Sistema continuo**: Evitar limitar la obtención de datos y el análisis a una única ocasión,
   sino que el proyecto debe estar preparado para funcionar de manera indefinida, obteniendo nuevos
   datos y proveyendo predicciones para los eventos próximos, mejorando el modelo.

1. **Multipropósito**: El sistema resultante debe encontrarse preparado para trabajar con más de un
   deporte, o debe ser sencillo ampliarlo para que lo haga sin necesidad de modificar la
   arquitectura de la aplicación.

Es por tanto la intención el realizar una herramienta de posible utilidad, a la que le sea posible
seguir funcionando sin intervención, más que un simple análisis puntual o la creación de un modelo
que carezca de uso.

## Alcance

Es enorme la cantidad de diferentes deportes que se practican profesionalmente, cada uno con sus
múltiples y distintas competiciones, normativas y reglas, en ocasiones cambiantes, diferentes
mecánicas, datos asociados, y eventos que se repiten en todo el mundo de manera regular y regulada,
o únicamente en momentos puntuales del año.

A pesar de plantear un sistema multipropósito que cubra diferentes deportes y eventos, esto no es
posible dadas las restricciones de tiempo y trabajo impuestas sobre este proyecto. Es necesario,
por tanto, elegir. Realizar una selección de qué deporte o deportes son los más asequibles para
poder realizar un análisis sencillo, dadas sus propiedades, reglas, datos de calidad disponibles y
frecuencia de competición.

### Deportes

A este respecto fueron evaluaron los siguientes deportes, por ser muy populares y contar con un
número alto de seguidores, tanto en España, como en Europa y el resto del mundo:

- Fútbol
- Baloncesto
- Tenis
- Ciclismo

Cada uno de estos deportes sigue un conjunto reglas bien definidas, mantenidas por federaciones o
asociaciones que gestionan la práctica del deporte y organizan las competiciones, las cuales son
atendidas por los medios de comunicación y su información ampliamente disponible en múltiples
medios.

### Fútbol

En el caso del *fútbol*, se trata del deporte más practicado en nuestro país y en Europa, regulado
a nivel mundial por la [@FIFA], europeo por la [@UEFA] y a nivel nacional por la [@RFEF], cuyas
federaciones mantienen listados de equipos y jugadores, e imponen una serie de políticas y
condiciones.

El fútbol se practica con equipos de 11 jugadores en partidos de 90 minutos, permitiendo su cambio
de acuerdo a ciertas normas. Los jugadores pueden ser expulsados sin reemplazo posible. Los equipos
van evolucionando históricamente en base a nuevos fichajes, cesiones y jubilaciones. Existe la
figura de entrenador en el equipo que influye en el modo de juego y los resultados[^1.4]
[@FCoachPerf].

Los partidos pueden terminar en tres posibles estados[^1.5] o bien gana el primer equipo, o gana el
segundo, o terminan en empate.

Es un deporte que mantiene equipos estables, tanto en competiciones nacionales como
internacionales, salvo ciertos eventos como la Copa del Mundo [@Worldcup], organizada por la
[@FIFA], o la Eurocopa [@Eurocup] organizada por la [@UEFA], en las cuales se forman selecciones
nacionales con equipos temporales.

### Baloncesto

El *baloncesto* es un deporte de equipo, muy popular especialmente en Estados Unidos y Europa. Se
encuentra regulado por la autoridad deportiva de la [@FIBA] a nivel mundial, y a nivel nacional, en
España, por la [@FEB], siendo especialmente famosa la liga nacional de Estados Unidos [@NBA].

El baloncesto incluye 5 jugadores por equipo, permitiendo su cambio. Los equipos adquieren y
pierden jugadores a lo largo del tiempo, pero se mantienen razonablemente estables. Existe la
figura de entrenador en el equipo.

Los partidos se juegan en cuatro períodos de diez[^1.6] o doce[^1.7] minutos, y solo pueden
terminar con la victoria de uno de los dos equipos, prorrogando el partido hasta que uno de los dos
equipos se adelante[^1.8].

Es un deporte que también mantiene equipos estables, en competiciones nacionales e internacionales,
y también crea selecciones nacionales para eventos deportivos como la Copa del Mundo de Baloncesto
[@BasketWorldcup].

### Tenis

El *tenis* es un deporte que se juega individualmente, o por parejas, muy extendido en la mayoría
de los países, incluso fuera de Europa y Estados Unidos. Se encuentra regulado por la Federación
Internacional de Tenis [@ITF], la cual organiza periódicamente multitud de competiciones en
distintas categorías con mucha frecuencia por todo el mundo.

El tenis, en su modalidad individual, más sencilla, enfrenta a dos jugadores que juegan partidos
acumulando puntos hasta que uno de los dos contrincantes alcanza una diferencia suficiente como
para declararlo ganador. Los jugadores mantienen una carrera estable a lo largo de toda su vida,
jugando bajo su nombre y no incluyéndose en equipos. Existe la figura del entrenador, pero esta
tiene poca relevancia[^1.9].

Los resultados del tenis son dos, gana el primer jugador o el segundo[^1.10] [@TennisRules].
Resultando sencillo determinar la carrera de un jugador en base a su tasa de éxitos. Cada jugador
tiene uno o más clasificaciones según su puntuación asociada. Estos *rankings* evolucionan a lo largo del
tiempo según los partidos que vayan ganando/perdiendo y permiten realizar comparaciones entre
diferentes jugadores en un punto dado.

Las competiciones de tenis se dividen en varias ligas o circuitos, contando con la liga [@ITF], en
categorías Junior, Senior, Playa y Silla de Ruedas, la liga [@ATP] que aglutina multitud de torneos
específicos como *Wimbledon* o el *US Open* en un único circuito.

### Ciclismo

El *ciclismo* de competición en ruta sobre asfalto es un deporte individual y por equipos. Las
competiciones unen conjuntos de ciclistas bajo el mismo equipo, pero que compiten individualmente.
Se encuentra regulado por la Unión de Ciclismo Internacional [@UCI], la cual ofrece licencias a los
corredores y gestiona las reglas, incluyendo sanciones disciplinarias.

La principal competición de ciclismo es el Campeonato Mundial de Ciclismo en Ruta, aunque las
competiciones nacionales cuentan con una gran popularidad; especialmente el Tour de France a nivel
mundial, el Giro de Italia y la Vuelta a España.

Los participantes en estas carreras corren en diferentes modalidades, individualmente en
contrarreloj o en persecución, en solitario y por equipos. El resultado de una carrera se recoge
como la posición de llegada a la meta o la lista de corredores ordenada por tiempo total de
carrera. Las carreras pueden durar desde pocos minutos a varias horas, y las competiciones, varias
semanas.

La información referente a los corredores y las carreras se encuentra disponible públicamente, pero
en ocasiones en un tiempo muy posterior al final de la competición.

### Restricciones

Dadas todas estas modalidades deportivas, se decidió acotar el análisis del proyecto exclusivamente
al *Tenis* profesional. Especialmente, en su categoría semi-profesional o liga [@ITF].

Se consideró al *Tenis* como el deporte más apto para ser analizado. Existe
información recopilada por la [@ITF] sobre los resultados de los partidos y los equipos cuentan con
un único jugador[^1.11] lo que simplifica mucho el modelado estadístico.

En concreto, se ha puesto especial énfasis en los torneos de la liga [@ITF], jugados por jugadores
jóvenes que no compiten al mayor nivel, de los cuales hay menor cantidad de información e interés,
pero de los cuales, aún así, se recopilan suficientes datos.

El sistema resultante está, sin embargo, preparado para admitir otros deportes sin demasiadas
modificaciones, permitiendo la recogida de datos y su análisis siguiendo los mismos procedimientos.

El resto de deportes evaluados incluía una serie de características negativas que dificultaban
mucho el trabajo:

- **Fútbol**: Al tratarse de un deporte de equipos grandes, resulta muy difícil determinar las
  características de un equipo, así como compararlos entre ellos. Estos equipos además cambian
  mucho a lo largo del tiempo, e incluso, en ocaciones, no existen datos históricos de los mismos,
  como es el caso de las selecciones nacionales. Los resultados del fútbol son además una
  dificultad añadida: La posibilidad de empate complica también la comparación entre partidos.
  La inmensa popularidad del fútbol hace que existan múltiples recursos de información sobre el
  mismo, pero también se trata de un campo ya muy estudiado, donde existen sistemas de predicción
  sofisticados que sería difícil mejorar, dado el alcance de este proyecto. La influencia del
  entrenador también es relativamente difícil de evaluar, dependiendo esta de factores poco
  cuantificables, que se obtienen a partir de información informal acerca del equipo y su entorno.

- **Baloncesto**: El ser un deporte de equipo cuyos componentes mutan a lo largo del tiempo
  dificulta mucho el modelado y la comparación. Incluso aunque se trate de equipos pequeños donde
  sus componentes tengan un peso proporcionalmente mucho mayor que en otros deportes. Aunque los
  resultados son fáciles de categorizar (gana uno u otro), el hecho de que haya diferencias entre
  distintas modalidades de juego introduce también ciertos inconvenientes que habría que tener en
  cuenta. El hecho de la posible influencia del entrenador sobre el equipo también es un obstáculo,
  ya que sería necesario modelar esta de alguna manera a partir de algún criterio informal.

- **Ciclismo**: Aunque el que los corredores participen a título personal, los resultados sean
  cuantificables y sea relativamente sencillo trazar el historial de un participante, se trata de
  un deporte con muchas modalidades diferentes, que depende en ocasiones de muchos factores
  ambientales[^1.12], y donde la forma de los corredores, algo difícil de cuantificar, tiene una
  influencia muy determinante. Se trata de un tipo de competición compleja, poco frecuente, de la
  que existen datos, pero no demasiados, por la frecuencia anual de las carreras. Es por tanto, un
  mal candidato para un modelado estadístico sencillo.

Es por tanto que este sistema se limita a predecir resultados de *Tenis* exclusivamente, sin
menoscabo de una futura ampliación.

### Entregables

El proyecto comprende los siguientes productos, que juntos conforman el sistema completo,
denominado **Pronos**:

- *Estudio* de la disponibilidad de datos sobre *Tenis* profesional, torneos, partidos, jugadores y
  resultados.
- *Base de datos* histórica de resultados y jugadores, estructurada de manera que permita las
  consultas necesarias para la creación de un conjunto de datos para su análisis estadístico.
- *Modelo predictivo* estadístico, entrenado a partir de los datos históricos, que permita predecir
  el resultado de un partido a partir de sus datos básicos antes del comienzo del mismo, con una
  tasa de error conocida.
- *Sistema de recopilación de datos*: Un sistema que permita obtener y guardar de forma organizada
  los datos actualizados referidos a los partidos, históricos y modernos a partir de fuentes
  públicas.

Estos diferentes componentes se encuentran en el software y la documentación entregada en este
proyecto, y conjuntamente, forman un sistema de predicción contínua de resultados deportivos.

### Trabajo futuro

Dado el estado del proyecto, se dejan abiertas las siguientes líneas de avance para futuras
modificaciones:

- Inclusión de recopilación de datos para otros eventos deportivos: El diseño de la aplicación
  debería permitir el encaje de varios tipos de deportes muy distintos, habiéndose realizado este
  de la manera más genérica posible, evitando las asunciones propias del deporte elegido para la
  primera implementación, esto es, el *Tenis*.

- Modelado de otros deportes: El análisis exploratorio y el modelado estadístico realizado para el
  *Tenis* es difícilmente válido para otros tipos de deportes, incluso de tipo individual. Es por
  tanto interesante la inclusión de modelado para otros deportes, empleando un enfoque diferente
  para admitir comparaciones de equipos y otros tipos de resultados no binarios.

- Visualización de datos: El proyecto se centra en la recopilación, procesado y análisis
  estadístico de los datos existentes de *Tenis*, sin embargo no realiza grandes esfuerzos en
  visualizar esos datos y en facilitar el acceso a los mismos, ya que se entiende que no es la
  finalidad última del proyecto, a parte de existir otros proyectos[^1.13] que ya realizan esa
  tarea de manera muy efectiva.

- Visualización de algoritmos: Los algoritmos de aprendizaje automático empleados para modelar la
  predicción de resultados pueden ser visualizados para su mejor comprensión. Esta labor excede el
  alcance del proyecto, ya que, aunque se compara entre diferentes algoritmos, no se realiza un
  trabajo exhaustivo en este sentido, evaluando simplemente su eficacia en términos de error
  relativo, y no en su rendimiento computacional.


[^1.1]: Ver [@GamingWorld] capitulo primero.
[^1.2]: Ver el informe financiero elaborado por Deloitte [@FootballReview2017] figura *Big five European league club's reneuves - 2015/16*.
[^1.3]: Ver el informe de la Comisión Europea [@OnlineGamblingStudy] apartado *Online gambling: an emerging policy issue*.
[^1.4]: Ver [@FCoachPerf] donde se analiza el efecto de los entrenadores sobre el rendimiento de equipos de fútbol universitarios.
[^1.5]: Salvo en cierto tipo de competiciones, donde no se permite el empate, forzando un ganador al final del tiempo.
[^1.6]: Según las reglas de la [@FIBA].
[^1.7]: Según las reglas de la [@NBA].
[^1.8]: Salvo en caso de fase final ida/vuelta, en que puede haber empate en el partido de ida o en
        el de vuelta, decidiéndose el ganador por la suma de puntos de ambos partidos.
[^1.9]: No existe ningún estudio que pruebe la poca relación del entrenador con los resultados del
        tenis, se trata esto, por tanto, de una opinión personal.
[^1.10]: En caso de que el partido se alargue demasiado sin que ninguno de los dos jugadores
         obtenga una diferencia significativa en puntuación que determine el resultado, se
         establece un procedimiento denominado *Tie-Break* o *Muerte Súbita*. Ver [@TennisRules].
[^1.11]: Ignorando la categoría de dobles.
[^1.12]: Incluyendo periódicos escándalos de dopaje y tráfico de influencias en equipos y
         federaciones.
