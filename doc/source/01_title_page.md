<!--
This is the Latex-heavy title page.
People outside UCL may want to remove the header logo
and add the centred logo
-->

\begin{titlepage}
    \begin{center}

    % Delete the following line
    % to remove the UCL header logo
    \ThisCenterWallPaper{0.5}{style/logo_uca.jpg}

        \vspace*{1cm}

        \huge
        Sistema de predicción de resultados deportivos

        \vspace{1cm}
        \huge
        Ingeniería en Informática

        \vspace{8.5cm}

        \Large
        Francisco Javier Santacruz López-Cepero

        \vspace{0.5cm}

        \vfill

        \normalsize
        Director:\\
        Manuel Palomo Duarte

        \vspace{0.8cm}

        % Uncomment the following line
        % to add a centered university logo
        % \includegraphics[width=0.4\textwidth]{style/univ_logo.eps}

        \normalsize
        Universidad de Cádiz\\
        Septiembre 2017

        % Except where otherwise noted, content in this thesis is licensed under a Creative Commons Attribution 4.0 License (http://creativecommons.org/licenses/by/4.0), which permits unrestricted use, distribution, and reproduction in any medium, provided the original work is properly cited. Copyright 2015,Tom Pollard.

    \end{center}
\end{titlepage}
