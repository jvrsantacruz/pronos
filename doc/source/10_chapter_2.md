# Fundamentos

Este proyecto se basa en el análisis sistemático y modelado estadístico de resultados de partidos
de tenis de la liga [@ITF]. Es por tanto interesante conocer las reglas, mecánica, historia e
idiosincrasia de este deporte, ya que de su estudio se derivan ciertas nociones intuitivas que
pueden resultar posteriormente útiles para el análisis.

## Tenis

El *Tenis* es un deporte de raqueta que puede ser jugado individualmente contra otro oponente, o
por parejas. Cada jugador emplea una *raqueta de tenis*, con la que golpea una pelota de goma
hueca, con la intención de introducirla en el campo contrario, haciéndola pasar sobre una red que
divide ambos campos. Cuando el otro jugador no es capaz de recibir y devolver la pelota, se
considera que pierde un punto, que es ganado por el contrario. El campo puede encontrarse sobre
diferentes superficies, que aportan diferentes características cinéticas al bote de la pelota y la
situación del tenista en el juego; es común jugar sobre *hierba*, pistas *duras* de cemento, pistas
de *tierra* batida o arcilla, y moqueta.

Las reglas del *Tenis* se establecieron alrededor de 1890, y han sufrido escasas modificaciones
desde entonces, con las excepciones de la introducción del *Tie Break* o *Muerte Súbita* en 1970 y
el uso de sistemas electrónicos para el conteo de puntos, sistema conocido como *Ojo de Halcón*.

Las competiciones de *Tenis* son especialmente populares en todo el mundo. En concreto los cuatro
torneos conocidos como *Grand Slam*, que comprende el *Open de Australia*, el *Open de Francia*, el
*US Open* y el torneo de *Wimbledon*.

## Origen Histórico

El origen del juego del *Tenis* puede verse en el *real tennis*, una forma Medieval de juego de
pelota que se jugaba alrededor del siglo *XII* en Francia, en la cual se golpeaba una pelota con la
mano desnuda, y posteriormente con un guante. Existe la teoría de que este juego era jugado por
monjes en los claustros de los monasterios, basado en la construcción y apariencia de las primeras
pistas.

Alrededor del siglo *XVI*, se introdujeron sensibles variaciones, como la introducción de la raqueta,
la delimitación del terreno de juego y un conjunto de reglas más o menos estable. En este período
se encuentra documentada la popularización del juego a través de la realeza por gran parte de
Europa, alcanzando su máxima popularidad en este siglo.

El origen del *Tenis* moderno se remonta a Birmingham alrededor de 1860, donde se establecen las
bases del "*Lawn Tennis*". con la publicación del libro "Sphairistike or Lawn Ten-nis" [@GameOfTennis].

Las primeras competiciones de *Tenis*, que luego darían lugar a los *Grand Slams*, tuvieron lugar
inicialmente en Estados Unidos, donde alrededor de 1881, el deseo de competir llevó al
establecimiento de clubs de tenis, lo que a su vez sirvió para fomentar las competiciones. Es en
esta época cuando se inician las competiciones de Wimbledon [@Wimbledon], US Open [@USOpen], French
Open [@RolandGarros] y el Australia Open [@AustraliaOpen]. Posteriormente, en 1899, se inicia una
competición internacional entre los Estados Unidos y Reino Unido, actualmente conocida como *Davis
Cup* [@DavisCup], que incluye a más de 129 países.

## Reglas de Juego

El tenis se juega en una superficie plana y rectangular. La pista tiene 23 metros de largo por 8
metros de ancho para partidos individuales, y 11 metros de ancho para partidos dobles. Una red
estirada a lo ancho divide el campo en dos mitades iguales. La red se encuentra a una altura
aproximada de 1 metro en los postes que la sujetan en los laterales, y de 0.91 metros de alto en el
centro. El suelo de la pista puede estar compuesto de una gran variedad de tipos de superficie,
incluyendo cemento, asfalto, césped, arcilla o moqueta, especialmente para partidos de interior.

La cancha se encuentra dividida en diferentes espacios por líneas dibujadas sobre su superficie,
como puede verse en la figura \ref{tennis-court}. Estas dividen cada mitad de la pista en tres
rectángulos, siendo dos de ellos, los más cercanos a la red, la mitad que el anterior, que se sitúa
al extremo de la pista. La línea que separa estos dos rectángulos divide la pista por la mitad en
dos, e incluye una pequeña marca al final, al intersectar con la línea que marca exterior de la
pista; esta es conocida como *Línea de servicio*, ya que es donde el jugador debe situarse a la
hora de realizar un *saque* o *servicio* y poner la pelota en juego.

![Pista de Tenis (Wikimedia CC-SA) \ref{tennis-court}](source/figures/tennis-court.jpg){ width=400px }

Cada jugador comienza a un lado de la red, uno de ellos es designado para servir el inicio del
partido, mientras que el otro recibirá el saque (*receptor*). Este servicio se alterna entre uno y
otro jugador, de manera que es realizado por uno de los contrincantes cada juego. En el servicio,
la pelota debe pasar al otro campo y tocar el suelo dentro del rectángulo pequeño del lado opuesto,
para lo cual existen dos oportunidades, permitiendo un segundo saque tras un primer *fallo de
servicio*. El jugador no debe pisar el interior de la lista ni la línea de saque antes de tocar la
pelota, de otra manera se considerará como un *fallo de servicio* igualmente. Si ambos intentos
fracasan, es considerado un *doble fallo* y el *receptor* ganará el punto.

Tras recibir el saque, el otro jugador debe devolver la pelota por encima de la red, de manera que
esta bote dentro del espacio del campo del otro jugador. En el momento en el que alguno de los dos
falla en hacer esto, se considera que su contrario gana el punto. Se considera legal que la pelota
toque la red, siempre que continue su camino hacia el campo contrario.

## Puntuación

La puntuación se plantea en tres niveles jerárquicos que se contienen unos a otros, *Juego*, *Set*
y *Partido*.

![Puntuación final en el partido Andy Roddick contra Cyril Saulnier (Luiz Eduardo CC-BY) \label{tennis-score}](source/figures/tennis-score.jpg){ width=350px }

Un *juego* consiste en una sucesión de puntos, en los que el mismo jugador realiza el servicio de
saque. El juego se considera ganado por el primer jugador que obtenga al menos cuatro puntos, con
una ventaja de dos sobre su oponente. La puntuación sigue un sistema particular, vagamente basado
en las marcas de un reloj, siguiendo la secuencia: *nada*, *quince*, *treinta*, y *cuarenta*. A
partir del cuarto punto, se denomina *ventaja* a cada punto que un jugador aventaje al otro, e
*iguales* cuando estos se encuentran empatados. La puntuación del jugador que sirve se denomina
primero en el resultado.

Un *set* consiste una sucesión de *juegos*, terminando cuando la cuenta de juegos ganados llega a
seis con una ventaja de dos. Si esto no es posible, se dispone de un juego adicional, donde si el
jugador que lleva ventaja consigue el juego, gana el set `7-5`, y en caso contrario, este se decide
a *Muerte Súbita* o *Tie-Break*.

El *Tie-Break* consiste en un *juego* especial, donde el primer jugador en conseguir al menos 7
puntos con una ventaja de dos sobre su oponente, gana automáticamente el *juego* y el *set*. Esta
regla fué introducida para evitar partidos muy largos con muchos puntos.

El *partido* consiste igualmente en una sucesión de *sets*, que se juega *al mejor de tres* o *al
mejor de cinco*, donde el primer jugador en ganar dos sets decide el resultado, como podemos ver en
la puntuación de final de partido mostrada en la figura \ref{tennis-score}.

En todas las competiciones profesionales, existe la figura del *juez*. Este se sitúa normalmente
sobre una silla alta a uno de los lados de la pista. El juez tiene autoridad total sobre el juego y
es ayudado por uno o más jueces de línea, situados a los lados de la pista, que le ayudan a
determinar. El *árbitro* se sitúa fuera de la pista, mantiene la autoridad sobre cuestiones de
reglas, no pudiendo contradecir los hechos aceptados por el *juez*, salvo que este se encuentre
dentro de la pista.

## Competición

El *Tenis* mantiene una serie de competiciones que se realizan periódicamente a lo largo del año en
todas partes del mundo donde juegan jugadores de niveles muy distintos. La Federación Internacional
de Tenis [@ITF] organiza torneos nacionales en todos los países contando con premios sustanciales
para el ganador. La liga se realiza en cada país y enfrenta a jugadores muy jóvenes con poca
experiencia. Se realizan muchos partidos, de los que se mantiene registro público por parte de la
Federación.

El hecho de que los torneos se celebren en diferentes fechas del año, y que la cuantía de los
premios sea elevada, en el rango de las decenas de miles de euros, ha permitido la creación de un
circuito profesional permanente, donde jugadores profesionales participan en torneos en diferentes
partes del mundo, dedicándose a ello en exclusiva.

La mayoría de competiciones se organizan en forma de torneos eliminatorios, donde los jugadores se
van enfrentando entre si en partidos asignados al azar en varias rondas, hasta llegar a un partido
final, donde se decide el ganador.

![Emparejamiento de jugadores en el torneo Spain F27 Futures \label{tournament-drawsheet}](source/figures/tournament-drawsheet.png){ width=400px }

La organización habitual incluye rondas eliminatorias (`R32`, `R16`, ...) que dan paso a cuartos de
fina, semifinal y final. Cada partido implica que el ganador pasa a la siguiente ronda, y el resto
queda fuera de la competición, tal como se puede apreciar en la figura \ref{tournament-drawsheet}.

Entre las principales ligas existentes se encuentran los *Masters 1000*, las *series 250 y 500* y
los torneos *Challenger Tour* y *Futures*.

La *ATP World Tour Masters 1000* consiste en un grupo de nueve torneos, fuera de los cuatro que
componen el *Grand Slam*. Mantienen un sistema de puntos, donde ganar un torneo proporciona 1000
puntos, que se emplean para confeccionar un ranking de jugadores.

Las *ATP World Tour 250 series* con 40 competiciones y *ATP World Tour 500 series* con 11 forman
las siguientes categorías de torneos de la [@ATP]. De la misma manera que la serie 1000, mantienen
un sistema de rankings basado en la puntuación de los jugadores. Estos torneos ofrecen premios de
hasta 2 millones de dólares.

La *Challenger Tour* es el nivel más bajo de competición administrado por la [@ATP]. Se encuentra
compuesto de alrededor de 150 torneos diferentes, y suele ser el el punto de entrada para tenistas
en su carrera profesional. Los premios ofrecidos van de los $25.000 a los $150.000, de manera que
es usado por muchos tenistas profesionales para mantener un puesto aceptable en los rankings.

Existen además otra serie de torneos denominados *Future tournaments*, organizados por la [@ITF].
Estos torneos ofrecen premios de alrededor de $10.000, aportan puntos para la ATP, y son muy
jugados en todo el mundo, con alrededor de 500 torneos al año.

## Apuestas

La industria del juego en línea ha creado un mercado muy sofisticado en el que puede encontrarse una
oferta amplia y variada de apuesta sobre eventos deportivos y competiciones de tenis. Estas
plataformas ofrecen información sobre resultados, así como de los partidos venideros. A la vez
permiten al usuario apostar dinero sobre el resultado de los partidos [@Bet888sport] [@WilliamHill]
[@Bet365].

Como parte del sistema de apuestas, estos sitios web recopilan y concentran una gran cantidad de
información sobre los diferentes torneos, los partidos y los jugadores. Suelen incluir una ficha
para cada jugador, con información adicional sobre los resultados históricos de ese jugador en
determinadas situaciones, dependiendo del tipo de pista, de torneo, y contra un determinado rival.
Esto puede verse en la figura \ref{Bet365Info}.

Junto a los hechos de un jugador o un torneo, las casas de apuestas también realizan *su propia
predicción* asignando una probabilidad de victoria a cada jugador. A partir de esta valoración, los
usuarios del sistema que apuestan, reciben mayor o menor bonificación en caso de acierto. Es por
tanto vital para el negocio de estas casas de apuestas la elección de un modelo predictivo eficaz
como condición para obtener beneficio de esas apuestas.

![Resumen de partido entre Tommy Robredo y Tallon Griekspoor en el torneo Challenger de Alphen en la página bet365.com \label{Bet365Info}](source/figures/result-bet365.png){ width=500px }

Sitios como [@Bet365] permiten realizar apuestas sobre el resultado de muchos torneos que podrían
categorizarse como prácticamente amateur, que suceden en lugares remotos, bajo poco control y de
los que se cuenta con una información muy limitada. Ese puede ser el caso de torneos como *China
Futures*, donde incluso la msima página de la federación [@ITF] no cuenta con la ficha completa de
todos los jugadores, omitiendo datos importantes como la edad del jugador.

Esta carencia de datos hace pensar que el sistema que establece los ratios de apuesta para cada
jugador en un partido dado, dependen menos de un análisis estadístico que de oferta y demanda pura,
actualizando los porcentajes a medida que los usuarios se van decantando por una opción u otra,
minimizando las pérdidas si muchos eligen una opción concreta. La ausencia de datos con los que
trabajar es también un factor limitante a lo que las casas de apuestas pueden hacer para determinar
el resultado de un partido de forma previa al mismo.

## Resumen

El *Tenis* es un deporte individual, de resultado binario y definitivo, que se juega continuamente
todo el mundo de forma profesional, regulada y registrada por una federación, contando con un
circuito estable de jugadores dedicados y remunerados, y un mercado de juego online que ofrece
predicciones sobre una gran mayoría de los partidos existentes, incluyendo muchos de muy escaso
nivel en lugares remotos.
