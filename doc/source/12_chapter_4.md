# Datos

> - Data! Data! Data! - He cried - I cannot make bricks without clay
>
[@ConanDoyle]

En este capítulo desarrollaremos la identificación, obtención, limpiado y exploración de los datos
con el fin último de establecer un modelo predictivo. Siendo nuestro objetivo partidos
*Challenger* y *Futures* de la liga *ITF*, las categorías más bajas, obtener datos históricos
completos, veraces y contínuamente actualizados se convierte en un reto.

Se pretende dar una idea completa de la estrategia de análisis empleada y de los procedimientos
generales que se usaron para obtener y limpiar los datos. Para más detalles de implementación,
véase el capítulo de *Implementación*, donde exponen con detalle las particularidades de los
sistemas de extracción de datos.

## Fuentes de datos

Existen múltiples fuentes que ofrecen un histórico razonablemente completo de partidos de la
[@ITF]. Cada una de las fuentes tiene sus particularidades, pero en todos los casos se trata de
páginas web diseñadas para el consumo humano, y que, en ocasiones incluso llegan a implementar
mecanismos para impedir, o más bien dificultar, la obtención de los mismos de manera automática.

Podemos en este caso distinguir entre dos tipos de fuentes: *primaria* y *secundarias*, siendo una
fuente *primaria* aquella de la que se derivan los datos directamente, es decir, la fuente oficial
que los publica, y *secundarias*, aquellas que simplemente recopilan y agregan la información
obtenida de otra parte, pero que, de alguna manera, ofrecen ventajas sobre la fuente original, bien
por organización, disponibilidad o por encontrarse los datos ampliados o agregados de antemano.

La principal fuente es naturalmente la página oficial de [@ITFProCircuit], la cual mantiene una
sección de torneos, donde estos se listan temporalmente[^4.1], como puede verse en la figura
\ref{itf-tournaments-page}. En la figura pueden apreciarse los diferentes torneos que se encuentran
abiertos actualmente, incluyendo el premio final.

![Listado de torneos en la página web de la ITF \label{itf-tournaments-page}](source/figures/itf-tournaments-page.jpg){ width=70% }

Cada torneo incluye información muy detallada sobre el tipo de superficia, los jugadores y su orden
de juego (ver figura \ref{itf-tournament-detail-page}). Cada jugador además incluye una ficha
potencialmente muy detallada especificando su edad, sexo, nacionalidad, ranking, e incluso algunos
datos personales como sus aficiones y hobbies o su superficie favorita, como puede verse en la
figura \ref{itf-player-profile}.

![Información general y detalles del torneo Canada F5 Futures. Puede apreciarse el orden de juego del día 3 de Septiembre, donde se enfrentaron Filip Peliwo y Ulises Blanch. \label{itf-tournament-detail-page}](source/figures/itf-tournament-detail-page.jpg){ width=90% }

![Página de información y ficha de jugador de Natalija Kostic. Sus principales aficiones son ir de compras, salir con los amigos y leer. \label{itf-player-profile}](source/figures/itf-player-profile.jpg){ width=70% }

Sin embargo, a pesar de ser muy completa en datos actuales, la página oficial de la [@ITF], como
fuente de datos, no presenta todas las entradas para torneos históricos, las cuales se retiran
periódicamente y, además, es un sitio razonablmente protegido contra la recolección automática de
datos, presentando una buena serie de impedimentos que detallamos en profundidad en el capítulo
de *Implementación*.

Es por estas dificultades que se inició la búsqueda de otras fuentes de datos alternativas, que
ofrecise la misma información, pero de una manera mucho más asequible. Efectivamente existen otras
fuentes, que denominamos *secundarias*, que se dedican a recopilar y ofrecer de forma homogénea el
histórico de partidos de estas ligas. Sin embargo, lo hacen un nivel de detalle mucho menor, y a
costa de mayor complejidad, al ser necesario recopilar información desde varios sitios a la vez.

Una de las principales y mejores fuentes de información actualmente es el proyecto *The Match
Charting Project*. Se trata de un esfuerzo colectivo por recopilar la mayor cantidad de información
sobre tenis disponible, y ofrecérsela a investigadores de la manera más limpia y ordenada posible.
La descripción que puede encontrarse en su página web [@TennisAbstract] reza así:

> The Match Charting Project is a crowdsourced effort to collect detailed shot-by-shot data for pro
> matches. We added over 1,100 matches in 2016 and continue to add more almost every day.
>
> The data is all available, free, to researchers. You can also click on any of the links below to
> see detailed reports for every match in the database. Please help us chart more matches!
>
> [@TennisAbstract]

En esta página web puede encontrarse gran cantidad de información en un formato muy claro y
sencillo[^4.2], como puede verse en la figura \ref{tennisabstract-player}, donde puede verse el
historial de un jugador, incluyendo su información, torneos y últimos partidos jugados.

![Perfil de la jugadora Natalija Kostic en Tennis Abstract. \label{tennisabstract-player}](source/figures/tennisabstract-player.jpg){ width=90% }

Se trata de un proyecto francamente ambicioso que recopila una gran cantidad de información muy
detallada sobre los puntos jugados en mutltitud de partidos, pero que lamentablemente, no incluye
esta información sobre los partidos que nos resultan relevantes, esto es, de la liga *Challenger* y
*Futures*. Entre otros motivos, porque muy probablemente, esa información no se recopile para estos
partidos, y si lo hace, la federación no tome el esfuerzo necesario para publicarlos y ofrecerlos
al público.

La única debilidad de este sitio, es su escasa frecuencia de actualización. A pesar de mantener
partidos razonablemente actualizados, sucedidos en el mismo mes, no incluye información sobre los
partidos inminentes que aún no se han jugado. En la página web de la [@ITF], si podemos encontrar
estos partidos, ya que se convocan el día anterior por la tarde, con aproximadamente 12h de
antelación, pero lo hacen en un formato un tanto variable que es difícil de extraer. Es por esto
que nos volvimos hacia una página de estadísticas con interés en en el mercado de apuestas, donde
estos datos son, quizá, el objetivo, para poder tener datos actualizados de los partidos.

Tras evaluar diferentes páginas como [@Betfair] o [@Bet365], se decidió optar por la página
[@Matchstat]. En *Matchstat*, como puede apreciarse en la figura \ref{matchstat-upcoming}, podemos
encontrar agrupados de una manera muy laxa todos los partidos próximos, incluyendo también fichas
actualizadas de los jugadores y otra información, aunque esta sea, en general, mucho menos
detallada que en las otras fuentes referenciadas.

![Partidos del día en Matchstat. Son estos los partidos convocados o realizados muy recientemente en cualquier parte del mundo. \label{matchstat-upcoming}](source/figures/matchstat-upcoming.jpg){ width=90% }

A partir de la información disponible en estos sitios web, se crea un sistema contínuo que extrae
información y la almacena en una base de datos de manera estructurada y tras un limpiado y
homogeneización, para posterior consulta.

## Base de datos

La extracción del cuerpo de datos histórico que compone la base de datos a partir de la cual se han
hecho las primeras labores de exploración y minado de datos, así como sobre la que se ha entrenado
el modelo posterior tras entrenar todos los algoritmos, se recopiló durante Agosto de 2017 de forma
permanente, siendo 15 días el tiempo total empleado hasta adquirir una base de datos de más de un
millón de registros.

A partir del trabajo de las arañas, se obtuvo una gran cantidad de datos históricos para este tipo
de competiciones. Teniendo en cuenta que en cada torneo se juegan entre 100 y 200 partidos como
mínimo, tras obtener alrededor de 50.000 torneos, se cuenta con alrededor de un millón de partidos,
implicando a más de cien mil jugadores. Sin embargo, no todos estos partidos ni jugadores mantienen
sus datos completos; en la tabla \ref{table-db-totals} podemos ver cómo solo conseguimos el
resultado para 800.000 partidos, dejando más de 200.000 sin conocer el ganador. Bien porque estos
datos no se encontraban disponibles, el partido se canceló o los sistemas fallaron en asociar el
resultado con el partido.

---------------------------------------------------------------------------
Events       Matches       Players      Results
-------      ---------     --------     --------
49.907       1.042.171     118.604      805.569

---------------------------------------------------------------------------

Tabla: Número total de elementos extraidos a partir de las diferentes fuentes de datos. Incluye el
número de eventos o torneos, en los cuales se celebran partidos, en los que compiten jugadores
obteniendo un resultado. \label{table-db-totals}

## Campos disponibles

Los datos que nos resultan interesantes son aquellos que nos valen para realizar una predicción.
Sin embargo, una de las grandes dificultades que encontramos es la escasez de datos completos. En
otras palabras, hay decenas de miles de registros de partidos de la [@ITF], sin embargo, no todos
ofrecen los mismos datos, especialmente en los perfiles de jugadores, dándose casos donde apenas se
contaba con el nombre del participante.

Esto nos obligó a realizar primero una criba sobre la cantidad de información que iba a extraerse y
almacenarse, según su disponibilidad, ya que carece de sentido el hacer acopio de campos que la
mayoría de los registros no aportan, o lo hacen con información parcial. Es necesario que los
registros sean lo más homogéneos posible, de forma que sea posible el realizar el entrenamiento,
todos deben ser comparables este si.

Viendo esta dificultad, analizamos los datos recopilados, buscando el conjunto mínimo de datos
disponible que nos daba una buena cantidad de partidos, y seleccionando estos datos para realizar
el trabajo. En la tabla \ref{table-data-fields} podemos ver la lista completa de campos que se
evaluaron a la hora de realizar la extracción.

-----------------------------------------------------------------------------------------
Entidad    Campo           Tipo      Descripción
--------   ------------    -------   --------------------------------
Evento     Nombre          String    Nombre del torneo.

Evento     Código          String    Código de la [@ITF] para el torneo.

Evento     Género          String    Sexo de los participantes en el torneo.

Evento     Lugar           String    Ciudad del mundo donde se realiza la competición.

Evento     URL             String    URL a la página oficial.

Evento     Premio          Numeric   Premio en metálico para el ganador.

Evento     Superficie      String    Tipo de superficie de juego.

Evento     Categoría       String    Categoría asignada al torneo.

Evento     Fecha inicio    Date      Fecha de inicio del torneo.

Evento     Fecha fin       Date      Fecha de fin del torneo.

Partido     Código         String    Código de la [@ITF] para el partido.

Partido     Género         String    Sexo de los jugadores en el partido.

Partido     Ronda          String    Ronda del partido dentro del torneo.

Partido     Evento         Evento    Evento en el que este partido se disputa.

Partido     Fecha          Date      Fecha de juego.

Partido    Superficie      String    Tipo de superficie de juego.

Partido    Categoría       String    Categoría del partido en el torneo.

Partido    Jugador1        Jugador   Jugador 1 del partido.

Partido    Jugador2        Jugador   Jugador 2 del partido.

Jugador    Nombre          String    Nombre del torneo.

Jugador    Código          String    Código de la [@ITF] para el jugador.

Jugador    Género          String    Sexo del jugador.

Jugador    Nacionalidad    String    País de origen del jugador.

Jugador    URL             String    URL al perfil del jugador en la federación.

Jugador    Ranking         Numeric   Ranking del jugador en su categoría.

Jugador    Nacimiento      Date      Fecha de nacimiento del jugador.

Jugador    Peso            Numeric   Peso del jugador.

Jugador    Altura          Numeric   Altura del jugador.

Jugador    Mano            String    Jugador zurdo/diestro. Mano preferida.

Resultado  Ganador         Jugador   Jugador que ganó el partido.

Resultado  Marcador        String    Resultado del marcador.

-----------------------------------------------------------------------------------------

Tabla: \label{table-data-fields} Campos disponibles en las fuentes de datos consultadas,
considerados útiles para la predicción.

Todos estos valores son obtenidos e insertados en una base de datos normalizada[^4.4]. Sin embargo,
a la hora de construir nuestro conjunto de datos de análisis (en adelante *dataset*), emplearemos
una estructura tabular donde cada fila será un partido, y cada columna, uno de sus valores
asociados. Cada campo es además convertido a un formato numérico de manera que podamos trabajar
incluso tratándose de categorías. La lista completa de campos puede verse en la tabla
\label{table-dataset-fields}, donde además figuran sus valores típicos y su traducción numérica.

-------------------------------------------------------------------------------------------------------
Campo        Valores                        Numérico      Descripción
-------      ---------                      ---------     ---------------------------------
date         Date                           NA            Fecha del partido

gender       *male*, *female*               1, 2          Sexo de los jugadores

p1age        Integer                        NA            Edad del jugador 1

p1country    String                         NA            País de origen del jugador 1

p1hand       *right*, *left*                1, 2          Mano preferida del jugador 1

p1height     Integer                        NA            Peso del jugador 1

p1ranking    Integer                        NA            Ranking del jugador 1

p1weight     Integer                        NA            Altura del jugador 1

p2age        Integer                        NA            Edad del jugador 2

p2country    String                         NA            País de origen del jugador 2

p2hand       *right*, *left*                1, 2          Mano preferida del jugador 2

p2height     Integer                        NA            Peso del jugador 2

p2ranking    Integer                        NA            Ranking del jugador 2

p2weight     Integer                        NA            Altura del jugador 2

place        String                         NA            Lugar de celebración del partido

prize        Integer                        NA            Premio del torneo, en euros

round        *final*, *semi-final*,         1-10          Ronda del torneo en la que se juega este partido
             *quarter-final*, *round-16*,
             *round-32*, *q*, *q1*, *q2*,
              *q3*

surface      *clay*, *grass*, *hard*,       1-4           Tipo de superficie de la pista de juego
             *carpet*

wonp1        *True*, *False*                1, 0          Indicador de si ganó el jugador1 o el jugador2

-------------------------------------------------------------------------------------------------------

Tabla: \label{table-dataset-fields} Campos disponibles en el *dataset* empleado para el análisis.
Cada campo incluye la lista de valores típicos y su traducción numérica.

De estos campos, muchos no se encontraban disponible en todas las fuentes de manera consistente, de
forma que hubo que ignorarlos. Otros contenían valores inválidos o no se encontraban registrados
para muchos de los partidos obtenidos. Como puede verse en la figura[^4.5]
\ref{data-not-null-fields} , hay una gran diferencia entre los valores de cada campo, teniendo de
algunos, como `prize` (*premio*), muy poca presencia.

![Cantidad de valores no-nulos para cada partido registrado. Mayor es mejor. \label{data-not-null-fields}](source/figures/data-not-null-fields.pdf)

Estos valores, además, no se encuentran concentrados en unos pocos partidos, sino que a la mayoría
le falta algún dato de una manera u otra. Como puede verse en la figura \ref{data-null-fields}, si
consideramos `prize` como un campo necesario, solo una pequeña proporción de los partidos
recopilados nos sirve de algo.

![Proporción de valores nulos o no disponibles para cada partido registrado. Mayor es peor.  \label{data-null-fields}](source/figures/data-null-fields.pdf)

De los datos recabados, no todos tienen sentido para la predicción. Por ejemplo, en los datos
históricos contamos con entradas muy antiguas de alrededor de 1900[^4.3] como puede verse en la
figura \ref{data-all-matches-per-year}, en la que puede observarse que apenas hay partidos
recopilados desde 1900 hasta 1990 aproximadamente.

![Número de partidos recopilados por año. Puede observarse como los datos históricos se remontan hasta 1900, pero con una cantidad anecdótica de casos. \label{data-all-matches-per-year}](source/figures/data-all-matches-per-year.pdf)

Los datos de estos partidos, naturalmente, no tienen demasiada relevancia a la hora de tratar de
predecir resultados futuros de partidos modernos, de manera que podemos eliminarlos. De hecho,
podemos eliminar todos los partidos que no tengan alguno de los datos básicos, o cuyos
participantes tengan más de 50 años, trabajando únicamente sobre los partidos con datos completos.
Esto puede verse en la figura \ref{data-matches-per-year}. Los números de partidos recopilados en
determinados años varían en función de cómo el procedimiento de extracción encontró los datos.

![Número de partidos recopilados con datos completos. La extracción de los datos no es lineal, de manera que la distribución por año de los partidos obtenidos no es homogénea. Esta gráfica muestra solo registros de partidos completos. \label{data-matches-per-year}](source/figures/data-matches-per-year.pdf)

Una vez filtrados y tratados los datos, podemos ver en la figura \ref{data-histogram} cómo estos se
encuentran distribuidos y en qué manera.

![\label{data-histogram} Histograma de valores para todas las columnas con valores completos del dataset](source/figures/data-histogram.pdf){ width=120% }

## Selección de parámetros

Ante una colección de datos grande, donde existen muchos parámetros que inicialmente podrían
constituir una fuente relevante de información, lo natural es preguntarnos ¿qué información nos
sirve? ¿cuánta aporta simplemente ruido? Empleando conocimiento específico sobre el dominio del
problema que nos encontramos evaluando, y un poco de sentido común, es posible identificar algunos
parámetros como relevantes. Sin embargo ¿lo son realmente?.

Por ejemplo, uno podría pensar que un jugador que juega un partido importante, rinde de manera
diferente a un partido inicial de clasificatoria; al fin y al cabo, la presión altera el
comportamiento de muchos jugadores, y puede llegar a ser determinante para el resultado del
partido. Sin embargo esto no es más que una de tantas hipótesis, y debe ser confirmada con datos.

Ante esta pregunta, aplicamos herramientas estadísticas básicas para comprobar si nuestras pequeñas
hipótesis se cumplen, y cuales son las relaciones entre los datos. Una primera aproximación
empleando el *coeficiente de correlación lineal de Pearson*, cuya matriz de correlación puede verse
en la figura \ref{data-correlation-matrix}, puede indicarnos cómo se relacionan los datos entre si.

Este coeficiente oscila entre *1 y -1*, donde 1 indica una relación lineal directa entre ambos
valores, 0 nos dice que no existe ningún tipo de relación y -1, el de relación lineal inversa.

![\label{data-correlation-matrix} Matriz de correlación para todos los campos del dataset que tienen valores completos.](source/figures/data-correlation-matrix.pdf)

En la matriz de correlación de la figura \ref{data-correlation-matrix} vemos cómo existe cierta
correlación entre la edad (`p1age` y `p2age`) y el premio (`prize`), lo que podría sugerir que los
jugadores de más edad disputan torneos más avanzados. Esto sería coherente al encontrar la misma
correlación con el ranking (`p1ranking`, y `p2ranking`). También existe una ligera correlación
entre los jugadores, las edades y rankings siguen la misma tendencia, lo que también nos podría
indicar que los jugadores tienen a jugar más con sus iguales. Otro factor interesante podría ser la
ronda del torneo (`round`) y el ranking de los jugadores; podría indicar que los jugadores con
mayor ranking tienden a llegar más lejos en las rondas de clasificación.

Aunque todas estas correlaciones nos aporten ciertas nociones intuitivas sobre cómo se relacionan
los datos entre ellos, lo que realmente queremos saber es si nos sirven para predecir el resultado
(`wonp1`). Para ello aislamos las correlaciones para esa única variable, como podemos ver en la
tabla \ref{table-won-correlation}.

---------------------------------------------------------------------------
Campo               Correlación
--------------      -------------------
prize               -0.001163

round               -0.005434

surface             -0.002747

p1age                0.056928

p2age               -0.060917

age\_diff             0.088200

p1hand               0.023016

p2hand              -0.015818

hand\_diff           -0.005651

p1height             0.039261

p2height             0.039261

height\_diff          0.047047

p1ranking           -0.253508

p2ranking            0.251823

ranking\_diff        -0.417147

p1weight             0.044315

p2weight             0.033540

weight\_diff          0.068796

wonp1                1.000000

---------------------------------------------------------------------------

Tabla: \label{table-won-correlation} Correlación de los distintos campos con la variable "ha ganado
el jugador 1". Esperamos ver cómo las correlaciones son positivas para el jugador 1 y negativas
para el jugador 2, con la excepción del ranking, el cual, cuanto mayor, peor.

Estos coeficientes suelen ser difíciles de interpretar en base a una tabla, de manera que es
siempre interesante visualizarlos, como puede verse en la figura \ref{data-correlation-lines}. En
la figura mencionada, se visualizan los datos como una nube de puntos, acumulada en dos líneas
verticales debido a la naturaleza binaria del parámetro objetivo `wonp1`, y una recta que
representa la pendiente de la correlación. Es posible así detectar de un simple vistazo cuáles son
los parámetros que mejor correlan.

![\label{data-correlation-lines} Regresión lineal de las distintas variables sobre el resultado objetivo
del partido `wonp1`. La correlación viene dada por la pendiente de la línea recta para cada
variable. Líneas horizontales indican ausencia de correlación. Líneas ascendentes, relación lineal
directa, líneas descendentes, relación lineal inversa.](source/figures/data-correlation-lines.jpg)

Tras analizar cómo se relacionan todas las variables con el resultado del partido, podemos ver como
existe una correlación evidente entre el *ranking* de un jugador y sus probabilidades de ganar.
Cuanto mejor es el *ranking* de un jugador (más cerca del 1), mejor es su desempeño. También
observamos una correlación algo más débil alrededor de la *edad*, aunque es posible que únicamente
refleje el efecto positivo del *ranking* de manera transitiva, ya que los jugadores con buen
ranking suelen tener mayor edad.

## Modelo predictivo

Una vez obtenidos los datos, explorado sus relaciones y determinado cuáles son los parámetros
relevantes, es el momento de tratar de configurar nuestro modelo para realizar esta predicción.

Por las características del problema, y por la base de datos que contamos, ya sabemos que queremos
un *algoritmo de aprendizaje supervisado*, que sea rápido en realizar la predicción, ya que debe
formar parte de un sistema dinámico, y que generalice correctamente, es decir, que sea resistente
al sobreentrenamiento, ya que a partir de los partidos conocidos debe ser capaz de interpretar
nuevos partidos también.

Otra posible decisión a tomar trata sobre el resultado de la predicción.
Debemos decidir si queremos aproximarnos al problema como uno de estimación o
de clasificación. Podríamos simplemente diseñar un algoritmo que clasifique los
partidos como ganados por el *jugador1* o el *jugador2*, o no solo eso, tratar
de obtener una probabilidad que nos indique cómo de probable es la predicción
realizada.

De los algoritmos de entrenamiento supervisado que permiten construir un modelo
predictivo existen varios que podrían adaptarse a nuestras necesidades:

- Decision-tree
- K-neighbors
- K-means
- Logistic-regression
- Random-forest
- Support-vector

Todos ellos se encuentran implementados en la biblioteca de recursos
científicos para el lenguaje Python *sklearn*. Los modelos cuentan con una
interfaz semejante que hace que podamos entrenarlos todos de manera homogénea.

Para realizar el entrenamiento necesitamos dividir nuestro conjunto de prueba
en dos partes de manera aleatoria.  Una, la mayor, el *conjunto de
entrenamiento*, con un 70% de los datos, será empleada para construir el modelo
empleando el algoritmo correspondiente. La otra, con el 30% restante,
seleccionado aleatoriamente, servirá para comprobar cómo generaliza el modelo y
su capacidad de predicción.

La secuencia es la siguiente:

1. Elegir algoritmo
1. Generar conjunto de entrenamiento
1. Generar conjunto de prueba
1. Entrenar el algoritmo
1. Comprobar el modelo contra el conjunto de prueba

En nuestro caso ejecutamos la predicción sobre un conjunto de entrenamiento formado por 28.887
partidos. Estos modelos entrenados son después probados contra el conjunto de datos que dejamos
apartado y observamos cómo de buenos son los resultados.

La precisión obtenida por los modelos para los algoritmos empleados puede verse en la figura
\ref{model-accuracy}, en la cual los algoritmos se encuentran ordenados en base a su precisión de
mayor a menor. En esta podemos ver cómo el algoritmo de **Regresión Logística** es el que mejor
rendimiento mantiene.

![Precisión de los modelos entrenados empleando diferentes algoritmos sobre los mismos conjuntos de datos. Los resultados se mantienen entre diferentes entrenamientos con datasets aleatorizados de difernete tamaño \label{model-accuracy}](source/figures/model-accuracy.pdf)

Al comprobar el modelo, no nos fijamos únicamente en su precisión, sino que generamos una serie de
métricas auxiliares que nos ayudan a entender cómo de bien o mal predice un modelo concreto. Estas
métricas se basan en tres conceptos: *precision*, *recall* y *support*.

- *precision*: Capacidad de predicción del modelo trabajando sobre los datos de prueba.
  Se calcula como el ratio $$tp / (tp + fp)$$ donde $$tp$$ es el número de positivos acertados y
  $$fp$$ el número de falsos positivos.
- *recall*: La capacidad de encontrar los resultados que son positivos. Se calcula como el ratio
  $$tp / (tp + fn)$$ donde $$tp$$ es el número de positivos acertados y $$fn$$ el número de falsos
  negativos.
- *support*: El número de ocurrencias en el conjunto de pruebas para cada clase.

En la gráfica de la figura \ref{model-classification-report} podemos observar matrices donde se
comparan las métricas de todos los algoritmos probados. Podemos observar cómo el algoritmo de
**Regresión logística** es el que mantiene mejores resultados para cada uno de los aspectos.
Podemos concluir entonces, a la vista del rendimiendo de los diferentes modelos, que este es el que
mejor se adapta al problema y el que debemos usar para realizar el predictor.

![Métricas de efectividad de los distintos modelos generados. \label{model-classification-report}](source/figures/model-classification-report.pdf)

Output completo del entrenamiento:

```
Training model random-forest using 28887 matches
Accuracy 0.610370729343
Coefficient:
 None
Intercept:
 None
R² Value:
 0.784539758369
             precision    recall  f1-score   support

        0.0       0.61      0.61      0.61      6143
        1.0       0.61      0.61      0.61      6238

avg / total       0.61      0.61      0.61     12381

Training model decision-tree using 28887 matches
Accuracy 0.604070753574
Coefficient:
 None
Intercept:
 None
R² Value:
 0.795444317513
             precision    recall  f1-score   support

        0.0       0.59      0.66      0.62      6143
        1.0       0.62      0.55      0.58      6238

avg / total       0.61      0.60      0.60     12381

Training model logistic-regression using 28887 matches
Accuracy 0.684678135853
Coefficient:
 [[  4.91696999e-03  -4.90726572e-04   5.90175911e-03   5.52214783e-04
    2.10788859e-03   2.32951444e-03  -2.12412205e-03   2.42444139e-03
    4.82678390e-04   1.23111871e-05  -4.05165524e-06  -2.30370432e-03]]
Intercept:
 [ 0.00491697]
R² Value:
 0.684667843667
             precision    recall  f1-score   support

        0.0       0.69      0.67      0.68      6143
        1.0       0.68      0.70      0.69      6238

avg / total       0.68      0.68      0.68     12381

Training model support-vector using 28887 matches
Accuracy 0.681447379049
Coefficient:
 None
Intercept:
 [ 0.00149786]
R² Value:
 0.693910755703
             precision    recall  f1-score   support

        0.0       0.68      0.68      0.68      6143
        1.0       0.68      0.69      0.68      6238

avg / total       0.68      0.68      0.68     12381

Training model k-means using 28887 matches
Accuracy 0.0535497940393
Coefficient:
 None
Intercept:
 None
R² Value:
 -157050925.081
             precision    recall  f1-score   support

        0.0       0.14      0.02      0.03      6143
        1.0       0.31      0.09      0.14      6238
        2.0       0.00      0.00      0.00         0
        3.0       0.00      0.00      0.00         0
        4.0       0.00      0.00      0.00         0
        5.0       0.00      0.00      0.00         0
        6.0       0.00      0.00      0.00         0
        7.0       0.00      0.00      0.00         0

avg / total       0.23      0.05      0.09     12381

Training model k-neighbors using 28887 matches
Accuracy 0.635732170261
Coefficient:
 None
Intercept:
 None
R² Value:
 0.737044345207
             precision    recall  f1-score   support

        0.0       0.63      0.63      0.63      6143
        1.0       0.64      0.64      0.64      6238

avg / total       0.64      0.64      0.64     12381
```


[^4.1]: Los torneos permanecen en la página web de manera indefinida, pero muchos de sus datos,
        incluyendo el orden de juego y resultados, son retirados tras unas pocas semanas.
[^4.2]: Para más información sobre cómo se disponen los datos en esta página y como fueron
        extraidos, consultar el capítulo *Implementación*.
[^4.3]: Esto es debido a que, al analizar el historial de un jugador, también se recopilan todos
        sus partidos jugados, y en ellos, se obtiene a su vez el historial de sus contrincantes.
        El efecto encadenado hace que se incorporen muchos partidos no directamente relacionados a
        la base de datos, llegando muy atrás, hasta 1900.
[^4.4]: Ver capítulo *Análisis y Diseño* para obtener una idea más precisa sobre el esquema de la
        base de datos.
[^4.5]: Las gráficas de este capítulo han sido generadas empleando `python`, empleando las
        librerías `pandas`, `matplotlib` y `seaborn`. Su generación se encuentra automatizada en el
        script `explore.py`, incluido junto al resto del código de aplicación. Ver el capítulo
        *Implementación* para más información.
