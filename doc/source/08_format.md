# Notación y formato {.unnumbered}

A lo largo de este texto, se empleará una notación propia para destacados, listados, citas y notas
que se especifica a continuación. Esta notación incluye el uso de cursiva para *Nombres Propios*,
así como de tipografía monoespaciada para menciones a literales específicos como nombres de
`variable`.

Las citas literales extraidas de fuentes se encontrarán sangradas a la izquierda y firmadas como
puede verse en el ejemplo:

> ¿Por qué permanezco acostado? La noche avanza, y lo más probable es que apenas raye el día se
> presenten los enemigos. Y si caemos en manos del rey, ¿quién podrá impedir que perezcamos entre
> ultrajes, y después de haber sufrido los suplicios más terribles? Nadie piensa en defenderse,
> nadie busca los medios para rechazar al enemigo; permanecemos acostados como si el ocio nos fuese
> permitido. Y yo, ¿a cuál general de otra ciudad espero para que haga esto? ¿A qué edad aguardo?
> Ciertamente que nunca seré mayor si hoy me entrego a los enemigos.
>
[@Anabasis]

Las imágenes serán enumeradas y referenciadas como *Figura \ref{example-figure}*. Estas se
encontrarán en una localización posterior al del resto del texto. En este caso se trata de un
logotipo alternativo para el proyecto GNU.

![Imagen de ejemplo: A logo for GNU with horns (Vladimir Zúñiga) Creative Commons CC0 \label{example-figure}](source/figures/gnu_hornedword.pdf){ width=400px }

Los *listados* permiten incluir fragmentos de código orignal[^1] dentro del propio texto del
documento.  El código se encontrará coloreado

```python
def crawl_historic(args, settings, spiders):
    settings['ITEM_PIPELINES'].update({
        'pronos.pipelines.MainPipeline': 350,
    })

    process = CrawlerProcess(settings)

    queues = []
    for spider in args.spider:
        queue = Queue(':memory:')
        queues.append(queue)
        for name in args.player:
            queue.push({'name': name})
        process.crawl(spiders[spider], queue=queue)
    process.crawl(spiders['main'], queues=queues, years=args.years)

    process.start()
```

[^1]: Indicando el autor original en casos en los que se trate de una fuente externa.

\newpage
\setcounter{page}{1}
\renewcommand{\thepage}{\arabic{page}}
