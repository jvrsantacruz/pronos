# Planificación

Durante el desarrollo del proyecto *Pronos* se ha seguido un modelo de ciclo de vida iterativo
incremental o creciente.

Este modelo de ciclo de vida se encuentra orientado al desarrollo del software mediante una serie
de versiones incompletas, pero que implementan funcionalidad parcial, entregando versiones con
funcionalidad creciente a cada etapa. Esto permite al desarrollador sacar conclusiones antes de
llegar al final del proyecto, permitiendo detectar antes los riesgos derivados de una mala
descripción de los requisitos, problemas inesperados o problemas de diseño.

Los pasos claves son construir una versión que implemente una simplificación de los requisitos del
sistema, y hacerlos evolucionar mediante una secuencia de versiones hasta que el sistema completo
se encuentre definido. En cada iteración, el diseño se actualiza, las nuevas funcionalidades son
agregadas y el sistema obtiene las capacidades previstas poco a poco. [@ISoftwareAddison]

El desarrollo de este proyecto ha sido dividido en cinco etapas principales. En ese sentido, no se
trata de un desarrollo convencional, donde se toman requisitos y se comienza a consturir el
software. Este proyecto, al estar orientado a la investigación y construcción de un modelo
estadístico, se ha visto orientado en sus etapas a la consecución de los requisitos de conocimiento
o materiales para poder comenzar con la siguiente. Todas ellas implicaron desarrollo sobre la misma
base de código de alguna manera. Estas etapas corresponden en ocasiones con la obtención de la
datos, el estudio de los servicios web que se consumían o el análisis de la base de conocimiento
obtenida.

Es por tanto que el proyecto no se compone solo del software resultante, sino del análisis y el
modelo resultante del estudio de los datos. El sistema completo, que obtiene, procesa, persiste y
evalúa esos datos, fue construido conforme a las necesidades en cada una de las etapas según
esperaba el cliente, en este caso *Manuel Palomo Duarte*, tutor de este proyecto.

La elección de este modelo de desarrollo fué originada a la vista de las siguientes necesidades:

- *Desconocimiento de las fuentes de información*: No era posible conocer de antemano la
  distribución de la información necesaria, su origen los campos y la cantidad de registros
  disponible para realizar el proyecto.
- *Diseño de obtención de datos dependiente de las fuentes*: El software de obtención de datos
  dependía completamente del formato y localización de las fuentes. El hecho de que muchas se
  encontrasen muy orientadas al consumo humano o implementasen protecciones contra la extracción
  automática de datos fué también determinante durante el desarrollo, creando nuevas necesidades
  imprevistas que consumieron gran parte del proyecto.
- *Análisis dependiente de la presencia de datos*: Desconociendo no solo los datos, sino la
  categorización y los campos disponibles, era imposible preveer qué aspecto tendría el análisis de
  los mismos, qué herramientas serían las más adecuadas para ello y qué posibles problemas tendría
  el proyecto.
- *Modelo predictivo dependiente del entrenamiento mediante estos datos*: Hasta no haber conseguido
  obtener una buena base de conocimiento no era posible trabajar en la confección y
  perfeccionamiento del modelo.

Estos factores que crean una interdependencia entre las distintas etapas del desarrollo del
software hicieron que siguiésemos esta aproximación, donde el cuerpo de código iba aumentando en
cada paso y perfeccionándose, conforme el concimiento del problema iba en aumento y los requisitos
no funcionales iban siendo establecidos mediante pura exploración.

## Descripción del desarrollo

El desarrollo se compuso de cinco iteraciones, incluyendo una final de documentación. En este
período, el proyecto pasa de ser una serie de pequeños programas dispersos, a una herramienta
centralizada que permite lanzar todos los procesos desde una interfaz unificada.

En general las distintas fases del proyecto se considera que son:

1. Investigación y documentación
1. Estudio de las fuentes de datos
1. Extracción de datos y base de conocimiento
1. Análisis exploratorio
1. Construcción del modelo

Se considera que existe una sexta etapa, en la que se compone todo el resultado de la investigación
obtenido anteriormente en un cuerpo de texto coherente que es el presente documento.

En los siguientes apartados se realizará una descripción en términos generales de las actividades
realizadas durante esos períodos de tiempo en los que se trabajó en desarrollo, los productos
obtenidos y las dependencias con respecto a la siguiente etapa o iteración.

### Iteración primera: Investigación y documentación

El primer paso tras la génesis inicial de la idea de proyecto, la de un sistema de predicción de
resultados deportivos, fué el estudio de los diferentes juegos y su grado de complejidad de cara a
la predicción, los datos disponibles públicamente, la literatura existente sobre predicción en
estos deportes, y las herramientas matemáticas relacionadas con el estudio de estos problemas.

Inicialmente se evaluarion los deportes:

- Fútbol
- Baloncesto
- Tenis
- Ciclismo

Se obtienen y estudian las reglas de estos deportes, tratando de obtener una medida de la
complejidad de su modelado en base al número de parámetros que intervienen en su desarrollo, y a la
posibilidad de medirlos.

Tras hacer una lista inicial con los deportes interesantes, se busca literatura científica
relacionada y se analiza la complejidad de las soluciones propuestas y su tasa de éxito.  Para
fútbol y baloncesto existe una larga serie de estudios que realizan un trabajo de predicción no muy
preciso, debido a que se trata de deportes de equipo y de la complejidad en la interpretación de
los resultados. Tras descartar la mayoría de ellos por su dificultad, se ajusta el alcance del
proyecto para incluir inicialmente el deporte de *Tenis*.

Se emplean inicialmente varias semanas en el estudio de diferentes artículos sobre modelado
estadístico de tenis. La mayoría de los artículos no emplea estrategias basadas en el
*Machine-Learning*. Se exploran las referencias de cada artículo. Estos son en ocasiones difíciles
de obtener, y no incluyen los datos ni el código empleado para los experimentos de los cuales
presentan los datos. En cada artículo se evalua la aproximación empleada, y, en los que lo
disponen, se estudia la categorización y organización de los datos empleados.

La exploración de las diferentes fuentes de datos se inicia con la determinación de las diferentes
categorías de competición. Se visitan las casas de apuestas y se trata de detectar patrones que
indiquen cuál es el modelado correcto de los datos, ya que todas las páginas ofrecen una vista
parcial donde no se encuentra la ficha completa del torneo, partido o jugador.

Se detectan una serie de páginas que contienen los datos deseados, se exploran sus fuentes para
conocer hasta qué nivel almacenan datos históricos. Se comprueba que los datos son consistentes
entre bases de datos. Se realiza un estudio comparativo en el que se determina que es preciso
acudir a más de una base de datos.

Este período de documentación inicial apenas si incluye código ejecutable como tal. Sin embargo se
hace acopio de documentación, se modelan los datos que hay que extraer y se disponen las fuentes.
Como resultado se obtiene una serie de contenidos y referencias de artículos de investigación que
tratan el tema con diferentes aproximaciones; una lista de sitios web que contienen la información
a priori necesaria para la realización de un modelo estadístico y una serie de nociones teóricas
sobre qué algoritmos disponibles son los apropiados para el tipo de predicción a realizar.

El período de esta iteración abarca dos meses y medio, se sitúa de primeros de Febrero al 15 de
Abril de 2017.

### Estudio de las fuentes de datos

Tras obtener una lista de posibles bases de datos de las que extraer la información necesaria, se
procede a estudiar las APIs disponibles o servicios de backend expuestos, de manera que se pueda
acceder a la información directamente, sin necesidad de realizar un procedimiento costoso para
extraerlo de la interfaz de las mismas.

Tras la búsqueda (mayormente infructuosa) de datos debidamente estructurados, se inicia un estudio
de las páginas de interfaz de las principales webs de datos. En este proceso se comprueba cómo de
referenciables son los datos dentro del HTML viendo si cumplen alguna de las condiciones:

- *Estructurados*: Los datos en el HTML no se encuentran en texto plano, sino separados en
  diferentes elementos.
- *Deterministas*: Los datos siempre mantienen la misma estructura, no cambian según la naturaleza
  de los mismos, necesitando de un parseo condicional para cada conjunto de valores del mismo tipo.
- *Referenciables*: Las diferentes piezas de información cuentan con un nombre embebido, un
  identificador o cualquier cosa que las distinga unas de otras y que pueda emplearse para
  construir una URL y acceder a ellas o como identificador dentro de la misma base de datos de la
  página.
- *Dispersión*: En ocasiones los resultados se encuentran dispersos en varias páginas, a las que es
  necesario acceder escalonadamente, habiendo visitado previamente las anteriores, y elevando así
  el esfuerzo y los puntos de fallos a la hora de obtener un registro completo.

Tras evaluar todas estas cualidades, se decide emplear tres páginas diferentes para la obtención de
la informacíon:

- *itftennis.com*: Página web oficial de la federación de Tenis. Contiene la base de datos
  autoritativa, con todos los resultados legalmente válidos de los diferentes torneos. Su historial
  es bueno, sin embargo el html es muy antiguo, no mantiene identificadores coherentes y la
  navegación es muy complicada.
- *matchstat.com*: Página web de resultados deportivos que mantiene una interfaz limpia y sencilla
  donde casi toda la información se encuentra estructurada en bloques coherentes.
- *abstracttennis.com*: Página amateur de recopilación de resultados de tenis y otra información.
  Mantiene un servicio web que da los resultados en un Javascript razonablemente estructurado,
  evitando el parseo de texto HTML.

Una vez identificadas las fuentes de datos deseadas, se comienza a explorar su HTML y servicios, y
a constuir *arañas* que realicen la doble labor de navegar la página y recopilar los datos. Se
realizan las consultas correspondientes, los mecanismos de limpiado de datos y se organizan los
scrapers son posiciones separadas.

Hasta este punto no se realizan recopilaciones masivas de datos, sino que se van probando los
scrapers de manera individual, modelando los resultados de manera muy laxa en *entidades* que
mantienen los campos sin un tipo forzado ni limpiar.

Las dificultades añadidas de la realización de esta etapa fueron la dispersión de los datos y la
ausencia de un identificador único para cualquier tipo de registro. Especialmente los *Matches*,
los partidos, aún contando con un identificador único otorgado por la federación, no eran
referenciables al no ser mostrados estos en la página oficial y no encontrarse disponibles tampoco
en las otras webs recopilatorias.

Los resultados de esta etapa consisten en un conjunto inconexo de *arañas* web, que eran capaces de
conectarse a una única web y obtener registros con campos variables y no homogéneos.

El período de esta iteración abarca un mes y medio, su inicio se sitúa a mediados de Abril y
termina cuando todas las páginas tienen un scraper funcional a principios de Junio de 2017.

### Extracción de datos y base de conocimiento

La tercera iteración pretendía como objetivo el obtener una base de datos consistente con partidos
de tenis, jugadores, torneos y sus resultados. Para esto era necesario el acceder a las diferentes
fuentes de datos y extraer gran cantidad de ellos, consolidarlos en un solo cuerpo, teniendo en
cuenta las actualizaciones parciales que habría que realizar, debido a que cada registro se
encontraba fragmentado en diferentes sitios.

Para esto se ideó un sistema que pudiese ser ejecutado de manera contínua, basada en un número
variable *crawlers* que ejecutaban uno o más *spiders* (*arañas*), cada uno especializado en un
sitio web diferente.

Diferentes problemas graves que complicaron el desarrollo alargando esta etapa surgieron debido a
la naturaleza de la actividad:

- *Protección contra robots*: Varias de las páginas implementaban protecciones para el acceso
  automático y masivo no autorizado, de manera que el desarrollo se enlenteció sustancialmente, al
  quedar la IP del puesto de desarrollo invalidada en varios servicios durante cierto tiempo. Esta
  necesidad complicó el sistema de obtención de datos y necesariamente alargó el periodo de
  extracción, al necesitar establecer limitaciones al número de conexiones concurrentes, cambiar
  los *User Agent* y emplear *Proxies HTTP*, por lo general mucho más lentos y propensos a errores.

- *Errores en las páginas*: Una buena parte de las referencias encontradas dentro de las bases de
  datos no podían encontrarse luego en ellas. De esta manera, muchas consultas a URLs internas
  encontradas durante la navegación de la página devolvían `404 Not Found` o sencillamente
  contenían datos incorrectos. Hubo que establecer un sistema de reintentos para eludir los fallos
  de los diferentes servicios web, e incluir código que detectaba los registros con datos erróneos
  para esquivarlos.

- *Registros incompletos*: Una gran cantidad de registros no contenía los datos que luego se
  demostraron necesarios para poder hacer una predicción sobre ellos. Esto llevó a hacer el sistema
  de mezcla de registros parciales mucho más sofisticado, y a poner mayor atención sobre la
  completitud de los datos.

Todas estas circunstancias hicieron del sistema de extracción de datos algo mucho más complejo de
lo inicialmente previsto. Todos estos retos técnicos tuvieron que solucionarse sobre la marcha,
intentando consolidar una base de datos fiable y lo más completa posible.

El resultado de esta iteración consiste en un sistema dinámico que es capaz de explorar las páginas
web con las bases de datos sobre Tenis empleando los scrapers diseñados anteriormente, navegando la
página de manera efectiva, paralelizando cuando es posible, evitando las protecciones establecidas
por esas mismas páginas, reaccionando a los errores y a las inconsistencias y sobre todo limpiando
los registros y estableciendo una política de inserción *create or update* que permitiese la
creación de la base de datos consolidada.

El modelado y creación de la base de datos, incluyendo sus migraciones, para hacerla variar a
medida que evolucionaba el software, tomó una cantidad razonable de tiempo también. La gran
variabilidad de los datos, la necesidad de homogeneizar nombres y otros factores hicieron que la
creación de un pipeline de limpiado y persistencia tuviese una complejidad técnica mayor de la
esperada.

Esta base de datos se extrae repetidamente a lo largo de esta etapa y del analisis exploratorio,
para finalmente realizar una desde cero en Septiembre de 2017, se incluye en el proyecto y es sobre
sobre la que se realiza el entrenamiento del modelo y el sistema final.

Este período del desarrollo abarca de principios de Junio a finales de Julio de 2017.

### Análisis exploratorio

Una vez obtenida una base de datos razonablemente completa, es necesario ver en qué consisten los
datos, detectar patrones en ellos y buscar posibles correlaciones que nos indiquen los parámetros
sobre los que basar nuestro sistema de predicción.

Se empieza por un análisis estadístico sencillo que permita revelar las principales características
de los datos. Para ello se analizan los partidos con mayores premios (finales de torneos), los
jugadores más efectivos y se comparan los diferentes campos para ver cuales son más significativos.
Es necesario a veces generar campos nuevos, compuestos a partir de la información contenida en los
demás. Estos nuevos campos a menudo son diferencias: diferencias en altura, ranking, edad, etc...

Para determinar qué parametros son los más efectivos, se analiza la correlación de la variable
*ganar el partido* con todas las demás, incluyendo las propias y la de los contrincantes. Para esto
se genera una matriz de correlación y se determinan las variables que, en solitario o combinadas
pueden resultar más efectivas a la hora de aportar información sobre el resultado del partido.

Esta iteración se ve dificultada por las diversas inconsistencias en los datos originales, lo que
lleva al refinamiento casi constante de los métodos de extracción de datos y del *pipeline* de
limpiado. Se definen enumerados cerrados, ya que se conocen todos los valores para determinados
campos y se toman otras decisiones para homogeneizar nombres y variables. También se decide
prescindir de gran parte de los datos, ya que presentaban registros incompletos que no aportaban la
información suficiente.

El resultado de esta etapa consiste en una lista de parámetros interesantes para realizar la
predicción en un paso posterior. Estos parámetros se derivan del análisis, y son acompañados de
abundante soporte en forma de gráficos y tablas, que serán necesarios posteriormente para redactar
el análisis y justificar las decisiones. Estas gráficas toman un tiempo bastante razonable, una vez
elegidas las herramientas para generarlos a partir de las tools de análisis.

Este período abarcó la parte central del verano, comenzando a finales de Julio y terminando a
finales de Agosto de 2017.

### Construcción del modelo

Una vez obtenida una base de datos completa y analizado su contenido e identificados los
parámetros, fué posible hacer un resumen de los algoritmos candidatos para realizar la predicción,
tomar una implementación de cada uno de ellos y obtener una comparativa.

Los principales candidatos, extraidos de la literatura ya existente sobre predicción de Tenis y
sobre variables booleanas en general a partir de un gran cuerpo de registros, eran los siguientes:

- Árboles de decisión (*decision-trees*)
- K-neighbours
- K-means
- Regresión Lineal (*lineal-regression*)
- Regresión Logística (*logistic-regression*)
- Bosque Aleatorio (*random-forest*)
- Vector de soporte (*support-vector*)

Para todos estos algoritmos, se eligió una implementación basada en Python (Del paquete estadístico
y de machine learning *scikit*), se construyó y entrenó un modelo a partir de los datos, y estos
fueron probados con un conjunto aleatorio de los datos que no había formado parte del
entrenamiento.

De entre todos los algoritmos, el más efectivo con diferencia resultó ser el de
*logistic-regression*. El modelo generado a partir de los datos con este algoritmo alcanzaba
alrededor de un 70% de acierto en la predicción, lo cual es muy razonable, a la vista de los
resultados de otros sistemas de predicción.

Una vez obtenido el modelo, era necesario ampliar el sistema de extracción de datos para que
obtuviese partidos no jugados, realizase una predicción y obtuviese luego los resultados, para
poder obtener una predicción realista a priori. Esto se hizo incluyendo dos tipos nuevos de
*crawler*, que extraían esta información de los diferentes sitios web donde se encontraban los
partidos aún por jugar, y más tarde, los resultados.

El objeto de esta etapa es un modelo entrenado para realizar la predicción de partidos de tenis, y
el sistema que obtiene información sobre nuevos partidos y cómo terminan estos luego.

El período de esta etapa abarcó de finales de Agosto a mediados de Septiembre de 2017, durante el
cual se fueron probando diferentes parámetros y perfeccionando el sistema de extracción.

### Documentación

Aunque no considerada formalmente como una etapa más del desarrollo, ya que no se realizaron
cambios en el software y su documentación asociada, la redacción del análisis y las conclusiones
tomó un tiempo y trabajo considerables, ya que hubo que realizar análisis adicional con el fin de
encontrar si determinadas conclusiones se encontraban debidamente justificadas, y en algunos casos,
repetir partes enteras del análisis al encontrarse estas faltas de base.

La redacción de este documento tomó alrededor de dos meses completos de trabajo, comenzando esta a
finales de Julio y terminando a finales de Septiembre de 2017.

## Calendario

Las diferentes iteraciones a partir de las cuales se realizó el software abarcan cerca de ocho
meses completos, de Febrero a Septiembre de 2017. En esta sección listaremos las diferentes tareas
que se llevaron a cabo y su organización en el tiempo.

### Tareas

1. Investigación y documentación
    1. Evaluación diferentes deportes
    1. Búsqueda de literatura sobre deportes
    1. Búsqueda de literatura *Machine Learning*
    1. Búsqueda de datos disponibles
1. Estudio de las fuentes de datos
    1. Análisis de las fuentes encontradas
    1. Estudio de contenido y el HTML
    1. Parser básico para los sitios
1. Extracción de datos y base de conocimiento
    1. Esquema base de datos
    1. Organización crawlers
    1. Sistema de colas
    1. Pipeline de datos
    1. Sistemas anti protección
    1. Extracción de los datos
1. Análisis exploratorio
    1. Investigación sobre herramientas
    1. Refinado y limpiado de los datos
    1. Análisis estadístico básico
    1. Generación de gráficas y documentación
1. Construcción del modelo
    1. Análisis de los principales algoritmos
    1. Evaluación de las herramientas
    1. Sistema de evaluación y entrenamiento
    1. Comparativa de modelos
    1. Arañas sobre partidos futuros y resultados

### Diagrama de Gantt

![Diagrama de gantt mostrando las tareas realizadas y su tiempo \label{gantt-diagram}](source/figures/gantt-diagram.pdf){ width=100% }
