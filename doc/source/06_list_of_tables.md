# Lista de tablas {.unnumbered}

<!-- 
For me, this was the only drawback of writing in Markdown: it is not possible to add a short caption to figures and tables. This means that the \listoftables and \listoffigures commands will generate lists using the full titles, which is probably isn't what you want. For now, the solution is to create the lists manually, when everything else is finished.
-->

Tabla \ref{table-db-totals} Número de entidades extraidas \hfill{página \pageref{table-db-totals}}  
Tabla \ref{table-data-fields} Campos disponibles en las fuentes de datos \hfill{página \pageref{table-data-fields}}  
Tabla \ref{table-dataset-fields} Campos en el *dataset* de trabajo \hfill{página \pageref{table-dataset-fields}}  
Tabla \ref{table-won-correlation} Correlación de cada parámetro con el objetivo *ganar* \hfill{página \pageref{table-won-correlation}}  
Tabla \ref{table-functional-requirements} Lista de requisitos funcionales \hfill{página \pageref{table-functional-requirements}}  
Tabla \ref{table-non-functional-requirements} Lista de requisitos no funcionales \hfill{página \pageref{table-non-functional-requirements}}  
Tabla \ref{table-data-model-restrictions} Lista de restricciones al modelo de datos relacional \hfill{página \pageref{table-data-model-restrictions}}  

