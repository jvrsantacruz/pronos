# Lista de figuras {.unnumbered}

<!--
For me, this was the only drawback of writing in Markdown: it is not possible to add a short caption to figures and tables. This means that the \listoftables and \listoffigures commands will generate lists using the full titles, which is probably isn't what you want. For now, the solution is to create the lists manually, when everything else is finished.
-->

Figure 4.1  This is an example figure . . .              \hfill{pp}  
Figure x.x  Short title of the figure . . .              \hfill{pp}  

Figura \ref{example-figure} Figura de ejemplo \hfill{página \pageref{example-figure}}  
Figura \ref{tennis-score} Marcador de tenis \hfill{página \pageref{tennis-score}}  
Figura \ref{tournament-drawsheet} Resultado de torneo \hfill{página \pageref{tournament-drawsheet}}  
Figura \ref{Bet365Info} Página de apuestas [@Bet365] \hfill{página \pageref{Bet365Info}}  
Figura \ref{itf-tournaments-page} Página de torneos de la [@ITF] \hfill{página \pageref{itf-tournaments-page}}  
Figura \ref{itf-tournament-detail-page} Detalle de página de torneos de la [@ITF] \hfill{página \pageref{itf-tournament-detail-page}}  
Figura \ref{itf-player-profile} Perfil de jugador en página de la [@ITF] \hfill{página \pageref{itf-player-profile}}  
Figura \ref{tennisabstract-player} Perfil de jugador en la página [@TennisAbstract] \hfill{página \pageref{tennisabstract-player}}  
Figura \ref{matchstat-upcoming} Partidos inminentes en la página [@Matchstat] \hfill{página \pageref{matchstat-upcoming}}  
Figura \ref{data-not-null-fields} Gráfica de campos no nulos en el dataset \hfill{página \pageref{data-not-null-fields}}  
Figura \ref{data-null-fields} Gráfica de campos nulos en el dataset \hfill{página \pageref{data-null-fields}}  
Figura \ref{data-all-matches-per-year} Gráfica total de número de partidos por año \hfill{página \pageref{data-all-matches-per-year}}  
Figura \ref{data-matches-per-year} Gráfica de número de partidos recientes por año \hfill{página \pageref{data-matches-per-year}}  
Figura \ref{data-histogram} Histograma de registros completos \hfill{página \pageref{data-histogram}}  
Figura \ref{data-correlation-matrix} Matriz de correlación de parámetros con *ganar* \hfill{página \pageref{data-correlation-matrix}}  
Figura \ref{data-correlation-lines} Regresión lineal de parámetros con *ganar* \hfill{página \pageref{data-correlation-lines}}  
Figura \ref{model-accuracy} Precisión de los algoritmos de predicción \hfill{página \pageref{model-accuracy}}  
Figura \ref{model-classification-report} Métricas de calidad de los algoritmos de predicción \hfill{página \pageref{model-classification-report}}  
Figura \ref{gantt-diagram} Diagrama de Gantt con la planificación \hfill{página \pageref{gantt-diagram}}  
Figura \ref{use-cases-diagram} Diagrama de casos de uso \hfill{página \pageref{use-cases-diagram}}  
Figura \ref{data-model-diagram} Modelo de datos de la base de conocimiento \hfill{página \pageref{data-model-diagram}}  
Figura \ref{scraping-sequence-diagram} Diagrama de secuencia de la operación extraer datos \hfill{página \pageref{scraping-sequence-diagram}}  
Figura \ref{db-er-diagram} Modelo Entidad-Relación de la base de conocimiento \hfill{página \pageref{db-er-diagram}}  
Figura \ref{architecture-diagram} Diagrama de arquitectura de scrapers \hfill{página \pageref{architecture-diagram}}  
Figura \ref{design-model-diagram} Modelo de Diseño de la aplicación \hfill{página \pageref{design-model-diagram}}  
Figura \ref{scraper-distribution-diagram} Diagrama de distribución de las *arañas* \hfill{página \pageref{scraper-distribution-diagram}}  
Figura \ref{architecture-detailed-diagram} Diagrama detallado de organización de las *arañas* \hfill{página \pageref{architecture-detailed-diagram}}  
Figura \ref{scrapy-pipeline} Diagrama del pipeline de limpiado de datos \hfill{página \pageref{scrapy-pipeline}}  
Figura \ref{spider-historic-diagram} Diagrama de organización de *arañas* \hfill{página \pageref{spider-historic-diagram}}  
Figura \ref{spider-single-diagram} Diagrama de la composición de una única *araña* \hfill{página \pageref{spider-single-diagram}}  
Figura \ref{scraper-loader-diagram} Diagrama de parseo de entidades por las *arañas* \hfill{página \pageref{scraper-loader-diagram}}  

\pagenumbering{roman}
\setcounter{page}{3}

\newpage
