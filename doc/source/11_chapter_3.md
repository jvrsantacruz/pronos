# Predicción

A partir del análisis de los datos que pueden obtenerse referidos a los partidos jugados en
competiciones y torneos, debería sernos posible construir un modelo estadístico que permita
predecir el resultado de un evento no acontecido en base a las características del mismo, con
relativamente poco conocimiento sobre el mismo, basándonos en el entrenamiento sobre datos
históricos.

Estos modelos pueden construirse de muy diferentes maneras, existiendo gran cantidad de variantes,
técnicas y metodologías para obtener este tipo de modelos. Elaboraremos un breve repaso sobre el
estado actual de la predicción de *Tenis*, pasando a analizar las técnicas disponibles y elegir una
a proximación para nuestro conjunto de datos.

## Literatura existente

El estudio sistemático del *Tenis* desde un punto de vista científico con vistas a la predicción de
resultados deportivos ha producido una pequeña cantidad de literatura, de cuya lectura y estudio
hemos podido obtener ideas muy valiosas sobre cómo aproximarnos al problema.

Podríamos dividir estos artículos en dos grandes categorías, basándonos fundamentalmente en el tipo
de información que emplean para realizar las predicciones.

Por un lado tendríamos una serie de estudios que emplean *información muy detallada sobre los
partidos*, sobre la cual pretenden modelar el estilo y el desempeño de los distintos jugadores
durante su juego. Basándose en la idea de que cada jugador posee una serie de rasgos
característicos que dominan su forma de competir, y que estos pueden categorizarse y medirse
cuantitativamente, se suponen los jugadores comparables entre si.

Diferentes modelos han sido planteados empleando *Cadenas de Markov*, por ejemplo en [@Barnett05],
donde se evalúa el resultado de cada punto servido en un partido, realizando su análisis sobre un
único partido muy largo sucedido en el Open de Australia. Esta aproximación, que desarrolla un
modelo jerárquico que permite obtener la probabilidad de que un jugador particular gane un partido,
es posteriormente ampliado por [@OMalley08] y en [@Knottenbelt12], el cual mejora el ratio de
acierto mediante una estrategia denominada *Common-Opponent*, donde únicamente se evalúa el
histórico de puntos de partidos de oponentes comunes en el pasado para un partido dado. Todos estos
artículos elaboran un conjunto de *fórmulas del tenis* que se emplean para predecir el resultado y
analizar el partido en si, con vistas a una mejor comprensión del juego, posibles cambios en las
reglas y aproximación a su divulgación.

Otra aproximación es una más *desinformada*, en la que se tienen pocos o ningún dato sobre la
evolución del partido, y se plantea la predicción sobre el conjunto de datos del torneo, el jugador
y su historial, particularmente su efectividad contra el otro contrincante. Esta aproximación
pretende que es posible predecir el resultado de un jugador en base a su nivel de rendimiento
histórico y características físicas, basándose más en la detección de patrones que indiquen cómo
unos tipos de jugadores desempeñan contra otros en el contexto de un torneo determinado.

Aproximaciones en este sentido han sido tomadas por [@Clarke00], el cual realiza un análisis sobre
los rankings de la ATP, estimando la probabilidad de victoria de los participantes en base a su
diferencia de puntos en el ranking. Su aproximación emplea un modelo de regresión logística,
empleando una única variable. En [@Ma13] se aplica la misma técnica sobre los partidos masculinos
individuales de *Grand Slam*, analizando alrededor de 9.000 partidos, empleando 16 parámetros para
la clasificación. En [@Spiko15] la regresión logística se emplea sobre un conjunto de datos
similar, obteniendo un rendimiento de alrededor del 65%, llegando a calcular el *Retorno de
Inversión* con respecto al valor de mercado de la apuesta en el momento de los partidos. de Otros
algoritmos de inteligencia artificial, como *Redes Neuronales* han sido aplicados por
[@Amornchai09], el cual desarrolló una red neuronal de tres capas empleando el algoritmo de
*Retropropagación* para su entrenamiento, obteniendo buenos resultados en predecir partidos de
*Grand Slam* de los años anteriores.

En general, la efectividad en la predicción es ligeramente superior en las aproximaciones basadas
en regresión logística; el meta estudio [@Kovalchik16] evalúa 11 modelos de predicción sobre
partidos de más de 2000 partidos de la ATP durante 2014, obteniendo que los modelos de regresión
basados en ranking fueron los más precisos. El estudio también incluye conclusiones interesantes,
al mostrar que la mayoría de los modelos obtienen peores resultados para los jugadores con peor
ranking.

## Aprendizaje Automático

El *Aprendizaje Automático* o *Machine Learning* (`ML`), trata de modificar o adaptar las acciones
de sistemas de computación (tales como realizar predicciones o controlar un robot) de manera que
estas acciones sean más precisas, donde la precisión se mide en cómo de bien estas acciones
reflejan las consideradas correctas. Este cae en la frontera entre múltiples disciplinas
académicas, principalmente ciencias de la computación, estadísticas, matemática e ingeniería. Esto
ha sido tanto una ventaja como un problema para el campo, ya que estos grupos no se han comunicado
mucho entre si tradicionalmente [@MLAP].

El `ML` se clasifica normalmente en dos grandes categorías: Aprendizaje *supervisado* y *no
supervisado*, dependiendo si tenemos las respuestas correctas de antemano a la hora de trabajar. 

En el **aprendizaje supervisado** emplea un conjunto de entrenamiento (*training set*) con ejemplos de
las respuestas correctas u objetivo (*target*). Basándose en estas respuestas, el algoritmo debe
*generalizar* para responder correctamente a todas las posibles entradas.

Por contra el **aprendizaje no supervisado** no dispone de las respuestas de antemano, de manera
que el algoritmo intenta identificad similitudes entre las entradas, de manera que las que tienen
algo en común son categorizadas juntas.

## Aproximación

El enfoque empleado a la hora de construir el modelo predictivo de este proyecto ha sido decidido
en base a la literatura ya existente y a su posible utilidad práctica. En este apartado
analizaremos las principales razones para la selección de datos a extraer, algoritmos con los que
trabajar y los parámetros elegidos para realizar la predicción.

Los torneos, partidos y jugadores más importantes atraen la mayor parte de la atención, tanto por
parte del público general y de los entornos de competición, como del mercado de apuestas, sus
clientes particulares. También ese el caso de los estudios científicas anteriormente mencionados;
la mayoría de ellos realizan sus predicciones para partidos de *Grand Slam* o sobre la parte más
alta del ranking de la [@ATP].

Este sesgo hacia la parte más profesional de la competición es fácilmente explicable por su peso
económico, interés social, y, en gran parte, por la gran variedad y abundancia de datos organizados
sobre estos partidos y jugadores, en fuerte contraste con los datos disponibles para la parte baja
de la liga, en torneos tipo *Challenguer* o *Futures*.

Estos partidos menores, realizados en pequeños clubs por jugadores muy jóvenes y principiantes, aún
así reciben atención por parte de las casas de apuestas online, las cuales permiten apostar sobre
su resultado y emiten una predicción sobre el resultado de cada partido. Estas estimaciones se
realizan necesariamente con muy escasos datos, ya que, en ocasiones, ni siquiera las propias
federaciones los tienen.

Es por esto que se consideró particularmente interesante enfocar este proyecto a la predicción de
partidos pequeños, con jugadores noveles, en un contexto de carencia casi absoluta de datos. Esta
restricción creaba condiciones muy especiales.

- Un *número de parámetros limitado* a la hora de estudiar el partido. Mucha información no está
  disponible históricamente, o no se conoce en el momento de realizar la predicción.

- Los jugadores *tienen poco historial*, al haber jugado pocas veces o ninguna, en ocasiones,
  haciendo que el rendimiento histórico de un jugador no tenga tanto peso, al tener que compararlo
  en ocasiones con un contricante completamente oscuro.

- Existe un conjunto de *incentivos* completamente diferente; aunque los premios son menores, pueden
  representar mucho más para jugadores que viven profesionalmente de ello, al contrario que para
  grandes ganadores de la ATP, que por lo general tienen una situación económica desahogada,
  contando con otras fuentes de ingresos, como los beneficios publicitarios.

- Probable existencia de *patrones distintos* a los de las altas ligas. Al ser el nivel de los
  jugadores *muy poco homogéneo*, existe la posibilidad de encontrar sesgos fuertes o patrones que
  permitan mejorar mucho la predicción. Cosa que no es tan sencilla en entornos mucho más
  profesionalizados, donde los jugadores tienen niveles y características mucho más próximas entre
  si.

- Aunque la *información es pobre* para los resultados de estos estos torneos, no incluyendo en
  ningún caso la información del *Ojo de Halcón* ni un desglose de los juegos ganados, al haber una
  gran cantidad de torneos y jugadores, existen una *gran cantidad de registros*.

Viendo que estas restricciones modificaban las condiciones del problema, convirtiéndolo en algo
relativamente distinto y no estudiado por la literatura existente, se decidió enfocar el modelo en
la predicción de estos pequeños partidos, empleando las técnicas que más se adecuasen a sus
características.

## Limitaciones

Los algoritmos de *Aprendizaje Automático* no vienen sin una serie de riesgos que pueden afectar al
modelo resultante, limitando su utilidad práctica, o incluso invalidándolo para cualquier uso fuera
de los datos de entrenamiento. Por este motivo, cuidado debe tomarse para evitar una serie de
propiedades inconvenientes.

La *elección de los parámetros* tiene una gran cantidad de implicaciones y determina en gran
medida la calidad de la predicción por parte del modelo. Esta se encuentra enormemente limitada por
la cantidad de datos disponible, de manera que puede que un dato determinante no esté disponible en
la abundancia necesaria, limitando la efectividad del algoritmo.

Dependiendo del algoritmo y de cómo se realice el entrenamiento, puede que el modelo se encuentre
*no generalice*, ajustándose demasiado a los datos de entrenamiento, de tal forma que al
presentar modelos reales, no sea capaz de predecir con efectividad, viéndose muy afectado por el
ruido. Este fenómeno también es denominado *overfitting*.

En casos en los que los datos de entrenamiento empleados no son lo suficientemente representativos
de la población general sobre la que queremos realizar la predicción, puede producirse casos de
*optimización*, donde el modelo se encuentra sesgado hacia determinados patrones presentes en el
entrenamiento, que luego no corresponden con los presentados en su uso, provocando una caida de la
efectividad en la predicción.
