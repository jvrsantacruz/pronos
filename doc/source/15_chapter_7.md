# Implementación

En el presente capítulo presentaremos y describiremos en mayor detalle los diferentes componentes
software de los que se compone el proyecto. Esto incluye la arquitectura general del sistema, el
almacenamiento de datos, las estructuras empleadas y las interfaces diseñadas para tal fin.
Detalles adicionales, incluyendo las estrategias empleadas, así como los diferentes retos
técnicos a superar y las tecnologías de terceros incluidas.

Los diferentes componentes del proyecto, cuyo nombre clave es `pronos`, aunque forman parte de un
todo relacionado, decidimos abordarlos por separado; primero el modelo de datos, parte
conceptualmente central a un proyecto basado en análisis de estos. Los sistemas de obtención de
esos datos, cada uno con sus diferentes objetivos, arquitectura y funcionalidad. La implementación
del modelo predictivo, descrito en el capítulo *Datos*, comentando los fragmentos relevantes y las
diferentes decisiones técnicas.

## Componentes

Podríamos dividir el proyecto conceptualmente en las siguientes partes o componentes:

- **Modelo de datos**: Diseño de la base de datos, implementación, modelo de transacciones,
  estrategia de migraciones.

- **Arañas**: Sistema de obtención de datos, software de red, limpiado de la información,
  distribución de tráfico, tratamiento de las fuentes.

- **Modelo predictivo**: Selección de datos, homogeneización y preparación, entrenamiento y
  herramientas de análisis, modelo final.

- **Despliegue**: Empaquetado de la aplicación, sistema de instalación, gestión de configuración.


Cada uno de los puntos anteriores corresponderá a un bloque de este capítulo, y en el se
justificarán las decisiones técnicas y resumirán los detalles de implementación relevantes para
comprender el funcionamiento del proyecto `pronos`.

## Modelo de datos

El modelo de datos es una abstracción que permite organizar información de manera estructurada, con
el fin de estandarizar cómo los datos se relacionan unos con otros, qué representan como entidades
en el mundo real y cómo puede accederse a ellos [datamodel2011].

A partir del diseño de nuestro modelo de datos (ver capítulo *Análisis y Diseño*), en el cual se
establecen las entidades, los datos y su tipo, creamos la definición de la base de datos. A través
del diagrama Entidad-Relación, expresamos cómo este modelado se transforma en tablas y relaciones
foráneas en una base de datos [requirements2003], comentaremos las decisiones de diseño, su grado
de normalización y el proceso de diseño, evolución y migración.

El proyecto requiere de una gran cantidad de datos, pero sin embargo los campos necesarios no son
muchos ni especialmente complejos. El perfil de un jugador, `Player`, se basa en su nombre
normalizado como identificador único, y contiene la información más importante del proyecto, al
encontrarse gran parte del modelo basado en el campo `ranking`. Para cada jugador se almacena su
país de origen, peso y altura, sexo y si es diestro o zurdo. Cada jugador además se encuentra
relacionado con todos los partidos que ha jugado, cuya información se modela como la entidad
`Match`. Cada partido incluye la fecha, el tipo de terreno, el lugar donde se juega y la ronda
dentro del torneo o `Event` del cual forma parte en la competición. Estos indican también el tipo
de terreno de juego, el lugar, las fechas y, sobre todo, el premio objetivo. Cada partido además,
cuando ha sido jugado, puede incluir su resultado en la entidad `Result`, donde se indica el
marcador del partido, el jugador ganador y, opcionalmente, las predicción realizada.

El diagrama *Entidad-Relación* de la base de datos, que puede verse en la figura[^7.1]
\ref{db-er-diagram} contiene sus tablas, campos, relaciones y tipos de datos relacionados
[eralchemy].

![Diagrama Entidad-Relación de la base de datos de *pronos* \label{db-er-diagram}](source/figures/db-er-diagram.pdf){ width=100% }

Existen restricciones de unicidad sobre los nombres de los jugadores, los eventos y partidos:

------------------------------------------------------------
Entidad      Campos        Restricción
--------     -------       ------------
*Player*     Name          Unique

*Event*      Name, Year    Unique

*Match*      Name, Event,  Unique
             Player1,
             Player2

*Result*     Match         Unique

------------------------------------------------------------

Tabla: \label{table-data-model-restrictions} Lista de restricciones al modelo de datos relacional.

Cada entidad, sin embargo, mantiene su propia clave primaria en forma de identificador numérico
autoincremental, que es el elegido para establecer las relaciones entre los diferentes campos.

La tabla `Upcoming` hace referencia a los partidos por venir, que aún no han sido procesados y se
encuentran en cola para obtener su resultado.

### Sqlite

La base de datos elegida para implementar la base de datos del proyecto es [sqlite]. Se trata
de un motor de bases de datos embebido. A diferencia de la mayoría de motores de bases de datos, es
ejecutado dentro del mismo proceso de la aplicación, de manera auto contenida, permitiendo su uso
sin configuración ni infraestructura dedicada de ningún tipo.

El funcionamiento de *SQLlite* se basa en la lectura y escritura de ficheros en disco,
implementando un sistema completo de SQL, incluyendo triggers y vistas, en un solo fichero en
disco. La implementación se encuentra basada en C y es multi-plataforma.

La elección de este motor de bases de datos para el proyecto se basa en su ligereza y sencillez,
permitiendo un desarrollo iterativo muy ágil, sin necesidad de establecer servidores dedicados, ni
de utilizar herramientas específicas para la configuración del servidor de base de datos, sus
backups y mantenimiento.

La decisión que se tomó al inicio del proyecto fué emplearla durante la fase de desarrollo, pero
manteniendo una capa de compatibilidad (`sqlalchemy`, ver el siguiente punto), de manera que
pudiese cambiarse de base de datos con facilidad, sin necesidad de reescribir absolutamente nada de
la capa de datos.

Una posible preocupación, al emplear un motor tan sencillo, es si cumple con todas las
características necesarias para implementar las funcionalidades requeridas por el proyecto. Estas
incluyen, o podrían incluir en ampliaciones próximas:

- Transacciones ACID
- Persistencia de los datos
- Sistema de backup
- Triggers

Todas estas características se cumplen, el motor de `sqlite` es ACID, la persistencia se realiza en
disco, y por tanto, el sistema de backup es tan sencillo como copiar el fichero, acción que puede
realizarse en cualquier momento sin necesidad de bloquear la base de datos. Los triggers son una
funcionalidad deseada, ya que sería posible modelar parte de la funcionalidad empleándolos, de
manera que en ampliaciones a corto plazo del software, no sería necesario cambiar de motor de base
de datos.

El rendimiento también puede ser un asunto importante. Se decidió evaluar el rendimento durante el
desarrollo, y cambiar a un motor más avanzado como *Mysql* o *PostgreSQL*, en caso de ser necesaria
un rendimiento mejor, contando con un servidor de bases de datos dedicado. Sin embargo este no ha
sido el caso; las consultas más intensivas, donde prácticamente intervienen todos los campos de la
base de datos a través de relaciones y más de un millón de filas, se encuentran por debajo del
límite marcado de dos segundos.

### ORM

Un *Object Relational Mapper* [fowlerpatterns] consiste en una técnica que permite emplear sistemas
de datos incompatibles a través del uso de orientación a objetos, creando una base de datos basada
en objetos que es traducida a operaciones relacionales en una base de datos de fondo.

En el caso de `pronos`, de manera interna la aplicación modela la base de datos empleando el
framework [sqlalchemy]. De esta manera, se definen varias clases, cada una basada en una tabla
diferente, proveyendo de un reflejo de la misma de manera interna.

Por ejemplo, la clase `Match`, que representa a un partido, es modelada de la siguiente manera:

\newpage

```python
class Match(Base):
    __tablename__ = 'matches'

    id = Column(Integer, primary_key=True)
    code = Column(String(), nullable=True)
    round = Column(String(), nullable=False)
    gender = Column(String(), nullable=False)
    event_id = Column(Integer(), ForeignKey(Event.id))
    date = Column(DateTime(), nullable=True)
    surface = Column(String(), nullable=True)

    player1_id = Column(Integer(), ForeignKey(Player.id))
    player2_id = Column(Integer(), ForeignKey(Player.id))

    event = relationship(Event, uselist=False)
    player1 = relationship(Player, uselist=False, foreign_keys=[player1_id])
    player2 = relationship(Player, uselist=False, foreign_keys=[player2_id])
    result = relationship('Result', uselist=False)

    def __str__(self):
        return '{} vs {} at {}'.format(
            self.player1, self.player2, self.event
        )

    def __repr__(self):
        return 'Match(event={self.event},player1={self.player1},'\
            'player2={self.player2})'.format(self=self)
```

Donde pueden observarse los tipos básicos que se encuentran en la tabla (`id`, `code`, `round`,
`gender`, `date`, `surface`), las claves foráneas (`player1_id`, `player2_id`, `event_id`) y
relaciones, una ficción orientada a objetos que permite modelar las relaciones entre tablas a modo
de colección (una lista, en este caso), permitiendo sintaxis como la siguiente:

```python
all_player_events = set(match.event for match in match.player1.matches)
```

Aunque por razones de eficiencia, las consultas a la base de datos se realizan de manera mixta,
construyendo las consultas en un modelado de SQL que implementa la capa más baja de SQLAlchemy,
especificando cada join, pero obteniendo como resultado un objeto completo. Por ejemplo:

```python
session.query(Match)\
    .options(
        joinedload(Match.event),
        joinedload(Match.player1),
        joinedload(Match.player2),
        joinedload(Match.result)
    )\
    .join(Match.event)\
    .join(Match.result)\
    .join(player1, Match.player1)\
    .join(player2, Match.player2)\
    .filter(Match.date != None)\
    .filter(Event.prize != None)\
    .filter(Match.surface != None)\
    .filter(player1.born != None)\
    .filter(player1.ranking != None)\
    .filter(player2.born != None)\
    .filter(player2.ranking != None)\
    .filter(Result.match_id == Match.id)\
    .filter(Result.match_id == Match.id)\
    .filter((Match.date - player1.born) < 50)\
    .filter((Match.date - player2.born) < 50)\
    .filter(Match.date >= datetime.datetime(1995, 1, 1))
```

En esta puede observarse una consulta a la base de datos, que filtra partidos en base a atributos
de los jugadores implicados, si tiene resultado o no y la cantidad del del premio del evento. El
código equivalente empleando la capa de ORM sería algo similar al siguiente listado:

\newpage

```python
for match in session.query(Match):
    if match.date is None or match.surface is None:
        continue

    (..)

    if match.player1 is None or match.player2 is None:
        continue

    if match.event.prize is None:
        continue

    if match.result is None:
        continue
```

La cual, salvo bajo existencia de configuración particular de la sesión de base de datos, lanzaría
una subquery por cada acceso a propiedades a través de una relación, haciendo de la consulta
completa, que debe recorrer más de un millón de registros, algo terriblemente lento que además
bloquearía la base de datos durante gran parte del mismo.

### Migraciones

El diseño evolutivo de las bases de datos es un concepto relativamente nuevo, el cual permite que
en lugar de planificar por completo el esquema de una base de datos antes de comenzar la
aplicación, este se construya a través de la vida de la misma, comenzando con un modelo muy
sencillo, y añadiendo campos conforme el desarrollo va requiriéndolo, y los requisitos evolucionan
[ambler2006refactoring].

Las ventajas de este enfoque son evitar el sobrediseño, evitando incluir cosas que no son
necesarias en la base de datos; se elimina el riesgo de tener que realizar grandes reescrituras, ya
que no se avanza en la aplicación hasta el punto de no tener más remedio que rehacer grandes
partes.

Además, adquiriendo este enfoque, se establece infraestructura que permite modificar el esquema de
la base de datos de forma ordenada, haciendo futuros cambios y la evolución de la aplicación mucho
más rápida y sencilla. A estas alteraciones del esquema de la base de datos, que aplicadas
secuencialmente llevan a la misma desde el origen hasta su estado de producción, las conoceremos en
el proyecto como `migraciones`.

En este caso las migraciones se han realizado empleando la herramienta [alembic], basada
también en SQLAlchemy. Para definir las migraciones, se crean una serie de *scripts de migración*
en el directorio `versions` del proyecto, cada uno identificado con un hash único. Estos script
interactúan con la base de datos, realizando diversas modificaciones en su esquema. Al ejecutarlos
de manera ordenada, son capaces de construir el esquema de la base de datos desde cero, hasta su
configuración final.

## Arañas

Se conoce como *araña* a una pieza de software que recorre páginas Web a través de sus hiper
enlaces, extrayendo información de estos sitios de manera estructurada en el camino.

A la hora de obtener los datos necesarios para entrenar el modelo y realizar las predicciones, fué
necesario contar con varias fuentes de datos diferentes, cuya información exponían en formato web,
pensado originalmente para consumo humano.

A partir de esos datos, obtenidos de manera distribuida, se conforma una base de datos consolidada
sobre la cual se realizan los análisis.

En esta sección analizaremos cómo se accede a esta información, cómo se distribuyen los datos desde
las arañas hacia el sistema central, la aproximación al *scraping* y las dificultades técnicas para
acceder a estas páginas.

![Esquema simplificado de arquitectura del sistema. En el pueden verse los diferentes componentes y cómo se comunican entre ellos de manera esquemática. \label{architecture-diagram} ](source/figures/architecture-diagram.pdf){ width=80% }

### Arquitectura

El sistema completo se encuentra formado por cuatro componentes principales, como puede verse en el
esquema de la figura \ref{design-model-diagram} existen cuatro componentes principales diferentes
que conforman la aplicación: las *Arañas*, el *Updater*, la *Base de Datos* y el sistema de
*Predicción*.

![Diseño de clases del sistema. En ella se representan mediante clases cada una de las reponsabilidades del sistema. Se separa conceptualmente el modelado de las clases que representan la base de conocimiento de las clases funcionales que las emplean. \label{design-model-diagram} ](source/figures/design-model-diagram.pdf){ width=100% }

![Esquema general de distribución de los crawlers, scrapers y su comunicación entre ellos. Los crawlers pueden ser procesos o hilos independientes, cada uno conteniendo una serie de scrapers no bloqueantes para un sitio web concreto. Los resultados son procesados por un pipeline que los limpia y organiza, los distribuye entre otros scrapers y los persiste pasándolos a una cola general que los lleva al Updater, donde se almacenan. \label{scraper-distribution-diagram}](source/figures/scraper-distribution-diagram.pdf){ width=100% }

1. *Arañas*: Se trata de workers distribuidos que realizan la comunicación por red, el análisis y
    parseo de la información y la distribuyen de forma estructurada al resto de la aplicación.
    Existen diferentes tipos de araña, las cuales obtienen diferentes tipos de datos necesarios
    para la generación del modelo y la predicción. Cada proceso de *Araña* se encuentra compuesto
    por al menos un *crawler*, el cual contiene una serie de *scrapers*, que emplean *loaders* para
    parsear fragmentos de los documentos que obtienen de los servidores, y los persisten y
    distribuyen via un *pipeline* a través de colas, como puede verse en la figura
    \ref{scraper-distribution-diagram}.

2. *Updater*: Recibe los datos de las Arañas, los pone en contexto, resuelve referencias y persiste
    esta información debidamente relacionada en la base de datos. La labor de este componente es
    limpiar y homogeneizar todo lo que el *pipeline* de arañas no haya sido capaz de realizar ya
    así como evitar la duplicación de información.

3. *Base de Datos*: La capa de base de datos permite persistir y relacionar todos los datos en el
    esquema provisto, evitando la duplicación y relacionando todos los registros de manera que sean
    útiles posteriormente. Sus contenidos pueden verse en la figura \ref{design-model-diagram}.

4. *Predicción*: El análisis de datos que obtiene un modelo funcional listo para empezar a predecir
   partidos.

Los procesos implicados en los cuatro componentes se comunican y sincronizan entre si mediante
colas. Las *Arañas* se encuentra denominado en plural por tratarse realmente de un número
configurable de workers que a su vez mantienen una serie de *hilos ligeros* mediante un bucle de
eventos, uno para cada sitio. En el diagrama de la figura \ref{architecture-detailed-diagram}
puede verse esta situación con algo más de detalle.

![Esquema de arquitectura del sistema \label{architecture-detailed-diagram} ](source/figures/architecture-detailed-diagram.pdf){ width=100% }

Cada Spider realiza una serie de peticiones HTTP que proporcionan datos, que son estructurados en
*items* y pasados al *pipeline* para su procesamiento y persistencia, así como nuevos enlaces web,
los cuales permiten lanzar próximas peticiones y navegar la base de datos expuesta en la web. Esta
información es pasada a su vez a un *pipeline* común, que realiza las labores de homogenización de
los datos mediante un limpiado (*data cleaning*) y también persiste las diferentes entidades que se
vayan conformando a partir de los datos fragmentarios obtenidos. El proceso completo puede verse en
la figura \ref{scrapy-pipeline}.

![Flujo de obtención de información de una araña. Las arañas obtienen documentos HTML y Javascript de los servicios web consultados, estos son parseados, los enlaces a otras páginas con información son procesados y se realizan nuevas peticiones. Las entidades extraidas del documento son pasadas para su limpieza y persistencia. \label{scrapy-pipeline}](source/figures/scrapy-pipeline.pdf){ width=100% }

Las arañas se organizan a su vez en varios procesos diferentes denominados *Crawlers*. Estos
*crawlers* ejecutan un conjunto de scrapers especializados cada uno en un único sitio web dentro
del mismo proceso. De esta manera, pueden lanzarse varios procesos, los cuales tienen un número
configurable de workers dedicados cada uno a una web específica. Cada *crawler* mantiene dos colas,
una cola *interna* que permite la comunicación entre sus spiders y otra cola de *salida*, donde se
van siendo almacenados los resultados, que son posteriormente consumidos por el *Updater* como
puede verse en la figura \ref{scrapy-pipeline}.

La implementación se encuentra realizada empleando el framework de scraping [@scrapy], este a su
vez se basa en el framework de programación de red asíncrono [@twisted]. Este framework emplea una
aproximación no-bloqueante, basada en un bucle de eventos en el que se registran callbacks que son
ejecutadas en un solo hilo. Esto permite que un solo proceso/hilo de *crawler* incluya múltiples
*arañas* que se ejecutan concurrentemente pero sin crear paralelismo. Al ser un software
fuertemente limitado por la latencia y tiempos de espera de la red, este esquema permite realizar
una gran cantidad de trabajo simultáneo sin necesidad de emplear mecanismos tradicionales de
control de concurrencia y simplificando la gestión del estado y los accesos a base de datos.

Las arañas disponibles dentro de un proceso worker son seis diferentes, cada una con un propósito
específico:

1. *MainSpider*: Obtiene los partidos del archivo histórico de matchstat, distinguiendo por año y
   series masculina/femenina.
1. *ITF Tennis*: Obtiene información completa a partir de un nombre de jugador desde la web oficial
   de la ITF. [@ITF]
1. *Matchstat*: Dado un nombre de jugador, extrae su historial completo y el de sus contrincantes
   de manera recursiva desde la web de Matchstat. [@Matchstat]
1. *Abstract Tennis*: Obtiene información histórica de un jugador concreto y el de sus
   contrincantes.
1. *Upcoming*: Obtiene lista de partidos que aún no han sido jugados y su información asociada.
1. *Result*: Obtiene resultados de partidos ya jugados de los que se encuentran en la base de
   datos.

Se trata de un sistema jerárquico donde existe una *Araña madre* que pasa información a las demás y
dirige qué datos son los que deben obtener de cada sitio los workers. En la figura
\ref{spider-historic-diagram} se muestra esta situación.

En los siguientes apartados iremos describiendo el funcionamiento de las diferentes arañas
disponibles, la información que obtienen y cómo se realiza la comunicación entre ellas y el
*Updater*.

### Partidos históricos

![Esquema de comunicación entre arañas para partidos históricos. La araña principal envía identificadores de jugador a los otros scrapers de manera que obtengan su historial completo de manera recursiva, mientras la principal sigue obteniendo los datos del campeonato que se encuentra analizando. \label{spider-historic-diagram}](source/figures/spider-historic-diagram.pdf){ width=100% }

El componente principal *MainSpider* recorre un archivo histórico de partidos jugados, los nombres
de los jugadores y los envía a las demás *spiders* para que obtengan recursivamente una información
más detallada sobre el historial completo de los mismos y de sus contrincantes.

Estas otras *spiders* son *ITF Tennis*, *Matchstat* y *Abstract Tennis*. Estas obtienen los
jugadores, inspeccionan su perfil, obtienen todos los partidos de su historial, e introducen a sus
contrincantes desconocidos en la cola. Este mecanismo permite que el historial completo de los
jugadores obtenidos por *MainSpider* se obtenga, incluyendo toda la información disponible,
explorando la base de datos completa a partir de los jugadores, que son el punto inicial. En la
figura \ref{spider-single-diagram} puede verse la configuración de un único *crawler* con esta
configuración.

![Esquema de spiders para un solo proceso. Indica los documentos que son analizados para extraer la información sobre partidos y jugadores, y la distribución de esa información. \label{spider-single-diagram}](source/figures/spider-single-diagram.pdf){ width=100% }

Cada *Spider* mantiene una secuencia de peticiones HTTP a partir de las cuales obtiene la
información. Ciertas peticiones se encuentran supeditadas a las anteriores debido a que la
información se encuentra en un documento previo, mediante identificadores, nombres o enlaces.
Todos los datos obtenidos por la etapa son tomados por el *Updater*, el cual realiza
actualizaciones parciales de los objetos a partir de la información extra que recibe,
completándolos.  A continuación describiremos la secuencia de peticiones que realiza cada araña y
los datos que obtiene en cada etapa:

#### MainSpider

Este *spider* se centra en obtener los eventos que han sucedido en un año o en un conjunto de años.
Permite distinguir entre partidos masculinos y femeninos, ya que se encuentran separados en las
distintas páginas de información, y la obtención de sus datos sigue un flujo diferente.
Los resultados de esta web se encuentran paginados, de manera que múltiples peticiones son
necesarias para obtener los datos completos de cada una.

- Petición 1
    Punto de entrada: `https://matchstat.com/tennis/calendar`
    Parámetros: Conjunto de años, masculino/femenino/ambos
    Resultados:
    - Lista de URL con los torneos del año correspondiente

- Petición 2
    Punto de entrada: *URL competición matchstat*
    Parámetros: URL de la competición, Nombre de la competición
    Resultados:
    - Lista de URL con los jugadores que participan
    - Lista de *Event* con los partidos que se realizan
    - Lista de *Match* con los partidos que se realizan
    - Lista de *Player* que participan
    - Lista de *Result* para los partidos que tienen resultado

Todas las entidades obtenidas son pasadas a un *pipeline* especial, el cual no solo limpia y envía
los datos al *updater*, sino que además los incluye en una cola interna del *crawler* de manera que
el resto de *spiders* obtienen esta información.

#### AbstractSpider

El *spider* se especializa en obtener el histórico completo a partir de un nombre de jugador. El
sistema accede al perfil de ese jugador, obtiene todos los torneos y partidos que ha jugado, y
sigue buscando recursivamente a partir de los nombres de los jugadores a los que se ha enfrentado.

Este scraper no se basa estrictamente en el contenido visto por el usuario. Tras inspeccionar la
página detenidamente, se obtuvo acceso al *backend* que provee de los datos. Esta especie de *API*
primitiva consiste en determinadas URLS, a las que a partir de un nombre de jugador o un número de
partido es posible obtener un HTML con código Javascript embebido, que incluye todos los datos.

- Petición 1
    Punto de entrada: `http://tennisabstract.com`
    Parámetros: Nombre jugador
    Resultados:
    - Lista de URL con los jugadores que participan
    - Lista de *Event* con los partidos que se realizan
    - Lista de *Match* con los partidos que se realizan
    - Lista de *Player* con los partidos que se realizan
    - Lista de *Result* con los partidos que se realizan

```
# Datos para jugadora Natalija Kostic

    var fullname = 'Natalija Kostic';
    var lastname = 'Kostic';
    var currentrank=577;
    var peakrank=398;
    var peakfirst=20120924;
    var peaklast=20120924;
    var dob=19940725;

    var hand='R';
    var backhand='2';
    var country='SRB';
    var shortlist=68;
    var careerjs=1;
    var active=1;
    var lastdate=0;
    var twitter='';
    var current_dubs="UNR";
    var peak_dubs="UNR";
    var peakfirst_dubs="";
    var liverank=571;
    var chartagg=0;
    var photog='';
    var photog_credit='';
    var photog_link='';
    var itf_id='100133362';
    var wta_id='16453/title/natalija-kostic';
    var fc_id='';
    var wiki_id='';
    var ychoices=["Time Span", "Last 52", "Career", "2017", "2016", "2014", "2013", "2012", "2011", "2010", "2009"];
var tchoices=["Event", "All", "Alkmaar 10K", "Antalya 10K", "Antalya Kaya Belek 10K", "Aschaffenburg 25K", "Bad Saarow 10K", "Baja $25K", "Belgrade 10K", "Benicarlo 25K", "Braunschweig 10K", "Brcko 10K", "Budapest 10K", "Caserta 25K", "Darmstadt 25K", "Dobrich 10K", "Dobrich 25K", "Dubrovnik 10K", "Essen 10K", "Hammamet $10K", "Hammamet $15K", "Hechingen 25K", "Heraklion $15K", "Heraklion 10K", "Horb 10K", "Hua Hin $10K", "Hua Hin $25K", "Imola 25K", "Istanbul 10K", "Moscow $K", "Nis $10K", "Nis 10K", "Palmanova Mallorca 10K", "Pirot 10K", "Podgorica 50K", "Pomezia 10K", "Prokuplje $10K", "Prokuplje 10K", "Santa Margherita Di Pula $25K", "Sharm El Sheikh 10K", "Stare Splavy $25K", "Stuttgart Vaihingen 25K", "Tunis 25K", "Umag 10K", "Velenje 10K", "Vrnjacka Banja 10K", "Wiesbaden 10K", "Zajecar 10K"];
var cchoices=["vs Country", "All", "ARG", "AUS", "AUT", "BEL", "BIH", "BLR", "BRA", "BUL", "CAN", "CHN", "CRO", "CZE", "DEN", "EGY", "ESP", "FRA", "GBR", "GEO", "GER", "GRE", "HKG", "HUN", "IND", "ISR", "ITA", "JPN", "KAZ", "KOR", "LIE", "MDA", "MKD", "MNE", "NED", "POL", "ROU", "RUS", "SLO", "SRB", "SUI", "SVK", "SWE", "THA", "TPE", "TUN", "TUR", "UKR", "USA", "UZB"];
var rchoices=["as Rank", "All"];
var ochoices=["Julia Stamatova", "Chantal Skamlova", "Lulu Radovcic", "Julyette Maria Josephine Steur", "Jade Suvrijn", "Barbara Kotelesova", "Valentini Grammatikopoulou", "Valeriya Strakhova", "Tamara Tomic", "Sandra Zaniewska", "Marina Lazic", "Cristina Ene", "Anita Husaric", "Vinciane Remy", "Varunya Wongteanchai", "Tingting Pei", "Tena Lukas", "Tamara Arnold", "Tamachan Momkoonthod", "Sowjanya Bavisetti", "Satsuki Takamura", "Sandra Samir", "Sandra Maletin", "Sabrina Bamburac", "Rebeca Pereira", "Rayen Turki", "Ramu Ueda", "Plobrung Plipuech", "Petra Uberalova", "Petra Januskova", "Olga Ianchuk", "Nika Kukharchuk", "Natela Dzalamidze", "Naiktha Bains", "Myrtille Georges", "Mizuno Kijima", "Mirabelle Njoze", "Michele Alexandra Zmau", "Margaux Bovy", "Manisha Foster", "Luisa Marie Huber", "Lucia De La Puerta Uribe", "Lisa Sabino", "Lisa Ponomar", "Lina Gjorcheska", "Kwan Yau Ng", "Komola Umarova", "Katharina Lehnert", "Kassandra Davesne", "Karin Kennel", "Jelena Simic", "Isabella Shinikova", "Ioana Diana Pietroiu", "Haruna Arakawa", "Hanyu Guo", "Giulia Crescenzi", "Gaia Sanesi", "Federica Arcidiacono", "Ema Polic", "Elizaveta Ianchuk", "Dalila Spiteri", "Dabin Kim", "Claudia Coppola", "Chieh Yu Hsu", "Caroline Uebelhoer", "Bunyawi Thamchaiwat", "Audrey Albie", "Arina Taluenko", "Anna Makhorkina", "Angelica Moratelli", "Alexandra Perper", "Agnes Bukta", "Xenia Knoll", "Victoria Bosio", "Veronika Kudermetova", "Vendula Zovincova", "Vanda Lukacs", "Steffi Distelmans", "Stamatia Fafaliou", "Roos Van Der Zwaan", "Raluca Elena Platon", "Polona Rebersak", "Petra Krejsova", "Olga Saez Larra", "Olga Parres Azcoitia", "Nikola Vajdova", "Nevena Selakovic", "Natalie Cvackova", "Natalia Nikolopoulou", "Martina Basic", "Marija Mastilovic", "Lenka Jurikova", "Lena Ruppert", "Ksenia Lykina", "Keren Shlomo", "Katarina Adamovic", "Karolina Muchova", "Julia Samuseva", "Julia Payola", "Julia Grabher", "Jovana Jaksic", "Jelena Stojanovic", "Irina Maria Bara", "Ingrid Alexandra Radu", "Gabriela Horackova", "Esen Kokce", "Elizabeta Bauer", "Ekaterine Gorgodze", "Diana Sumova", "Damira Muminovic", "Dalia Zafirova", "Dajana Dukic", "Cristina Adamescu", "Bojana Marinkovic", "Bojana Cecez", "Bibiane Schoofs", "Barbora Krejcikova", "Barbara Haas", "Anastasiya Rychagova", "Zuzana Linhova", "Zsofia Susanyi", "Yana Mogilnitskaya", "Vladica Babic", "Vishesh Vkc", "Virag Nemeth", "Vinja Lehmann", "Viktoriya Tomova", "Viktoria Kuzmova", "Valeria Podda", "Tjasa Srimpf", "Tereza Mrdeza", "Tanya Samodelok", "Tamara Curovic", "Sultan Gonen", "Sonja Larsen", "Silvia Njiric", "Sara Sussarello", "Sandra Martinovic", "Quirine Lemoine", "Polina Vinogradova", "Polina Monova", "Pei Chi Lee", "Patricia Lancranjan", "Nonna Kurginyan", "Nicolette Van Uitert", "Neda Koprcina", "Natasa Zoric", "Natalia Ryzhonkova", "Melis Sezer", "Martina Kubicikova", "Martina Gledacheva", "Martina Caciotti", "Martina Borecka", "Marina Kachar", "Marie Benoit", "Maria Mokh", "Margarita Gasparyan", "Mailen Auroux", "Lucia Butkovska", "Lisa Matviyenko", "Linda Dubska", "Lilla Barzo", "Laura Siegemund", "Laura Ioana Andrei", "Klaudia Boczova", "Klaartje Liebens", "Kathinka Von Deichmann", "Katerina Vankova", "Karen Barbat", "Jesika Maleckova", "Jelena Lazarevic", "Ivana Jorovic", "Iva Mekovec", "Iryna Shymanovich", "Indy De Vroome", "Indire Akiki", "Hulya Esen", "Gizem Melisa Ates", "Gioia Barbieri", "Gabriela Porubin", "Florencia Molinero", "Evelyn Mayr", "Eugeniya Pashkova", "Eugenie Chapelle", "Estelle Guisard", "Erin Routliffe", "Eri Hozumi", "Ellen Allgurin", "Elizaveta Kulichkova", "Ekaterina Semenova", "Dunja Sunkic", "Diana Buzean", "Denisa Allertova", "Dejana Radanovic", "Danka Kovinic", "Danijela Tomic", "Csilla Borsanyi", "Clothilde De Bernardi", "Christina Makarova", "Celine Cattaneo", "Catherine Chantraine", "Carina Witthoeft", "Bernarda Pera", "Barbara Sobaszkiewicz", "Anna Veleva", "Anna Klasen", "Anna Floris", "Angelique Van Der Meet", "Andjela Novcic", "Anastasia Vdovenco", "Anastasia Stepu", "Anastasia Rudakova", "Anamika Bhargava", "Ana Jovanovic", "Ana Bogdan", "Alina Wessel", "Alexandra Damaschin", "Alexandra Artamonova"];
var tdates=["20090629", "20100705", "20101004", "20101018", "20110404", "20110620", "20110627", "20110704", "20110926", "20111003", "20111017", "20111031", "20111107", "20120206", "20120213", "20120409", "20120430", "20120507", "20120514", "20120618", "20120625", "20120702", "20120709", "20120716", "20120820", "20120827", "20120903", "20120917", "20121015", "20121022", "20121105", "20121119", "20121126", "20121203", "20130121", "20130128", "20130311", "20130318", "20130325", "20130520", "20130610", "20130617", "20130624", "20130701", "20130708", "20130715", "20130805", "20130902", "20130909", "20130916", "20131007", "20131014", "20131021", "20131028", "20131118", "20131125", "20131202", "20140217", "20140224", "20140317", "20140324", "20140505", "20140519", "20140602", "20140707", "20140721", "20140804", "20140811", "20140901", "20140908", "20141013", "20141020", "20141027", "20141103", "20141110", "20141117", "20141222", "20160411", "20160418", "20160425", "20160516", "20160523", "20160530", "20160704", "20160711", "20160718", "20160919", "20161003", "20161010", "20161017", "20161121", "20161128", "20170306", "20170313", "20170320", "20170424", "20170508", "20170515", "20170522", "20170605", "20170619", "20170710"];
var vranks=["159", "171", "200", "209", "253", "256", "271", "275", "279", "296", "299", "338", "339", "345", "359", "361", "370", "374", "383", "385", "387", "388", "394", "398", "400", "404", "407", "414", "417", "421", "422", "423", "429", "433", "435", "436", "445", "446", "447", "461", "462", "478", "479", "483", "484", "487", "500", "501", "504", "509", "512", "513", "515", "517", "522", "528", "534", "538", "540", "545", "546", "553", "556", "560", "564", "571", "579", "582", "584", "586", "591", "592", "598", "604", "608", "609", "612", "613", "618", "620", "621", "624", "625", "637", "638", "644", "646", "649", "654", "665", "668", "669", "674", "675", "680", "683", "688", "692", "695", "699", "701", "705", "706", "708", "709", "716", "720", "722", "723", "724", "728", "731", "744", "746", "753", "754", "761", "773", "774", "775", "776", "783", "786", "788", "796", "802", "806", "810", "815", "820", "823", "824", "829", "831", "837", "838", "839", "849", "851", "856", "866", "871", "872", "892", "913", "926", "933", "939", "953", "960", "967", "976", "978", "995", "996", "997", "1005", "1010", "1016", "1032", "1033", "1037", "1039", "1041", "1043", "1044", "1056", "1064", "1078", "1080", "1084", "1095", "1096", "1110", "1111", "1112", "1114", "1117", "1132", "1167", "1189", "1190", "1206", "1207", "1211", "1224", "1255", "1266", "1278"];
playernews = [];
var upcoming = "";
var matchmx = [["20160411", "Hammamet $10K", "Clay", "10", "L", "", "", "Q", "R16", "6-2 6-2", "3", "Valentini Grammatikopoulou", "345", "2", "", "U", "19.1676933607", "", "GRE", "0", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "0", "2016-W-C10-TUN-14A-2016-024"],
          ["20160411", "Hammamet $10K", "Clay", "10", "W", "", "", "Q", "R32", "4-6 6-3 6-1", "3", "Sabrina Bamburac", "1278", "", "Q", "R", "22.8829568789", "", "GBR", "0", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "0", "2016-W-C10-TUN-14A-2016-015"],
          ["20160418", "Hammamet $10K", "Clay", "10", "L", "", "", "WC", "QF", "6-3 7-5", "3", "Angelica Moratelli", "534", "6", "", "R", "21.6700889802", "", "ITA", "0", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "1", "2016-W-C10-TUN-15A-2016-026"],
          ["20160418", "Hammamet $10K", "Clay", "10", "W", "", "", "WC", "R16", "7-6(3) 6-1", "3", "Julyette Maria Josephine Steur", "913", "", "", "U", "20.7255304586", "", "GER", "0", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "2016-W-C10-TUN-15A-2016-019"],
          ["20160418", "Hammamet $10K", "Clay", "10", "W", "", "", "WC", "R32", "4-6 7-6(4) 7-6(6)", "3", "Sandra Samir", "423", "3", "WC", "R", "18.4531143053", "", "EGY", "0", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "0", "2016-W-C10-TUN-15A-2016-005"],
          ["20160425", "Hammamet $10K", "Clay", "10", "L", "", "", "Q", "SF", "6-7(7) 7-6(5) 6-0", "3", "Sandra Zaniewska", "731", "6", "", "R", "24.3093771389", "", "POL", "0", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "0", "2016-W-C10-TUN-16A-2016-030"],
(...)
```

#### MatchstatSpider

El *spider* obtiene datos sobre un jugador, sus partidos y contrincantes a partir de la página de
matchstat. Los datos en ocasiones pueden resultar redundantes con respecto a los del *MainSpider*,
sin embargo, se consultan registros diferentes. Mientras que la *MainSpider* obtiene un listado de
jugadores principales en los que se encuentra interesado, este realiza el comportamiento recursivo
de obtener el histórico de sus contrincantes y extrae toda la base de datos de esta manera.

- Petición 1
    Punto de entrada: `https://matchstat.com/tennis/player/{name}`
    Parámetros: Nombre jugador
    Resultados:
    - Lista de URL con los jugadores que participan
    - Lista de *Event* con los partidos que se realizan
    - Lista de *Match* con los partidos que se realizan
    - Lista de *Player* con los partidos que se realizan
    - Lista de *Result* con los partidos que se realizan

- Petición 2
    Punto de entrada: `https://matchstat.com/tennis/player/{name}/n`
    Parámetros: Nombre jugador, Número de página
    Resultados:
    - Lista de URL con los jugadores que participan
    - Lista de *Event* con los partidos que se realizan
    - Lista de *Match* con los partidos que se realizan
    - Lista de *Player* con los partidos que se realizan
    - Lista de *Result* con los partidos que se realizan

La lista de partidos histórico para un jugador dado se encuentra paginada, de manera que debe ser
el mismo scraper el que, a partir de la primera, lance una serie de peticiones encadenadas para
recorrer la lista completa.

#### ITFTennisSpider

Este *spider* se considera auxiliar, debido a la cantidad de dificultades que supuso el escribir un
parser y una navegación para el html de su web.

La web se encuentra implementada de una manera que impide el acceso a muchos tipos de datos de una
manera estructurada. No provee de API ni endpoints específicos para los datos, los partidos no
tienen identificador, las urls emplean identificadores distintos para torneos y jugadores, el HTML
es inconsistente y escrito a mano con errores en ocasiones.

A parte de esas inconveniencias que pueden dificultar el parseo, es probable que la página sufra el
acceso de muchas arañas por parte de varios servicios de información similares a este, ya que se
trata de la página de la federación, y esta es por tanto la fuente autoritativa de los datos al
respecto.

La web implementa una serie de protecciones que no permiten a los robots acceder, determinando que
únicamente ciertos *User Agent*[^7.2] son válidos para acceder. Y más importantemente, limitando el
acceso repetido a un mismo usuario. Esta última restricción resulta difícil de implementar en los
casos de webs de información que es consultada anónimamente, sufriendo bloqueos persistentes
bloqueos durante los diversos intentos de extracción de información, que obligaron a implementar
diversas medidas como el falseo de *User Agents* aleatorio y el empleo de múltiples proxies como se
trata en los siguientes apartados.

- Petición 1
    Punto de entrada:
    `http://itftennis.com/procircuit/tournaments/men\'s-calendar.aspx`
    Resultados:
    - Lista de URL con los jugadores que participan
    - Lista de *Event* con los partidos que se realizan
    - Lista de *Match* con los partidos que se realizan
    - Lista de *Player* con los partidos que se realizan
    - Lista de *Result* con los partidos que se realizan

### Partidos futuros

Otro tipo de *crawler*, se encuentra exclusivamente dedicado a encontrar partidos *aún no jugados*.
Estos partidos son denominados *partidos futuros*. La implementación de estos dos *crawler* es
mucho más sencillo que el que extra el archivo histórico, ya que se basa en el parseo de ciertas
páginas de apuestas que mantienen listas de estos partidos con el fin de que los usuarios puedan
realizar su apuesta.

Estos partidos son almacenados por el *Updater* en una tabla diferente, de manera que se encuentren
separados de la base de conocimiento, ya que no tienen resultado. Son incorporados al resto de
partidos históricos en el momento que se encuentra un resultado para ellos, tarea que recae en el
*ResultsSpider*.

#### UpcomingSpider

- Petición 1
    Resultados:
    - Lista de URL con los jugadores que participan
    - Lista de *Event* con los partidos que se realizan
    - Lista de *Match* con los partidos que se realizan
    - Lista de *Player* con los partidos que se realizan

#### ResultsSpider

- Petición 1
    Resultados:
    - Lista de URL con los jugadores que participan
    - Lista de *Event* con los partidos que se realizan
    - Lista de *Match* con los partidos que se realizan
    - Lista de *Result* con los partidos que se realizan

### Parseo

Cada parser mencionado anteriormente trabaja con documentos HTML o Javascript que son devueltos por
los servidores consultados. Estos resultados son parseados a una estructura en memoria que es
posible consultar mediante un API.

El parseo de los documentos HTML se realiza empleando *lxml*, una librería de Python que se
encuentra integrada en *Scrapy*, y que permite parsear los documentos en una estructura de árbol en
un documento xml. El parseo es lo suficientemente tolerante como para admitir muchos los errores e
inconsistencias presentes en los html que pueden encontrarse en la web.

Existen dos lenguajes específicos para la consulta de estos documentos XML:

- `XPath`: Lenguaje estandarizado para la consulta de documentos xml.
- `CSS`: Lenguaje ad-hoc que permite emplear la sintaxis de CSS para generar consultas *Xpath*.

Para los *endpoints* que devuelven Javascript se ha integrado un intérprete de este lenguaje en la
aplicación. Estas peticiones son por tanto ejecutadas en un proceso aislado de la aplicación, de
manera que no sean posibles interacciones con el scraper, por seguridad. El resultado de este
parseo se encuentra luego disponible traducido a estructuras de datos equivalentes en Python. El
componente [@PyExecJS] permite ejecutar código Javascript desde Python. Realmente la interpretación
y procesamiento de ese Javascript es delegado en un motor conocido como V8 o el de algún navegador
conocido.

Cada *Spider* necesita conocer la disposición específica de cada dato en cada página,
cómo se distribuyen entre los elementos del HTML o Javascript así como saber manejar las
inconsistencias y particularidades que tenga cada uno. En muchas ocasiones es necesario componer un
dato con múltiples elementos distribuidos a lo largo del sitio, o incluso entre diferentes páginas,
arrastrando datos entre petición y petición.

![Relación entre Scrapers y Loaders que realizan el parseo. Cada scraper específico para una página emplea un conjunto de loaders que producen las entidades formalizadas a partir del análisis de fragmentos del html. \label{scraper-loader-diagram}](source/figures/scraper-loader-diagram.pdf){ width=100% }

Para realizar esta tarea de manera ordenada, evitando embeber el código de parseo con el que
gestiona las peticiones, la gestión de links y la comunicación con el servidor, se ha creado una
colección de objetos `Loader` por cada una de las páginas a consultar. Como puede verse en la
figura \ref{scraper-loader-diagram}, cada página mantiene una serie de loaders que conocen su HTML
específico y que permiten extraer una entidad a partir de un fragmento parcial y ciertos datos
extras que son arrastrados

### Sistema de colas

La comunicación entre los diferentes procesos se realiza mediante colas. Estas colas consisten en
estructuras de datos persistentes que tienen un modo de acceso FIFO. Los datos introducidos primero
por un proceso *productor* son obtenidos en orden de inserción por otro proceso *consumidor*.

Estas estructuras también se ocupan de controlar la concurrencia en caso de acceso por parte de
múltiples procesos, de manera que los datos no se corrompan, no existan condiciones de carrera al
obtener los datos y la sincronización sea posible. Para esto se controla la escritura (que suele
ser un único proceso) y la lectura (realizada por múltiples *consumidores*).

En el caso concreto del nuestro proyecto, las colas se han implementado como estructuras de datos
sincronizadas sobre un backend de *sqlite*. La base de datos es abierta en modo bloqueante, de
manera que múltiples *consumidores* pueden acceder en orden pero un único *writer* accede al mismo
tiempo.

Aunque el esquema descrito es potencialmente un riesgo para el rendimiento general del sistema, ya
que se bloquea la base de datos completa en cada escritura, hay que recordar que tal como se
muestra en la figura \ref{architecture-detailed-diagram} solo un *crawler* accede al mismo tiempo a
una cola, ya que se trata de un único proceso, y aunque contiene múltiples *spiders*, estas se
ejecutan secuencialmente según obtengan sus resultados de la red. Este modelo, que se encuentra
limitado fundamentalmente por las latencias de red, permite ser laxos en la escritura de
resultados, ya que la mayor parte del tiempo se emplea esperando que lleguen los documentos web.

### Proxies

Las diferentes páginas consultadas suelen implementar sistemas de limitación para evitar que
conjuntos de robots o clientes automatizados provoquen una caida de su servicio. Esto se traduce en
que múltiples peticiones realizadas desde una misma IP son bloqueadas tras unos cuantos intentos.

Para evitar este problema es posible el uso de un *Proxy Web*. Estos proxies trabajan reenviando
las peticiones que reciben, manteniendo la conexión en nombre del cliente original, enmascarando
así el origen real de la petición, y apareciendo como la IP real que inicia la petición.

Existen múltiples proxies disponibles para uso personal, tanto gratuitos como de pago, ofrecen una
calidad de conexión razonable por un precio bajo, y nos permitirían realizar las peticiones sin
exponer la IP desde la que se realiza el desarrollo.

Sin embargo, esta aproximación no nos resulta suficiente para nuestro proyecto, ya que las
protecciones que implementan las web consultadas prohiben la entrada durante un tiempo largo (24h+)
a cada IP tras un cierto número de peticiones seguidas, no demasiado elevado. Esto haría que la
obtención de los datos históricos se prolongase durante semanas, en lugar de las horas que tarda el
proceso actual.

La solución fué utilizar *múltiples proxies*, elaborando una lista de proxies HTTP gratuitos y
empleándolos de forma aleatoria. Esto permitía realizar cada conjunto de peticiones con una IP
distinta, y cambiar de proxy cuando una de las ips era invalidada y la página comenzaba a devolver
errores.

Cada proxy se utiliza durante un número fijo de peticiones, tras lo cual es devuelto al pool, del
cual se extraen empleando una selección aleatoria. En caso de registrar errores, el proxy es
rechazado y no es devuelto al pool, sino eliminado completamente. Es relativamente habitual que los
proxies gratuitos dejen de encontrarse disponible, su ancho de banda sea muy escaso, dejen de
funcionar repentinamente o devuelvan códigos de error.

### User Agents

Las defensas que emplean estas páginas web también se fijan en el *User Agent* [^7.2] declarado por
el navegador. Estos mecanismos rechazan *User Agent* que consideran ilegítimos, o que se repiten
demasiado para la misma IP, ya que a ello lo consideran un usuario individual.

La estrategia empleada para esto consistió en encontrar una lista válida de User Agents realistas a
partir de las estadísticas publicadas por la W3C [@w3cbrowserstats]. A partir de esa lista se
conforman User Agent realistas, que se cambian de forma aleatoria por cada petición.

De esta manera, cada petición realizada a cada servicio web, se realiza declarando un *User Agent*
distinto, que es indistinguible de un usuario legítimo en un navegador moderno. Esto nos permite
evitar el *user tracking* al que someten estas webs a sus usuarios para detectar accesos no
autorizados.

El hecho de que el par *IP-UserAgent* cambie con cada petición, permite neutralizar las
protecciones de todos los servicios a los que se accede como parte del proyecto, y poder obtener
los datos sin bloqueos.

### Limitación de Tráfico

El acceso masivo e indiscriminado a un servidor web puede provocar no solo un bloqueo por parte del
extremo remoto, sino también consecuencias negativas para el nivel de servicio de la página que nos
encontramos consultando.

Para evitar efectos adversos que puedan sufrir estas páginas, es necesario no provocar una gran
avalancha de peticiones concurrentes a cada servicio; de otra manera, no solo es posible que no
acepten más peticiones, sino que podemos provocar un efecto de Denegación de Servicio (*DDOS*) que
haga que deje de funcionar el servicio.

Esta situación no solo es poco respetuosa con las fuentes de las que consultamos la información,
sino que también es inconveniente para el resultado del proyecto, ya que si se pierde el acceso a
las fuentes de información, no es posible continuar con la recopilación de la misma.

Para evitar esta situación, se encuentra una limitación de tráfico que impide realizar y mantener
más de 5 conexiones concurrentes por proceso, esto es, realizadas al mismo tiempo. Es necesario
pues que termine una conexión existente antes de que entre otra al pool. Esto permite controlar de
manera muy efectiva el nivel de carga al que se somete a todos los servidores en general.

Adicionalmente, existe una limitación extra de 2 segundos entre diferentes peticiones. Esto
previene de realizar grandes cantidades de conexiones nuevas en los casos en los que las respuestas
por parte del servidor sean muy rápidas, o muy especialmente, en el caso en el que los servidores
comiencen a devolver errores inmediatamente debido a un estado de mantenimiento o una caida del
servicio. En ese caso, es posible que el mecanismo de reintento de peticiones iniciase muchas
nuevas, las cuales acabarían todas en error.


[^7.1]: Este diagrama ha sido construido empleando la herramienta de generación automática de
        diagramas [eralchemy], a partir de una base de datos real, actualizada hasta la última
        versión de migraciones, con lo que representa fielmente el estado de la base de datos.  El
        modo de uso es el siguiente `eralchemy -i sqlite:///data.sqlite -o db-er-diagram.pdf`.
        Requiere instalado el paquete graphviz.

[^7.2]: Fragmento de información que envían los navegadores a los servidores web, indicando nombre,
        versión y otra información acerca del sistema operativo.
