# Abreviaturas {.unnumbered}

\begin{tabbing}
\textbf{API}~~~~~~~~~~~~ \= \textbf{A}pplication \textbf{P}rogramming \textbf{I}nterface \\  
\textbf{ML}~~~~~~~~~~~~ \= \textbf{M}achine \textbf{L}earning \\  
\textbf{JSON} \> \textbf{J}ava \textbf{S}cript \textbf{O}bject \textbf{N}otation \\  
\textbf{ORM} \> \textbf{O}bject \textbf{R}relational \textbf{M}apper \\  
\textbf{W3C} \> \textbf{W}orld Wide Web \textbf{C}onsortium \\  
\end{tabbing}

\newpage

