import datetime

from sqlalchemy import inspect
from sqlalchemy.orm import relationship, sessionmaker
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import (Column, ForeignKey, Integer, String, DateTime,
                        create_engine)

Base = declarative_base()
_SESSION = None
NOW = datetime.datetime.now()


def get_session():
    return _SESSION


def set_session(session):
    global _SESSION
    _SESSION = session
    return session

def make_session(url):
    return sessionmaker(bind=create_engine(url))()


def create_all(url):
    Base.metadata.create_all(create_engine(url))


class Player(Base):
    __tablename__ = 'players'

    id = Column(Integer, primary_key=True)
    name = Column(String(), nullable=False, unique=True)
    country = Column(String(), nullable=True)
    ranking = Column(Integer(), nullable=True)
    gender = Column(String(), nullable=True)
    born = Column(DateTime(), nullable=True)
    url = Column(String(), nullable=True)
    weight = Column(Integer(), nullable=True)
    height = Column(Integer(), nullable=True)
    hand = Column(String(), nullable=True)

    matches = relationship(
        'Match', primaryjoin='Match.player1_id == Player.id or '
        'Match.player2_id == Player.id'
    )
    results = relationship(
        'Result', primaryjoin='Result.winner_id == Player.id or '
        'Result.loser_id == Player.id'
    )

    @property
    def age(self):
        if self.born:
            return self.age_at(NOW)

    def age_at(self, date):
        return (date - self.born).days / 365.

    def age_at_or_none(self, date):
        if date and self.born:
            return self.age_at(date)

    def __str__(self):
        return self.name

    def __repr__(self):
        return 'Player(name={self.name},born={self.born})'.format(self=self)


class Event(Base):
    __tablename__ = 'events'

    id = Column(Integer(), primary_key=True)
    name = Column(String(), nullable=False)
    code = Column(String(), nullable=True)
    gender = Column(String(), nullable=True)
    date = Column(DateTime(), nullable=True)
    place = Column(String(), nullable=True)
    url = Column(String(), nullable=True)
    prize = Column(Integer())
    surface = Column(String(), nullable=True)

    matches = relationship('Match')

    def __str__(self):
        return '{self.name} ({self.date.year})'.format(self=self)

    def __repr__(self):
        return 'Event(name={self.name},date={self.date},code={self.code})'\
            .format(self=self)


class Match(Base):
    __tablename__ = 'matches'

    id = Column(Integer, primary_key=True)
    code = Column(String(), nullable=True)
    round = Column(String(), nullable=False)
    gender = Column(String(), nullable=False)
    event_id = Column(Integer(), ForeignKey(Event.id))
    date = Column(DateTime(), nullable=True)
    surface = Column(String(), nullable=True)

    player1_ranking = Column(Integer(), nullable=True)
    player2_ranking = Column(Integer(), nullable=True)

    player1_id = Column(Integer(), ForeignKey(Player.id))
    player2_id = Column(Integer(), ForeignKey(Player.id))

    event = relationship(Event, uselist=False)
    player1 = relationship(Player, uselist=False, foreign_keys=[player1_id])
    player2 = relationship(Player, uselist=False, foreign_keys=[player2_id])
    result = relationship('Result', uselist=False)

    def __str__(self):
        return '{} vs {} at {}'.format(
            self.player1, self.player2, self.event
        )

    def __repr__(self):
        return 'Match(event={self.event},player1={self.player1},'\
            'player2={self.player2})'.format(self=self)


class Result(Base):
    __tablename__ = 'results'

    id = Column(Integer, primary_key=True)
    match_id = Column(Integer(), ForeignKey(Match.id))
    winner_id = Column(Integer(), ForeignKey(Player.id))
    loser_id = Column(Integer(), ForeignKey(Player.id))
    score = Column(String(), nullable=False)
    odds = Column(String(), nullable=True)

    match = relationship(Match, uselist=False, back_populates='result',
                         foreign_keys=[match_id])
    winner = relationship(Player, uselist=False, backref='won',
                          foreign_keys=[winner_id])
    loser = relationship(Player, uselist=False, backref='lost',
                         foreign_keys=[loser_id])

    def __str__(self):
        return '{s.winner} vs {s.loser} at {s.match.event} score {s.score}'\
            .format(s=self)

    def __repr__(self):
        return 'Result(winner={self.winner},loser={self.loser},'\
            'event={self.match.event},score={self.score})'.format(self=self)


class Upcoming(Base):
    __tablename__ = 'upcoming'

    id = Column(Integer, primary_key=True)
    match_id = Column(Integer(), ForeignKey(Match.id))
    date = Column(DateTime(), default=datetime.datetime.utcnow)

    match = relationship(Match, uselist=False, foreign_keys=[match_id])

    def __str__(self):
        return 'Upcoming {s.match} saw at {s.date}'.format(s=self)

    def __repr__(self):
        return 'Upcoming(match={s.match},date={s.date})'.format(s=self)
