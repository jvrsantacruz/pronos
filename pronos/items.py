import scrapy

from scrapy.loader.processors import MapCompose
from pronos.cleaners import (clean_gender, clean_surface, clean_hand,
                             clean_round, clean_event_name, clean_ranking)


class Player(scrapy.Item):
    type = scrapy.Field()
    name = scrapy.Field()
    country = scrapy.Field()
    ranking = scrapy.Field(input_processor=MapCompose(clean_ranking))
    gender = scrapy.Field(input_processor=MapCompose(clean_gender))
    idweb = scrapy.Field()
    born = scrapy.Field()
    url = scrapy.Field()
    hand = scrapy.Field(input_processor=MapCompose(clean_hand))
    weight = scrapy.Field()
    height = scrapy.Field()


class Event(scrapy.Item):
    type = scrapy.Field()
    name = scrapy.Field()
    code = scrapy.Field(input_processor=MapCompose(clean_event_name))
    date = scrapy.Field()
    place = scrapy.Field()
    url = scrapy.Field()
    gender = scrapy.Field(input_processor=MapCompose(clean_gender))
    idweb = scrapy.Field()
    title = scrapy.Field()
    country = scrapy.Field()
    end_date = scrapy.Field()
    category = scrapy.Field()
    prize = scrapy.Field()
    surface = scrapy.Field(input_processor=MapCompose(clean_surface))
    finished = scrapy.Field()
    matches = scrapy.Field()
    winner = scrapy.Field()


class Match(scrapy.Item):
    type = scrapy.Field()
    code = scrapy.Field()
    event = scrapy.Field()
    round = scrapy.Field(input_processor=MapCompose(clean_round))
    gender = scrapy.Field(input_processor=MapCompose(clean_gender))
    surface = scrapy.Field(input_processor=MapCompose(clean_surface))
    court = scrapy.Field()
    date = scrapy.Field()
    tournament_day = scrapy.Field()
    player1 = scrapy.Field()
    player2 = scrapy.Field()
    url = scrapy.Field()


class MatchResult(scrapy.Item):
    type = scrapy.Field()
    match = scrapy.Field()
    winner = scrapy.Field()
    loser = scrapy.Field()
    score = scrapy.Field()
    odds = scrapy.Field()
