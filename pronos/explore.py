import os
import pickle
import argparse
import collections

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns

from pronos.prediction import algorithm_names, train_models
from pronos.queries import make_query, iter_matches, base_query
from pronos.db import set_session, make_session, get_session, create_all


#if not get_session():
#    url = os.getenv('URL') or 'sqlite:///bench.sqlite'
#    create_all(url)
#    session = make_session(url)
#    set_session(session)


def age_at(player, match):
    return (match.date - player.born).days // 365


def sub_or_none(value1, value2):
    if not pd.isnull(value1) and not pd.isnull(value2):
        return value1 - value2


def eq_or_none(value1, value2):
    if not pd.isnull(value1) and not pd.isnull(value2):
        return int(value1 == value2)


def _get_dataframe(data, path):
    if os.path.isfile(path):
        d = pd.read_pickle(path)
    else:
        print('Creating dataframe at', path)
        d = pd.DataFrame(data).drop_duplicates()
        d.to_pickle(path)
    return d


def get_raw_data():
    return _get_dataframe(iter_matches(base_query()), '.raw-dataframe')


def get_data():
    return _get_dataframe(iter_matches(make_query()), '.dataframe')


def save(name, aspect=None):
    print('Saving plot', name)
    ax = plt.gca()
    if aspect is not None:
        ax.set_aspect(aspect)
    plt.savefig(name)
    plt.clf()  # reset state to avoid interferring with next plot


def plot_null_values_per_column_proportion(d):
    d.isnull().sum().map(lambda x: float(x) / len(d)).plot.barh()
    save('figures/data-null-fields.pdf')

def plot_not_null_values_per_column_horizontal(d):
    df = d.copy()
    for column in df.columns:
        if column.startswith('p'):
            del df[column]

    d.count().plot.barh()

    plt.tight_layout()
    save('figures/data-not-null-fields-horizontal.pdf')


def plot_null_values_per_column(d):
    d.count().plot.bar()
    save('figures/data-not-null-fields.pdf')


def plot_matches_per_year(d):
    d\
        .set_index(['date'])\
        .groupby(lambda x: x.year)['gender']\
        .transform('count')\
        .plot()
    save('figures/data-all-matches-per-year.pdf')


def plot_matches_per_year_filtered(d):
    d\
        .set_index(['date'])\
        .groupby(lambda x: x.year)['gender']\
        .transform('count')\
        .plot()
    save('figures/data-matches-per-year.pdf')


def plot_histogram(d):
    d.hist()
    plt.tight_layout()
    save('figures/data-histogram.pdf', aspect=0.3)


def plot_correlation_with_target(d):
    df = d.corr()

    # delete clutter columns
    undesired_cols = [c for c in df.columns if c.startswith('p') or c == 'gender']
    for name in undesired_cols:
        del df[name]
        del df['wonp1'][name]
    del df['wonp1']['wonp1']
    ax = df['wonp1'].abs().plot.bar()

    # set ranking diff in red
    bars = ax.get_children()
    bars[list(df.columns).index('ranking_diff')].set_color('r')

    plt.tight_layout()
    save('figures/correlation_with_target.pdf')


def plot_correlation_g(d):
    corr = d.corr()
    # Show only lowest part of the diagonal
    mask = np.zeros_like(corr, dtype=np.bool)
    mask[np.triu_indices_from(mask)] = True
    sns.heatmap(
        corr,
        xticklabels=corr.columns,
        yticklabels=corr.columns,
        vmin=-1,
        vmax=1,
        center=0,
        square=True,
        mask=mask,
        cmap='Blues'
    )
    plt.tight_layout()
    save('figures/data-correlation-g.pdf')


def plot_correlation_line(d):
    variables = [
        ['p1weight', 'p1height', 'p1ranking'],
        ['weight_diff', 'height_diff', 'ranking_diff'],
        ['p1age', 'p1hand', 'prize'],
        ['age_diff', 'hand_diff', 'round'],
        ['gender', 'surface', 'wonp1']
    ]
    rows, cols = len(variables), len(variables[0])
    fig, ax = plt.subplots(figsize=(10, 10), ncols=cols, nrows=rows)
    for x in range(rows):
        for y in range(cols):
            current_ax = ax[x][y]
            sns.regplot(variables[x][y], 'wonp1', data=d, ax=current_ax)
            current_ax.wspace = 0.5
            current_ax.hspace = 0.22
    plt.tight_layout()
    save('figures/data-correlation-lines.jpg')


def plot_model_accuracy(models):
    data = dict(
        algorithms=[name for name in models],
        accuracies=[m.calculated_accuracy for m in models.values()]
    )
    order_by_accuracy = sorted(
        models.items(), key=lambda pair: pair[1].calculated_accuracy)
    names_by_accuracy = [name for name, model in order_by_accuracy]
    sns.barplot(x="accuracies", y="algorithms",
                data=data, order=names_by_accuracy, palette='Blues_d')
    plt.tight_layout()
    save('figures/model-accuracy.pdf')


def plot_model_classification_reports(models):
    fig, ax = plt.subplots(3, 2, sharex=True, sharey=True)
    plt.tight_layout()
    for n, name in enumerate(models):
        cr = models[name].calculated_classification_report
        make_plot_classification_report(cr, name, ax=ax[n % 3, n % 2])
    save('figures/model-classification-report.pdf'.format(name))


def make_plot_classification_report(cr, name, ax, cmap=plt.cm.Blues):
    lines = cr.split('\n')

    classes = []
    matrix = []
    for line in lines[2 : (len(lines) - 3)]:
        t = line.split()
        classes.append(t[0])
        v = [float(x) for x in t[1: len(t) - 1]]
        print(v)
        matrix.append(v)

    df = pd.DataFrame(matrix)
    ax.title.set_text(name)
    xticklabels = ['precision', 'recall', 'f1-score', 'support']
    sns.heatmap(df, ax=ax, annot=True, cmap=cmap, vmin=0, vmax=1,
                xticklabels=xticklabels)


def make_raw_plots(d):
    plot_null_values_per_column_proportion(d)
    plot_not_null_values_per_column_horizontal(d)
    plot_null_values_per_column(d)
    plot_matches_per_year(d)


def make_plots(d):
    plot_matches_per_year_filtered(d)
    plot_histogram(d)
    plot_correlation_g(d)
    plot_correlation_line(d)
    plot_correlation_with_target(d)


def make_model_plot(models):
    plot_model_accuracy(models)
    plot_model_classification_reports(models)


Datasets = collections.namedtuple(
    'Dataset', 'x_train x_test y_train y_test'
)


def save_models(path, model):
    with open(path, 'wb') as stream:
        pickle.dump(model, stream)


def load_models(path):
    with open(path, 'rb') as stream:
        return pickle.load(stream)


def main(args):
    if args.db:
        set_session(make_session(args.db))

    if 'all' in args.algorithm:
        args.algorithm = algorithm_names

    if not os.path.isdir(args.output):
        os.mkdir(args.output)

    d = get_data()

    models = None
    if args.train:
        models = train_models(args.algorithm, d)
        if args.models:
            save_models(args.models, models)
    elif args.models:
        models = load_models(args.models)

    if args.plots:
        make_raw_plots(get_raw_data())
        make_plots(d)
        # winning correlation
        print('Correlation with wonp1')
        print(d.corr()['wonp1'])
        if models:
            make_model_plot(models)

    if args.interactive:
        from IPython import embed
        embed()


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--interactive', action='store_true')
    parser.add_argument('-o', '--output', default='figures')
    parser.add_argument('-p', '--plots', action='store_true')
    parser.add_argument('-t', '--train', action='store_true')
    parser.add_argument(
        '-a', '--algorithm', action='append', default=[],
        choices=algorithm_names + ('all',)
    )
    args = parser.parse_args()
    main(args)
