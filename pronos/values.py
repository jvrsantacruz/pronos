hands = ('right', 'left')
genders = ('male', 'female')
surfaces = ('clay', 'grass', 'hard', 'carpet')
rounds = ('final', 'semi-final', 'quarter-final', 'round-16', 'round-32', 'q', 'q1', 'q2', 'q3')
