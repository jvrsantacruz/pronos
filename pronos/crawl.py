import os
import logging
import argparse

from scrapy.crawler import CrawlerProcess
from scrapy.utils.project import Settings

from pronos.local_queue import Queue
from pronos.spiders.main import MainSpider
from pronos import settings as settings_module
from pronos.spiders.result import ResultSpider
from pronos.spiders.abstract import AbstractSpider
from pronos.spiders.upcoming import UpcomingSpider
from pronos.spiders.itftennis import ItftennisSpider
from pronos.spiders.matchstat import MatchstatSpider
from pronos.db import make_session, create_all, set_session

settings = Settings()
settings.setmodule(settings_module)
available_spiders = {
    'itf': ItftennisSpider,
    'matchstat': MatchstatSpider,
    'abstract': AbstractSpider,
    'upcoming': UpcomingSpider,
    'main': MainSpider,
    'result': ResultSpider,
}


def configure_logging(args):
    handler = logging.FileHandler(args.log or 'errors.log')
    handler.setLevel(logging.WARNING)
    logging.getLogger('scrapy.core.scraper').setLevel(logging.INFO)
    names = [None, 'scrapy', 'scrapy.core.scraper']
    loggers = [logging.getLogger(name) for name in names]
    for logger in loggers:
        logger.addHandler(handler)


def setup_database(args):
    if args.db:
        path = os.path.abspath(args.db)
        url = 'sqlite:///' + path
        if not os.path.exists(path):
            create_all(url)
        session = make_session(url)
    else:
        url = 'sqlite:///:memory:'
        create_all(url)
        session = make_session(url)

    set_session(session)


def configure_jobdir(args):
    if args.jobdir and not os.path.exists(args.jobdir):
        os.makedirs(args.jobdir)


def crawl_historic(args, settings, spiders):
    settings['ITEM_PIPELINES'].update({
        'pronos.pipelines.MainPipeline': 350,
    })

    process = CrawlerProcess(settings)

    queues = []
    for spider in args.spider:
        queue = Queue(':memory:')
        queues.append(queue)
        #for name in args.player:
        #    queue.push({'name': name})
        process.crawl(spiders[spider], queue=queue)
    process.crawl(spiders['main'], queues=queues, years=args.years)

    process.start()


def crawl_upcoming(args, settings, spiders):
    settings['ITEM_PIPELINES'].update({
        'pronos.pipelines.UpcomingPipeline': 350,
    })

    process = CrawlerProcess(settings)
    process.crawl(spiders['upcoming'])
    process.start()


def crawl_result(args, settings, spiders):
    settings['ITEM_PIPELINES'].update({
        'pronos.pipelines.ResultPipeline': 350,
    })

    process = CrawlerProcess(settings)
    process.crawl(spiders['result'])
    process.start()


def run_spider(name, args):
    configure_logging(args)
    setup_database(args)

    settings = Settings()
    settings.setmodule(settings_module)
    if args.no_middlewares:
        settings['DOWNLOADER_MIDDLEWARES'] = {}

    crawler = {
        'main': crawl_historic,
        'upcoming': crawl_upcoming,
        'result': crawl_result,
    }[name]
    crawler(args, settings, available_spiders)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-s', '--spider', action='append', default=[])
    parser.add_argument('-o', '--output', default='output.jl')
    parser.add_argument('-f', '--format', default='jsonlines')
    parser.add_argument('-M', '--no-middlewares', action='store_true')
    parser.add_argument('-p', '--player', action='append', default=[])
    parser.add_argument('-y', '--years', default='2017')
    parser.add_argument('-w', '--workflow', default='main',
                        choices=['main', 'upcoming', 'result'])
    parser.add_argument('-j', '--jobdir')
    parser.add_argument('-d', '--db')
    parser.add_argument('-l', '--log')
    args = parser.parse_args()

    configure_logging(args)
    setup_database(args)
    configure_jobdir(args)

    settings['FEED_URI'] = args.output
    settings['FEED_FORMAT'] = args.format
    if args.no_middlewares:
        settings['DOWNLOADER_MIDDLEWARES'] = {}
    if args.jobdir:
        settings['JOBDIR'] = args.jobdir

    if args.workflow == 'main':
        crawl_historic(args, settings, available_spiders)
    elif args.workflow == 'upcoming':
        crawl_upcoming(args, settings, available_spiders)
    elif args.workflow == 'result':
        crawl_result(args, settings, available_spiders)
