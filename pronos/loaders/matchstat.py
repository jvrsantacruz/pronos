import re
from datetime import datetime

from scrapy.loader import ItemLoader
from scrapy.loader.processors import MapCompose, TakeFirst

from pronos.items import Event, Match, Player, MatchResult


class EventLoader(ItemLoader):
    default_item_class = Event
    default_output_processor = TakeFirst()

    def __init__(self, *args, **kwargs):
        super(EventLoader, self).__init__(*args, **kwargs)
        response = kwargs['response']
        self.add_value('type', 'event')
        self.add_value('url', response.url)
        self.add_value('date', response.meta['date'])
        self.add_value('gender', response.meta['gender'])
        self.add_css(
            'name', 'div.ms-event-title h3::text', re='(.*) - .* Singles',
        )


class MatchLoader(ItemLoader):
    default_item_class = Match
    default_output_processor = TakeFirst()

    def __init__(self, *args, **kwargs):
        event = kwargs.pop('event', None)
        super(MatchLoader, self).__init__(*args, **kwargs)
        meta = kwargs['response'].meta
        event = event or meta['event']
        self.add_value('type', 'match')
        self.add_value('event', event)
        self.add_value('gender', meta['gender'])
        self.add_value('date', meta['event'].get('date'))
        self.add_css('round', 'td.round::text', re='(\w+)')
        players = self.get_css('td.player-name a::text')

        players = []
        for player in self.selector.css('td.player-name'):
            name = player.css('a::text').extract_first()

            # Matches without one player often appear
            # but are not visible on the # web interface
            if not name:
                self.replace_value('type', 'invalid')
                return

            url = player.css('a::attr(href)').extract_first()
            name = name.strip()
            country = ''.join(player.css('td::text').extract())\
                .strip().replace('(', '').replace(')', '')
            repeated = player.css('.badge::text').extract_first()
            repeated = repeated and repeated.strip()
            players.append(dict(
                url=url,
                name=name,
                country=country,
                repeated=repeated,
                gender=meta['gender']
            ))

        self.add_value('player1', players[0])
        self.add_value('player2', players[1])


class ResultLoader(ItemLoader):
    default_item_class = MatchResult
    default_output_processor = TakeFirst()

    odds_output_processor = MapCompose()

    def __init__(self, *args, **kwargs):
        match = kwargs.pop('match', None)
        super(ResultLoader, self).__init__(*args, **kwargs)
        self.add_value('type', 'result')
        match = match or kwargs['response'].meta.get('match')
        self.add_value('match', match)
        self.add_value('winner', match['player1'])
        self.add_value('loser', match['player2'])

        score = self.selector.css('td.score a::text').extract_first()
        if not score or 'Retired' in score:
            self.replace_value('type', 'invalid')
        score = score if score and 'Retired' not in score else 'invalid'
        self.add_value('score', score.strip())
        self.add_value('odds', self.selector.css('.odds::text').extract())


class PlayerLoader(ItemLoader):
    """Load data from player page"""
    default_item_class = Player
    default_output_processor = TakeFirst()

    def __init__(self, *args, **kwargs):
        super(PlayerLoader, self).__init__(*args, **kwargs)
        response = kwargs['response']
        self.add_value('type', 'player')
        self.add_css('name', '.player-bio-bg h1::text')
        self.add_value('url', response.url)
        self.add_value('gender', response.meta.get('gender'))
        self.add_xpath('ranking', self._field('Ranking:'))
        self.add_xpath('country', self._field('Country:'))
        self.add_xpath('hand', self._field('Hand:'))
        self.add_xpath(
            'height', self._field('Height/Weight:'), re='.*\(([0-9.]+ m)\).*'
        )
        self.add_xpath(
            'weight', self._field('Height/Weight:'), re='.*\(([0-9]+ kg)\).*'
        )
        self.add_xpath(
            'born', self._field('Born:'), self._parse_date,
            re=('(\d{4}-\d{2}-\d{2}).*')
        )

    def _parse_date(self, dates):
        for date in dates:
            try:
                yield datetime.strptime(date, r'%Y-%m-%d')
            except (ValueError, TypeError):
                pass

    def _field(self, name):
        return '//tr[td[text()="{}"]]/td[position() = 2]/text()'.format(name)


class PlayerEventLoader(ItemLoader):
    default_item_class = Event
    default_output_processor = TakeFirst()

    def __init__(self, *args, **kwargs):
        super(PlayerEventLoader, self).__init__(*args, **kwargs)
        self.add_value('type', 'event')
        self.add_css('date', 'span::text', self._parse_date)
        self.add_css('url', 'a::attr(href)')
        self.add_css('name', 'a::text')
        self.add_css('prize', 'a::text', self._parse_prize)
        self.add_css('place', 'a::text', self._parse_place)
        self.add_value(
            'gender', self._parse_gender(
                self.get_css('a::attr(href)', TakeFirst())
            )
        )

    def _parse_place(self, names):
        for name in names:
            match = re.match('\d+k ((?:\w|\s)+), Wk\. \d+', name)
            if match is not None:
                yield match.group(1)

            match = re.match('(\w|\s)+ F\d+ Futures', name)
            if match is not None:
                yield match.group(1)

    def _parse_prize(self, names):
        for name in names:
            match = re.match('^(\d+)k ', name)
            if match is not None:
                yield int(match.group(1)) * 1000

    def _parse_date(self, years):
        for year in years:
            return datetime.strptime('0 / ' + year, '%w / %W / %Y')

    def _parse_gender(self, url):
        if '/m/' in url:
            return 'male'
        if '/w/' in url:
            return 'female'
        raise Exception('Unknown gender for event %r' % url)


class PlayerMatchLoader(ItemLoader):
    default_item_class = Match
    default_output_processor = TakeFirst()
    header = ('round', 'surface', 'player1', 'player2', 'score', 'odds')

    def __init__(self, *args, **kwargs):
        response = kwargs['response']
        event = kwargs.pop('event', None)
        gender = event.get('gender')
        super(PlayerMatchLoader, self).__init__(*args, **kwargs)
        self.add_value('type', 'match')
        self.add_value('event', event)
        self.add_value('gender', gender)
        self.add_value('date', event['date'])
        self.add_xpath('round', self._query('round') + '/text()')
        self.add_xpath('surface', self._query('surface') + '/span/text()')
        self.add_value(
            'player1', self._parse_player('player1', response, gender)
        )
        self.add_value(
            'player2', self._parse_player('player2', response, gender)
        )

    def _query(self, field):
        return 'td[position() = {}]'.format(self.header.index(field) + 1)

    def _parse_player(self, field, response, gender):
        selector = self.selector.xpath(self._query(field))[0]
        if selector.css('a'):
            return dict(
                name=selector.css('a::text').extract_first().strip(),
                url=selector.css('a::attr(href)').extract_first(),
                gender=gender
            )
        else:
            return dict(
                name=selector.xpath('text()').extract_first().strip(),
                url=response.url.rsplit('/', 1)[0],  # url ends in eg: /150
                gender=gender
            )

    def _strip(self, values):
        return [value.strip()
                for value in values
                if value and isinstance(value, str)]
