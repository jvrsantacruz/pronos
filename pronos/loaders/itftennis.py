import re
from datetime import datetime
from urllib.parse import urlparse, parse_qsl

from w3lib.html import remove_tags
from scrapy.loader import ItemLoader
from scrapy.loader.processors import MapCompose, TakeFirst

from pronos.items import Event, Match, Player, MatchResult


class PlayerLoader(ItemLoader):
    default_output_processor = TakeFirst()

    def __init__(self, **kwargs):
        response = kwargs['response']
        kwargs.setdefault('item', Player())
        ItemLoader.__init__(self, **kwargs)
        self.add_value('gender', response.meta.get('gender'))
        name_cell = self.get_xpath('td[position() = 2]').extract()[0]
        player = parse_player(remove_tags(name_cell))
        if player:
            self.add_value('name', player['name'])
            self.add_value('country', player['country'])
        self.add_xpath('ranking', 'td[position() = 3]/text()')

        #player_url = self.get_xpath('td[position() = 2]/a/@href').extract()[0]
        parameters = dict(parse_qsl(urlparse(response.url).query))
        self.add_value('idweb', parameters['playerid'][0])

def _parse_start_date(value):
    return _parse_dates(value)[0]


def _parse_end_date(value):
    return _parse_dates(value)[1]


def _parse_dates(value):
    """Parse things like: 26 June - 02 July 2017"""
    if value and '-' in value:
        dates, year = value.rsplit(' ', 1)
        start, end = dates.split('-', 1)
        format = '%d %B %Y'
        try:
            return (
                datetime.strptime(start.strip() + ' ' + year, format),
                datetime.strptime(end.strip() + ' ' + year, format),
            )
        except ValueError:
            pass

    return None, None


class EventLoader(ItemLoader):
    default_input_processor = MapCompose(str.strip)
    default_output_processor = TakeFirst()

    start_date_in = MapCompose(str.strip, _parse_start_date)
    end_date_in = MapCompose(str.strip, _parse_end_date)

    finished_out = bool

    def __init__(self, **kwargs):
        response = kwargs['response']
        kwargs.setdefault('item', Event())
        ItemLoader.__init__(self, **kwargs)
        self.add_value('gender', response.meta.get('gender'))
        self.add_value('idweb', self._get_web_id(response))
        self.add_xpath('code', '//span[@class="h-tkey"]/text()')
        self.add_xpath('title', '//h1[@class="hBg"]/text()')
        self.add_xpath('name', '//li[@id="liPromoName"]'
                       '/span[position()=2]/strong/text()')
        self.add_xpath('country', self._header_field_xpath('Host nation:'))
        self.add_xpath('city', self._header_field_xpath('City/Town:'))
        self.add_xpath('start_date', self._header_field_xpath('Date:'))
        self.add_xpath('end_date', self._header_field_xpath('Date:'))
        self.add_xpath('category', self._header_field_xpath('Category'))
        self.add_xpath('prize', self._header_field_xpath('Prize Money:'))
        self.add_xpath('surface', self._header_field_xpath('Surface:'))
        self.add_xpath('finished', '//ul[@class="wDelist"]/li/span/a/text()')
        #self.add_xpath('winner', '//ul[@class="wDelist"]/li/span/a/text()')

    def _header_field_xpath(self, name):
        return '//span[normalize-space()="{}"]'\
            '/following-sibling::span[1]/strong/text()'.format(name)

    def _get_web_id(self, response):
        parameters = dict(parse_qsl(urlparse(response.url).query))
        return parameters.get('tournamentid')


def parse_player(line):
    match = re.search('([^(]+) \((\w+)\)(?: \[(\d+)\])?', line)
    if match:
        name, country, repeated = match.group(1, 2, 3)
        return dict(name=name, country_code=country, repeated=repeated)


class MatchLoader(ItemLoader):
    default_output_processor = TakeFirst()

    def __init__(self, **kwargs):
        kwargs.setdefault('item', Match())
        ItemLoader.__init__(self, **kwargs)
        self.lines = self.selector.extract().split('<br>')
        self.add_value('order', self.context['order'])
        self.add_value('court', self.context['court'])
        self.add_value('event', self.context['event'])
        self._parse_mode()
        self._parse_gender()
        self._parse_players()
        self._parse_starting_time()

    def _parse_mode(self):
        is_single = set(self.lines) & {'WS', 'MS'}
        self.add_value('mode', 'single' if is_single else 'double')

    def _parse_gender(self):
        if 'WS' in self.lines:
            self.add_value('gender', 'female')
        elif 'MS' in self.lines:
            self.add_value('gender', 'male')

    def _parse_players(self):
        try:
            index = self.lines.index('vs.')
        except ValueError:
            return

        line1, line2 = self.lines[index - 1], self.lines[index + 1]
        self.add_value('player1', parse_player(remove_tags(line1)))
        self.add_value('player2', parse_player(remove_tags(line2)))

    def _parse_starting_time(self):
        time = self.get_xpath('b[position() = 1]/text()', re='(\d{2}:\d{2})')
        if time:
            hour, minute = time[0].split(':', 1)
            self.add_value(
                'date',
                self.context['date'].replace(hour=int(hour), minute=int(minute))
            )

    def has_result(self):
        """Result in sets per player: 2-6 3-6"""
        return re.search(_RESULT_SETS_REGEX, self.lines[-1])


_RESULT_SETS_REGEX = '(\d+-\d+(?:\(\d+\))? ?)+'


class ResultLoader(ItemLoader):
    default_output_processor = TakeFirst()

    def __init__(self, **kwargs):
        kwargs.setdefault('item', MatchResult())
        ItemLoader.__init__(self, **kwargs)
        match = self.context['match']
        self.add_value('match', match)
        self.lines = self.selector.extract().split('<br>')
        self.add_value(
            'score', re.search(_RESULT_SETS_REGEX, self.lines[-1]).group(0)
        )
        p1, p2 = match['player1'], match['player2']
        self.add_value('winner', p1 if self._is_the_winner(p1) else p2)

    def _is_the_winner(self, player):
        for line in self.lines[1:]:
            if player['name'] in line and line.startswith('<b>'):
                return True
