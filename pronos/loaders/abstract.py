from datetime import datetime

import execjs

from scrapy.loader import ItemLoader
from scrapy.loader.processors import TakeFirst
from pronos.items import Event, Match, Player, MatchResult


class JsLoader(ItemLoader):
    def __init__(self, *args, **kwargs):
        self.js = kwargs['js']
        super().__init__(*args, **kwargs)

    def add_js(self, name, key, *args, **kwargs):
        try:
            return self.add_value(name, self.js.eval(key), *args, **kwargs)
        except execjs._exceptions.ProgramError:
            return None


class PlayerLoader(JsLoader):
    default_item_class = Player
    default_output_processor = TakeFirst()

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        response = kwargs['response']
        self.add_value('type', 'player')
        self.add_value('gender', response.meta['player']['gender'])
        self.add_js('name', 'fullname')
        self.add_js('ranking', 'currentrank')
        self.add_js('born', 'dob', parse_date)
        self.add_js('hand', 'hand')
        self.add_js('country', 'country')


class EventLoader(ItemLoader):
    default_item_class = Event
    default_output_processor = TakeFirst()

    def __init__(self, *args, **kwargs):
        entry = kwargs.pop('entry')
        super().__init__(*args, **kwargs)
        response = kwargs['response']
        self.add_value('type', 'event')
        self.add_value('date', entry['date'], parse_date)
        self.add_value('name', entry['tournament'])
        self.add_value('gender', response.meta['player'].get('gender'))
        self.add_value('code', entry['code'].rsplit('-', 1)[0])
        self.add_value('surface', entry['surface'])
        self.add_value('prize', int(entry['prize']) * 1000
                       if entry['prize'] and entry['prize'].isdigit()
                       else None)


class Player2Loader(ItemLoader):
    default_item_class = Player
    default_output_processor = TakeFirst()

    def __init__(self, *args, **kwargs):
        entry = kwargs.pop('entry')
        super().__init__(*args, **kwargs)
        response = kwargs['response']
        self.add_value('type', 'player')
        self.add_value('name', entry['player2_name'])
        self.add_value('gender', response.meta['player'].get('gender'))
        self.add_value('country', entry['player2_country'])


class MatchLoader(ItemLoader):
    default_item_class = Match
    default_output_processor = TakeFirst()

    def __init__(self, *args, **kwargs):
        entry = kwargs.pop('entry')
        event = kwargs.pop('event')
        player = kwargs.pop('player')
        player2 = kwargs.pop('player2')
        super().__init__(*args, **kwargs)
        self.add_value('type', 'match')
        self.add_value('event', event)
        self.add_value('player1', player)
        self.add_value('player2', player2)
        self.add_value('gender', player['gender'])
        self.add_value('code', entry['code'])
        self.add_value('round', entry['round'])
        self.add_value('surface', entry['surface'])
        self.add_value('date', entry['date'], parse_date)


class ResultLoader(ItemLoader):
    default_item_class = MatchResult
    default_output_processor = TakeFirst()

    def __init__(self, *args, **kwargs):
        entry = kwargs.pop('entry')
        match = kwargs.pop('match')
        super().__init__(*args, **kwargs)
        self.add_value('type', 'result')
        self.add_value('match', match)
        self.add_value('score', entry['score'])
        winner = match['player1'] if entry['won'] == 'W' else match['player2']
        self.add_value('winner', winner)
        loser = match['player1'] if entry['won'] == 'L' else match['player2']
        self.add_value('loser', loser)


def parse_date(text):
    if not isinstance(text, str):
        text = str(text)

    # try fixing bad months and days
    for start, end in [(4, 6), (6, 8)]:
        if text[start:end] == '00':
            text = text[:start] + '01' + text[end:]

    try:
        return datetime.strptime(text, "%Y%m%d")
    except ValueError:
        return None
