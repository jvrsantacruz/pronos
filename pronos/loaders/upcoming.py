import re
from datetime import datetime

from scrapy.loader import ItemLoader
from scrapy.loader.processors import MapCompose, TakeFirst

from pronos.items import Event, Match, Player, MatchResult


class EventLoader(ItemLoader):
    default_item_class = Event
    default_output_processor = TakeFirst()

    def __init__(self, *args, **kwargs):
        super(EventLoader, self).__init__(*args, **kwargs)
        response = kwargs['response']
        self.add_value('type', 'event')
        self.add_value('url', response.url)
        self.add_value('date', response.meta['date'])
        self.add_value('gender', self._find_gender())
        self.add_css('name', 'td.event-name.a::text')
        self.add_css('url', 'td.event-name.a::href')

    def _find_gender(self):
        value = self.selector.root.attrib['data-gender']
        if value == 'm':
            return 'male'
        elif value == 'female':
            return 'female'


class MatchLoader(ItemLoader):
    default_item_class = Match
    default_output_processor = TakeFirst()

    def __init__(self, *args, **kwargs):
        event = kwargs.pop('event')
        super(MatchLoader, self).__init__(*args, **kwargs)
        self.add_value('type', 'match')
        self.add_value('event', event)
        self.add_value('gender', event['gender'])
        self.add_value('date', event.get('date'))
        self.add_css('round', 'td.round::text', re='(\w+)')
        players = []
        for player in self.selector.css('td.player-name'):
            name = player.css('a::text').extract_first()

            # Matches without one player often appear
            # but are not visible on the # web interface
            if not name:
                self.replace_value('type', 'invalid')
                return

            url = player.css('a::attr(href)').extract_first()
            name = name.strip()
            country = ''.join(player.css('td::text').extract())\
                .strip().replace('(', '').replace(')', '')
            repeated = player.css('.badge::text').extract_first()
            repeated = repeated and repeated.strip()
            players.append(dict(
                url=url,
                name=name,
                country=country,
                repeated=repeated,
                gender=event['gender']
            ))

        self.add_value('player1', players[0])
        self.add_value('player2', players[1])
