"""Scrap matchstat matches and players"""
import urllib.parse

import scrapy

from pronos.loaders.matchstat import (PlayerLoader, PlayerEventLoader,
                                      PlayerMatchLoader)
from pronos.spiders.queue_spider import QueueSpider


class MatchstatSpider(QueueSpider):
    """Load player profile and all its matches"""
    name = 'matchstat'
    allowed_domains = ['matchstat.com']
    player_url = 'https://matchstat.com/tennis/player/{name}'
    matches_url = 'https://matchstat.com/tennis/player-record/{name}/{n}'
    start_urls = []
    step = 30

    def request_from_item(self, player):
        return scrapy.Request(
            self._url_from_player(player),
            callback=self.parse_player,
            meta={'player': player}
        )

    def _url_from_player(self, player):
        return self.player_url.format(name=urllib.parse.quote(player['name']))

    def parse_player(self, response):
        response.meta['gender'] = self._find_player_gender(response)
        loader = PlayerLoader(response=response)
        player = loader.load_item()
        yield player
        yield self._match_request_from_player(player, 0)

    def _find_player_gender(self, response):
        event_url = response.css('.event-name a::attr(href)').extract_first()
        if '/w/' in event_url:
            return 'female'
        elif '/m/' in event_url:
            return 'male'
        raise Exception('Unknown gender for player %r', response.url)

    def _match_request_from_player(self, player, n):
        return scrapy.Request(
            self._matches_url(player, n),
            callback=self.parse_matches,
            meta=dict(player=player, n=n)
        )

    def _matches_url(self, player, n):
        return self.matches_url.format(
            name=urllib.parse.quote(player['name']), n=n
        )

    def parse_matches(self, response):
        if not response.text:
            self.logger.info('Last match page reached for %r', response.url)
            return

        # Table with alternate event and match rows
        event = None
        for row in response.css('tr'):
            if row.xpath('@class'):
                loader = PlayerEventLoader(response=response, selector=row)
                event = loader.load_item()
            else:
                loader = PlayerMatchLoader(
                    response=response, selector=row,
                    event=event, gender=event and event.get('gender')
                )
                match = loader.load_item()
                # Event surface is determined by first match
                if not event.get('surface'):
                    event['surface'] = match.get('surface')
                    yield event
                yield match

        # Next page
        yield self._match_request_from_player(
            response.meta['player'], response.meta['n'] + self.step
        )
