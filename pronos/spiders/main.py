from datetime import datetime

import scrapy

from pronos.loaders.matchstat import (MatchLoader, ResultLoader, EventLoader,
                                      PlayerLoader)


class MainSpider(scrapy.Spider):
    name = 'main'
    allowed_domains = ['matchstat.com']
    calendar_url = 'https://matchstat.com/tennis/calendar'
    pipelines = set(['MainPipeline'])

    def __init__(self, years='', genders='', *args, **kwargs):
        self.queues = kwargs.pop('queues', [])
        super().__init__(*args, **kwargs)
        if '-' in years:
            start, end = [int(y) for y in years.split('-', 1)]
            self.years = range(start, end + 1)
        elif years.isdigit():
            self.years = [int(y.strip()) for y in years.split(',')]
        else:
            self.years = list(range(1998, 2018))

        self.genders = [g.strip() for g in genders.split(',') if g.strip()]
        self.genders = self.genders or ['male', 'female']
        assert 'male' in self.genders or 'female' in self.genders

        self.start_urls = [
            self.calendar_url + '/' + str(year) for year in self.years
        ]

    def parse(self, response):
        """Parse calendar page and find events"""
        urls = {'male': [], 'female': []}
        date = datetime(
            year=int(response.url.rsplit('/', 1)[1]), month=1, day=1
        )

        if 'male' in self.genders:
            urls['male'].extend(
                response.xpath(
                    '//a[re:test(text(), ".* F\d+ Futures")]/@href'
                ).extract()
            )

        if 'female' in self.genders:
            urls['female'].extend(
                response.xpath(
                    '//a[re:test(text(), "\d+k .*, Wk. \d+")]/@href'
                ).extract()
            )

        for gender, urls in urls.items():
            for url in urls:
                yield scrapy.Request(
                    url,
                    callback=self.parse_event,
                    meta=dict(gender=gender, date=date)
                )

    def parse_event(self, response):
        event = EventLoader(response=response).load_item()
        response.meta['event'] = event
        yield event
        yield from self._get_matches(response)

    def _get_matches(self, response):
        for selector in response.css('tr.match'):
            loader = MatchLoader(
                response=response,
                selector=selector,
                meta=response.meta,
            )
            match = loader.load_item()
            if match['type'] == 'invalid':
                self.logger.info('Ignoring phony match %r', match)
                return

            response.meta['match'] = match
            yield match
            yield from self._get_result(response, selector)
            yield from self._get_players(response, selector)

    def _get_result(self, response, selector):
        loader = ResultLoader(response=response, selector=selector)
        result = loader.load_item()
        if result['score'] == 'invalid':
            self.logger.info('Ignoring result without final score: %r', result)
        else:
            yield result

    def _get_players(self, response, selector):
        for url in selector.css('td.player-name a::attr(href)').extract():
            yield scrapy.Request(
                url, callback=self.parse_player, meta=response.meta
            )

    def parse_player(self, response):
        yield PlayerLoader(response=response).load_item()
