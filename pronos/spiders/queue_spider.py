"""Base spider to """
import scrapy


class QueueSpider(scrapy.Spider):
    def __init__(self, queue, *args, **kwargs):
        self.queue = queue
        super(QueueSpider, self).__init__(*args, **kwargs)

    @classmethod
    def from_crawler(cls, crawler, *args, **kwargs):
        spider = super(QueueSpider, cls)\
            .from_crawler(crawler, *args, **kwargs)
        crawler.signals.connect(spider.spider_idle, scrapy.signals.spider_idle)
        return spider

    def spider_idle(self, spider):
        player = self.queue.pop(sleep_wait=False)
        if player:
            request = self.request_from_item(player)
            self.crawler.engine.crawl(request, spider)
        raise scrapy.exceptions.DontCloseSpider()
