"""Spider for tennisabstract.com"""
import re
import json
from datetime import datetime

import scrapy
import execjs

from pronos.spiders.queue_spider import QueueSpider
from pronos.items import Player, Match, Event, MatchResult
from pronos.loaders.abstract import (PlayerLoader, Player2Loader, EventLoader,
                                     MatchLoader, ResultLoader)


class AbstractSpider(QueueSpider):
    """Load player profile and all its matches"""
    name = 'abstract'
    allowed_domains = ['tennisabstract.com', 'www.minorleaguesplits.com']
    #player_url = 'http://www.tennisabstract.com/cgi-bin/wplayer.cgi?p={name}'
    player_url = 'http://www.minorleaguesplits.com/tennisabstract'\
        '/cgi-bin/jsmatches/{name}.js'
    player_list_url = 'http://www.minorleaguesplits.com/tennisabstract'\
        '/cgi-bin/jsplayers/mwplayerlist.js'

    start_urls = [player_list_url]
    step = 30
    players = {}

    def request_from_item(self, player):
        return scrapy.Request(
            self._url_from_player(player),
            callback=self.parse_player,
            meta={'player': player}
        )

    def _url_from_player(self, player):
        return self.player_url.format(name=player['name'].replace(' ', ''))

    def parse(self, response):
        """Parse player list

        This is a javascript initialization with all the names in this format:

            var playerlist=[
                '(M) Cheong Eui Kim',
                '(W) Kristina Brandi',
                (..)
            ];

        It's a rather heavy list so we might be caching it at some point
        """
        if self.players:
            return

        js = execjs.compile(response.text)
        self.players = {p['name']: p for p in
                        self._parse_player_list(self._eval(js, 'playerlist'))}
        yield from (
            scrapy.Request(
                self._url_from_player(player),
                meta={'player': player},
                callback=self.parse_player
            )
            for player in self.players.values()
        )

    def _parse_player_list(self, entries):
        for entry in entries:
            match = re.match('\((.)\) (.*)', entry)
            if match:
                player = dict(name=match.group(2))
                if match.group(1) == 'M':
                    player['gender'] = 'male'
                elif match.group(1) == 'W':
                    player['gender'] = 'female'
                yield player

    def parse_player(self, response):
        """Parse player page"""
        name = response.meta['player']['name']
        if name in self.players:
            response.meta['player'].update(self.players[name])

        js = execjs.compile(response.text)
        loader = PlayerLoader(js=js, response=response)
        player = loader.load_item()
        yield player
        yield from self._get_entries(response, js, player)

    def _get_entries(self, response, js, player):
        header = ('date', 'tournament', 'surface', 'prize', 'won',  'ranking',
                  '', '', 'round', 'score', '', 'player2_name', 'player2_rank',
                  '', '', '', 'player2_age', '', '', 'player2_country', '', '',
                  '', '', '', '', '', '', '', '', '', '', '', '', '', '', '',
                  '', '', '', '', '', '', 'code')
        for row in self._eval(js, 'matchmx'):
            entry = dict(zip(header, row))
            if 'code' not in entry:
                entry['code'] = row[-1]
            try:
                yield from self._get_entry(response, player, entry)
            except Exception:
                self.logger.exception(
                    'Error while loading entry %r %r %r %r',
                    response.url, player, row, entry
                )

    def _get_entry(self, response, player, entry):
        event = EventLoader(entry=entry, response=response).load_item()
        yield event
        player2 = Player2Loader(entry=entry, response=response).load_item()
        yield player2
        yield self.request_from_item(player2)
        match = MatchLoader(
            entry=entry, event=event, player=player,
            player2=player2, response=response
        ).load_item()
        yield match
        result = ResultLoader(
            entry=entry, match=match, response=response
        ).load_item()
        yield result

    def _eval(self, ctx, name):
        try:
            return ctx.eval(name)
        except execjs._exceptions.ProgramError:
            return []
