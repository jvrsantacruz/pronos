from datetime import datetime

import scrapy

from pronos.db import get_session, Upcoming, Match, Event, Result
from pronos.loaders.matchstat import MatchLoader, EventLoader, ResultLoader


class ResultSpider(scrapy.Spider):
    name = 'result'
    allowed_domains = ['matchstat.com']
    pipelines = set(['UpcomingPipeline'])

    def start_requests(self):
        # Event url of upcoming matches with no result
        return (
            scrapy.Request(url) for (url,) in
            get_session()
            .query(Event.url)
            .join(Match)
            .join(Upcoming)
            .outerjoin(Result)
            .filter(Result.id == None)
            .filter(Event.url != None)
            .distinct(Event.url)
        )

    def parse(self, response):
        response.meta['date'] = datetime.utcnow()
        for row in self._find_match_rows(response):
            yield from self._parse_row(response, row)

    def _find_match_rows(self, response):
        return response.css('tr.match')

    def _parse_row(self, response, row):
        event = EventLoader(response=response, selector=row).load_item()
        match = MatchLoader(
            response=response, selector=row, event=event
        ).load_item()
        result = ResultLoader(
            response=response, selector=row, match=match
        ).load_item()
        yield result
