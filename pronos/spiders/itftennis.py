import itertools
from datetime import timedelta
from urllib.parse import urlparse, parse_qsl, urlencode

import scrapy

from pronos.loaders.itftennis import (MatchLoader, ResultLoader, EventLoader,
                                      PlayerLoader)


class ItftennisSpider(scrapy.Spider):
    name = 'itftennis'
    allowed_domains = ['itftennis.com']
    start_urls = [
        'http://itftennis.com/procircuit/tournaments/men\'s-calendar.aspx',
        'http://itftennis.com/procircuit/tournaments/women\'s-calendar.aspx',
    ]

    def parse(self, response):
        urls = response.xpath(
            '//a[contains(@href, "info.aspx?tournamentid=")]/@href'
        ).extract()
        if not urls:
            raise Exception("Could not load the real page")

        for url in urls:
            meta = {'gender': 'female' if 'women' in url else 'male'}
            yield scrapy.Request(
                response.urljoin(url),
                callback=self.parse_event,
                meta=meta
            )

            yield scrapy.Request(
                response.urljoin(self._event_player_list_url(url)),
                callback=self.parse_player_list,
                meta=meta
            )

    def parse_event(self, response):
        loader = EventLoader(response=response)
        event = loader.load_item()
        yield event

        # Each event day has matches to play
        for date in self._event_dates(event):
            yield scrapy.Request(
                response.urljoin(self._event_day_url(event, date)),
                callback=self.parse_event_day,
                meta={'date': date, 'event': event}
            )

    def parse_event_day(self, response):
        courts = response.xpath(
            '//table[@class="oopGrid"]/tr/th/text()'
        ).extract()[1:]
        cells = response.xpath(
            '//table[@class="oopGrid"]/tr[position() > 1]/td[position() > 1]'
        )
        rows = itertools.zip_longest(*[iter(cells)] * len(courts))
        for order, row in enumerate(rows, 1):
            for court, cell in zip(courts, [cell for cell in row if cell]):
                match_loader = MatchLoader(
                    response=response,
                    selector=cell,
                    court=court,
                    order=order,
                    date=response.meta['date'],
                    event=dict(response.meta['event'])
                )
                match = match_loader.load_item()
                yield match

                if match_loader.has_result():
                    result_loader = ResultLoader(
                        response=response,
                        selector=cell,
                        match=match,
                    )
                    yield result_loader.load_item()

    def parse_player_list(self, response):
        rows = response.xpath(
            '/tr[.//a[contains(@href, "profile.aspx")]]'
        ).extract()
        for row in rows:
            loader = PlayerLoader(response=response, selector=row)
            yield loader.load_item()

    def _event_player_list_url(self, url):
        event = dict(parse_qsl(urlparse(url).query)).get('tournamentid')
        return '/itf/web/usercontrols/tournaments/tournamententrylist.aspx' + \
            '?tcc=MT&eventid=' + event

    def _event_day_url(self, event, date):
        return '/itf/web/usercontrols/tournaments'\
            '/tournamentorderofplaypage.aspx' + \
            '?' + urlencode({
                'tournamentId': event['idweb'],
                'tcc': 'WT',
                'oopdate': date.strftime('%d/%m/%Y')
            })

    def _event_dates(self, event):
        start, end = event.get('start_date'), event.get('end_date')
        if start and end:
            return [start + timedelta(n) for n in range((end - start).days)]
        return []
