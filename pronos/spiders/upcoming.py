from datetime import datetime

import scrapy

from pronos.loaders.matchstat import PlayerLoader
from pronos.loaders.upcoming import MatchLoader, EventLoader


class UpcomingSpider(scrapy.Spider):
    name = 'upcoming'
    allowed_domains = ['matchstat.com']
    start_urls = ['https://matchstat.com/tennis/all-upcoming-matches']
    pipelines = set(['UpcomingPipeline'])

    def parse(self, response):
        response.meta['date'] = datetime.utcnow()
        for row in self._find_match_rows(response):
            yield from self._parse_row(response, row)

    def _find_match_rows(self, response):
        return response.css('tr.match')

    def _parse_row(self, response, row):
        event = EventLoader(response=response, selector=row).load_item()
        match = MatchLoader(response=response, selector=row).load_item()
        yield event
        yield match
        for player in match['players']:
            yield scrapy.Request(
                player['url'],
                callback=self.parse_player,
                meta={'gender': event['gender']}
            )

    def parse_player(self, response):
        yield PlayerLoader(response=response).load_item()
