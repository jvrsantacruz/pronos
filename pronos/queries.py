import random

import pandas as pd
from sqlalchemy.orm import joinedload, aliased

from pronos.db import *
from pronos import values


def numerize(values):
    return {value: n for n, value in enumerate(values, 1)}


hands = numerize(values.hands)
rounds = numerize(values.rounds)
genders = numerize(values.genders)
surfaces = numerize(values.surfaces)


def make_upcoming_query():
    return make_query().join(Upcoming)


def make_query():
    player1 = aliased(Player)
    player2 = aliased(Player)

    session = get_session()
    return session.query(Match)\
        .options(
            joinedload(Match.event),
            joinedload(Match.player1),
            joinedload(Match.player2),
            joinedload(Match.result)
        )\
        .join(Match.event)\
        .join(Match.result)\
        .join(player1, Match.player1)\
        .join(player2, Match.player2)\
        .filter(Match.date != None)\
        .filter(Event.prize != None)\
        .filter(Match.surface != None)\
        .filter(player1.born != None)\
        .filter(player1.ranking != None)\
        .filter(player2.born != None)\
        .filter(player2.ranking != None)\
        .filter(Result.match_id == Match.id)\
        .filter(Result.match_id == Match.id)\
        .filter((Match.date - player1.born) < 50)\
        .filter((Match.date - player2.born) < 50)\
        .filter(Match.date >= datetime.datetime(1995, 1, 1))


def iter_matches(query=None):
    for m in query or make_query():
        # Randomize player position for the g
        # as most matches display player1 as the winner
        players = [m.player1, m.player2]
        random.shuffle(players)
        player1, player2 = players

        p1age = player1.age_at_or_none(m.date)
        p1height = as_number(player1.height)
        p1weight = as_number(player1.weight)
        p1ranking = as_number(player1.ranking)
        p2age = player2.age_at_or_none(m.date)
        p2height = as_number(player2.height)
        p2weight = as_number(player2.weight)
        p2ranking = as_number(player2.ranking)

        yield dict(
            date=m.date,
            round=rounds.get(m.round),
            prize=m.event.prize,
            gender=genders.get(m.gender),
            surface=surfaces.get(m.surface),
            place=m.event.place,
            p1name=player1.name,
            p1age=p1age,
            p1hand=hands.get(player1.hand),
            p1ranking=p1ranking,
            p1weight=p1weight,
            p1height=p1height,
            p1country=player1.country,
            p2name=player2.name,
            p2age=p2age,
            p2hand=hands.get(player2.hand),
            p2ranking=p2ranking,
            p2weight=p2weight,
            p2height=p1height,
            p2country=player2.country,
            age_diff=sub_or_none(p1age, p2age),
            ranking_diff=sub_or_none(p1ranking, p2ranking),
            weight_diff=sub_or_none(p1weight, p2weight),
            height_diff=sub_or_none(p1height, p2height),
            hand_diff=eq_or_none(player1.hand, player2.hand),
            wonp1=m.result and int(m.result.winner == player1)
        )


def get_matches():
    return list(iter_matches())


def base_query():
    player1 = aliased(Player)
    player2 = aliased(Player)

    return session.query(Match)\
        .options(
            joinedload(Match.event),
            joinedload(Match.player1),
            joinedload(Match.player2),
            joinedload(Match.result)
        )\
        .join(Match.event)\
        .join(Match.result)\
        .join(player1, Match.player1)\
        .join(player2, Match.player2)


def get_totals():
    return {
        'events': session.query(Event).count(),
        'matches': session.query(Match).count(),
        'players': session.query(Player).count(),
        'results': session.query(Result).count(),
    }


def as_number(text):
    value = text
    if isinstance(text, str):
        try:
            value = float(text.replace('m', '').replace('kg', '').strip())
        except (ValueError, AttributeError):
            return None
    if pd.isnull(value):
        return None
    return value


def age_at(player, match):
    return (match.date - player.born).days // 365


def sub_or_none(value1, value2):
    if not pd.isnull(value1) and not pd.isnull(value2):
        return value1 - value2


def eq_or_none(value1, value2):
    if not pd.isnull(value1) and not pd.isnull(value2):
        return int(value1 == value2)
