from types import SimpleNamespace as Namespace

import click

from pronos.crawl import run_spider
from pronos.prediction import prediction
from pronos.explore import main as training, algorithm_names

db_option = click.option(
    '-d', '--db', metavar='URI',
    default='sqlite:///data.sqlite',
    help='Database connection URI',
    show_default=True
)
log_option = click.option(
    '-l', '--log', type=click.Path(dir_okay=False),
    help='Path to the output log file'
)
queue_option = click.option(
    '-q', '--queue', type=click.Path(file_okay=False),
    metavar='URI', help='URI to the local queue'
)
no_middlewares_option = click.option(
    '-M', '--no-middlewares', is_flag=True,
    help='Disable proxies and throttling middlewares'
)

def scraping_options(function):
    options = [db_option, log_option, queue_option, no_middlewares_option]
    for option in options:
        function = option(function)
    return function


@click.group()
def main():
    pass


@main.command()
@click.option('-s', '--spider', multiple=True,
              help='Spiders to enable in this crawler')
@click.option('-y', '--years', default='2017',
              help='Years of historic archive to crawl',
              show_default=True)
@scraping_options
def historic(**kwargs):
    """Crawl historic data"""
    run_spider('main', Namespace(**kwargs))


@main.command()
@scraping_options
def upcoming(**kwargs):
    """Crawl upcoming matches"""
    run_spider('upcoming', Namespace(**kwargs))


@main.command()
@scraping_options
def results(**kwargs):
    """Crawl new results of recent matches"""
    run_spider('result', Namespace(**kwargs))


@main.command()
@db_option
@click.option(
    '-i', '--interactive', is_flag=True,
    help='Start interactive prompt after training'
)
@click.option(
    '-o', '--output', default='figures',
    type=click.Path(file_okay=False),
    help='Output directory plot output, charts and figures',
    show_default=True
)
@click.option(
    '-p', '--plots', is_flag=True,
    help='Plot charts and figures based on data and models'
)
@click.option(
    '-t', '--train', is_flag=True,
    help='Train models for given algorithms'
)
@click.option(
    '-m', '--models', type=click.Path(dir_okay=False),
    help='Path to models file'
)
@click.option(
    '-a', '--algorithm', multiple=True, default=[],
    type=click.Choice(algorithm_names),
    help='Use this algorithm for model training'
)
def train(**kwargs):
    """Train one or more models"""
    training(Namespace(**kwargs))


@main.command()
@db_option
@click.option(
    '-m', '--models', type=click.Path(dir_okay=False),
    default='models.pickle',
    help='Path to models file',
    show_default=True
)
@click.option(
    '-a', '--algorithm', default='logistic-regression',
    type=click.Choice(algorithm_names), show_default=True,
    help='Use this algorithm to predict matches'
)
def predict(**kwargs):
    """Predict future matches using a trained model"""
    prediction(**kwargs)
