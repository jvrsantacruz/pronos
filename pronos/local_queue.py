# Thiago Arruda Public Domain
import os, sqlite3
from time import sleep
from pickle import loads, dumps
try:
    from threading import get_ident
except ImportError:
    from dummy_threading import get_ident


class Queue(object):

    _create = 'CREATE TABLE IF NOT EXISTS queue '\
        '('\
        '  id INTEGER PRIMARY KEY AUTOINCREMENT,'\
        '  item BLOB'\
        ')'
    _count = 'SELECT COUNT(*) FROM queue'
    _iterate = 'SELECT id, item FROM queue'
    _push = 'INSERT INTO queue (item) VALUES (?)'
    _write_lock = 'BEGIN IMMEDIATE'
    _popleft_get = 'SELECT id, item FROM queue '\
        'ORDER BY id LIMIT 1'
    _popleft_del = 'DELETE FROM queue WHERE id = ?'
    _peek = 'SELECT item FROM queue '\
        'ORDER BY id LIMIT 1'

    def __init__(self, path):
        if path != ':memory:':
            path = os.path.abspath(path)
        self.path = path
        self._connection_cache = {}
        with self._get_conn() as conn:
            conn.execute(self._create)

    def __len__(self):
        with self._get_conn() as conn:
            for row in conn.execute(self._count):
                return row[0]

    def __iter__(self):
        with self._get_conn() as conn:
            for id, obj_buffer in conn.execute(self._iterate):
                yield loads(bytes(obj_buffer))

    def _get_conn(self):
        id = get_ident()
        if id not in self._connection_cache:
            self._connection_cache[id] = sqlite3.Connection(self.path,
                    timeout=60)
        return self._connection_cache[id]

    def push(self, obj):
        obj_buffer = memoryview(dumps(obj, 2))
        with self._get_conn() as conn:
            conn.execute(self._push, (obj_buffer,))

    def pop(self, sleep_wait=True):
        keep_pooling = True
        wait = 0.1
        max_wait = 2
        tries = 0
        with self._get_conn() as conn:
            id = None
            while keep_pooling:
                conn.execute(self._write_lock)
                cursor = conn.execute(self._popleft_get)
                for row in cursor:
                    id, obj_buffer = row
                    keep_pooling = False
                    break

                conn.commit() # unlock the database
                if not sleep_wait:
                    keep_pooling = False
                    continue
                tries += 1
                sleep(wait)
                wait = min(max_wait, tries/10 + wait)

            if id:
                conn.execute(self._popleft_del, (id,))
                return loads(bytes(obj_buffer))
        return None

    def peek(self):
        with self._get_conn() as conn:
            cursor = conn.execute(self._peek)
            for row in cursor:
                return loads(bytes(row[0]))
