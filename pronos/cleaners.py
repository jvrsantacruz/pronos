import re


def clean_str(value):
    return (value or '').strip().lower()


def clean_hand(value):
    value = clean_str(value)
    if value == 'l':
        return 'left'
    if value == 'r':
        return 'right'
    if 'left' in value:
        return 'left'
    if 'right' in value:
        return 'right'


def clean_gender(value):
    value = clean_str(value)
    if 'female' in value:
        return 'female'
    if 'male' in value:
        return 'male'


def clean_surface(value):
    value = clean_str(value)
    for surface in ('clay', 'grass', 'hard', 'carpet'):
        if surface in value:
            return surface
    return value


def clean_round(value):
    value = clean_str(value)
    if value == 'f':
        return 'final'
    if value == 'sf':
        return 'semi-final'
    if value in ('fq', 'qf'):
        return 'quarter-final'
    if value in ('r15', 'r16'):
        return 'round-16'
    if value in ('r29', 'r31', 'r32'):
        return 'round-32'
    return value


_event_name_patterns = (
    re.compile(r'$(?P<level>[0-9.,]+)\+H (?P<place>[a-z\'()-_ ])+', re.IGNORECASE),
    re.compile(r'(?P<level>\d+)k (?P<place>[a-z\'()-_ ]+), Wk\. \d+', re.IGNORECASE),
    re.compile(r'(?P<place>[a-z\'()-_ ]+) F\d+ (?P<level>Futures)', re.IGNORECASE),
    re.compile(r'(?P<place>[a-z\'()-_ ]+) F\d+ (?P<level>Futures) \d+', re.IGNORECASE),
    re.compile(r'(?P<place>[a-z\'()-_ ]+) $(?P<level>\d+)k', re.IGNORECASE),
    re.compile(r'(?P<place>[a-z\'()-_ ]+) (?P<level>\d+)k', re.IGNORECASE),
)


def clean_event_name(value):
    value = clean_str(value)

    for pattern in _event_name_patterns:
        match = pattern.match(value)
        if match:
            place, level = match.group('place', 'level')
            if len(place) <= 2:
                continue
            level = level.replace(',', '').replace('.', '')
            if level.isdigit():
                level = int(level)
                if level > 1000:
                    level = level // 1000
                level = str(level) + 'k'

            value = "{} {}".format(place, level)

    return value.strip().strip(' I').strip(' II').strip(' III')\
        .strip(' IV').strip(' V').title()


def clean_ranking(value):
    try:
        return int(clean_str(str(value)).replace(',', '').replace('.', ''))
    except (ValueError, TypeError):
        return None
