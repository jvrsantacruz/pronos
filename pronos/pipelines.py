import logging
import functools

from scrapy.exceptions import DropItem

from pronos.db import get_session
from pronos.items import Event, Player, Match, MatchResult
from pronos.data import (insert_event, insert_player, insert_match,
                         insert_result, insert_upcoming)


log = logging.getLogger('scrapy.pipelines.main')


class ResultPipeline:
    def __init__(self, *args, **kwargs):
        self.session = get_session()
        super().__init__(*args, **kwargs)

    def process_item(self, item, spider):
        return process(item, spider, self.session)


class UpcomingPipeline:
    def __init__(self, *args, **kwargs):
        self.session = get_session()
        super().__init__(*args, **kwargs)

    def process_item(self, item, spider):
        obj = process(item, spider, self.session)
        insert_upcoming(self.session, obj)
        return obj


class MainPipeline:
    def __init__(self, *args, **kwargs):
        self.session = get_session()
        super().__init__(*args, **kwargs)

    def process_item(self, item, spider):
        return process(item, spider, self.session)


@functools.singledispatch
def process(item, spider, session):
    """Unknown item"""
    print('Dropping', item, repr(item), type(item))
    raise DropItem()


@process.register(Event)
def process_event(item, spider, session):
    if not insert_event(session, item):
        log.error('Could not insert %r', item)
    return item


@process.register(Player)
def process_player(item, spider, session):
    if not insert_player(session, item):
        log.error('Could not insert %r', item)
    # pump the player into satellite scrapers
    if spider.name == 'main':
        for queue in spider.queues:
            queue.push(item)
    return item


@process.register(Match)
def process_match(item, spider, session):
    if not insert_match(session, item):
        log.error('Could not insert %r', item)
    return item


@process.register(MatchResult)
def process_result(item, spider, session):
    if not insert_result(session, item):
        log.error('Could not insert %r', item)
    return item
