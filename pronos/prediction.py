import pickle
import collections

import patsy
import numpy as np
import pandas as pd

from sklearn import metrics
from sklearn.svm import SVC
from sklearn.cluster import KMeans
from sklearn.tree import DecisionTreeClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LogisticRegression

from pronos.db import set_session, make_session
from pronos.queries import iter_matches, make_upcoming_query


algorithms = {
    'decision-tree': DecisionTreeClassifier,
    'k-neighbors': KNeighborsClassifier,
    'k-means': KMeans,
#    'linear-regression': LinearRegression,
    'logistic-regression': LogisticRegression,
    'random-forest': RandomForestClassifier,
    'support-vector': SVC,
}
algorithm_names = tuple(algorithms)


def create_matrix(matches):
    d = pd.DataFrame(matches)
    y, X = patsy.dmatrices(
        'wonp1 ~ ranking_diff + age_diff',
        d, return_type='dataframe'
    )
    y = np.ravel(y)

    return y, X


def training_set(matches):
    y, X = create_matrix(matches)
    Datasets = collections.namedtuple(
        'Dataset', 'x_train x_test y_train y_test'
    )
    return Datasets(*train_test_split(X, y, test_size=0.3, random_state=0))


def train_model(datasets):
    model = LogisticRegression()
    model.fit(datasets.x_train, datasets.y_train)
    return model


def test_model(model, datasets):
    prediction = model.predict(datasets.x_test)
    accuracy = metrics.accuracy_score(datasets.y_test, prediction)
    model.calculated_accuracy = accuracy
    print('Accuracy', accuracy)
    try:
        print('Coefficient: \n', model.coef_)
    except (ValueError, AttributeError):
        print('Coefficient: \n', None)
    try:
        print('Intercept: \n', model.intercept_)
    except (ValueError, AttributeError):
        print('Intercept: \n', None)
    print('R² Value: \n', model.score(datasets.x_train, datasets.y_train))
    model.calculated_classification_report = \
        metrics.classification_report(datasets.y_test, prediction)
    print(model.calculated_classification_report)


def create_model(name, datasets):
    print('Training model {} using {} matches'.format(name, len(datasets.x_train)))
    model = algorithms[name]()
    model.fit(datasets.x_train, datasets.y_train)
    test_model(model, datasets)
    return model


def train_models(algorithms, matches):
    datasets = training_set(matches)
    models = {name: create_model(name, datasets) for name in algorithms}
    return models


def save_models(path, models):
    with open(path, 'wb') as stream:
        pickle.dump({name: model for name, model in models.items()}, stream)


def load_models(path):
    with open(path, 'rb') as stream:
        return pickle.load(stream)


def prediction(algorithm, models, db):
    import click

    if db:
        set_session(make_session(db))

    models = load_models(models)
    model = models[algorithm]

    matches = list(make_upcoming_query())
    matches_data = list(iter_matches(matches))

    _, X = create_matrix(matches_data)
    predictions = model.predict(X)

    total_correct, total = 0, len(matches)
    for match, data, guess in zip(matches, matches_data, predictions):
        p1_should_win = bool(guess)
        p1_won = bool(data['p1name'] == match.result.winner.name)
        correct = p1_should_win == p1_won

        if correct:
            total_correct += 1

        click.echo("{} {}".format(
            click.style('OK    ', fg='green') if correct else
            click.style('WRONG ', fg='red'),
            match.id
        ))

    click.echo()
    click.echo('Predicted {:.2f}% {} correct predictions of {}'
               .format(total_correct / total * 100, total_correct, total))
