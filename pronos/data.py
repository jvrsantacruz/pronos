import logging

import scrapy
from sqlalchemy import extract

from pronos.db import Player, Event, Match, Result, Upcoming

log = logging.getLogger('pronos')


def check_fields(fields, item):
    for field in fields:
        if not item.get(field):
            log.warning(
                'Incomplete record %r missing %r field: %r',
                type(item), field, item
            )
            return False
    return True


def create_obj(cls, data):
    forbidden = ['type', 'repeated']
    return cls(**{
        k: v for k, v in data.items()
        if v and not isinstance(v, (dict, scrapy.Item)) and k not in forbidden
    })


def find_by(session, type_, query):
    q = session.query(type_)
    if isinstance(query, dict):
        q = q.filter_by(**query)
    else:
        q = q.filter(*query)
    return q.one_or_none()


def insert_or_update(session, obj, query):
    try:
        query = query if query else dict(name=obj.name)
        existing = find_by(session, type(obj), query)
        if existing:
            obj.id = existing.id
            session.merge(obj)
        else:
            session.add(obj)
        session.commit()
        return obj
    except Exception:
        session.rollback()
        raise


def insert_event(session, item):
    required = ('name', 'gender', 'date')
    #optional = ('date', 'code', 'place', 'url', 'prize', 'surface')
    if not check_fields(required, item):
        return

    query = [
        extract('year', Event.date) == item['date'].year,
        Event.name == item['name']
    ]
    event = create_obj(Event, item)
    return insert_or_update(session, event, query)


def insert_player(session, item):
    required = ('name', 'gender')
    if not check_fields(required, item):
        return
    player = create_obj(Player, item)
    return insert_or_update(session, player, dict(name=item['name']))


def insert_match(session, item):
    required = ('event', 'round', 'player1', 'player2')
    if not check_fields(required, item):
        return

    event = insert_event(session, item['event'])
    if event is None:
        logging.warning('Could not find event %r', item['event'])
        return

    player1 = insert_player(session, item['player1'])
    if player1 is None:
        logging.warning('Could not find player %r', item['player1'])
        return

    player2 = insert_player(session, item['player2'])
    if player2 is None:
        logging.warning('Could not find player %r', item['player2'])
        return

    match = create_obj(Match, item)
    match.event_id = event.id
    match.player1_id = player1.id
    match.player2_id = player2.id

    player_ids = set([player1.id, player2.id])
    query = [
        Match.player1_id.in_(player_ids),
        Match.player2_id.in_(player_ids),
        Match.player1_id != Match.player2_id,
        Match.round == item['round'],
        Match.event_id == event.id
    ]

    return insert_or_update(session, match, query)


def insert_result(session, item):
    required = ('match', 'score', 'winner', 'loser')
    if not check_fields(required, item):
        return

    match = insert_match(session, item['match'])
    if match is None:
        logging.warning('Could not find match %r', item['match'])
        return

    result = create_obj(Result, item)
    result.match_id = match.id
    result.winner_id = _find_player_id(session, item['winner'], match)
    result.loser_id = _find_player_id(session, item['loser'], match)
    return insert_or_update(session, result, dict(match_id=match.id))


def _find_player_id(session, player, match):
    for p in [match.player1, match.player2]:
        if p and p.name == player['name']:
            return p.id

    p = insert_player(session, player)
    if p is None:
        raise Exception('Result has player %r not in match %r', player, match)
    return p.id


def insert_upcoming(session, item):
    if item['type'] != 'match':
        return

    match = insert_match(session, item)
    if match is None:
        logging.warning('Could not find match %r', item['match'])
        return

    upcoming = Upcoming(match=match)
    insert_or_update(session, upcoming, dict(match_id=match.id))
    return upcoming
