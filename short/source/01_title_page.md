\begin{center}
  \LARGE{\textbf{Sistema de predicción de resultados deportivos}}

  \vspace{1cm}

    \large
    Francisco Javier Santacruz López-Cepero   \\
    Manuel Palomo Duarte

\vspace{1.5cm}

\textit{
\small
\textbf{Extracto:}
Sistema de recopilación de datos relativos a partidos de tenis para las ligas menores de la ITF,
permitiendo entrenar un modelo estadístico a partir de datos históricos que realice una predicción
sobre estos eventos.  Los datos empleados para la construcción del modelo son obtenidos de manera
contínua por una serie de "arañas" distribuidas, comunicadas por un sistema de colas, que
consolidan sus datos en una base de datos relacional.  Para la realización del modelo de
predicción, tras un análisis exploratorio se deciden los parámetros más apropiados y se comparan
diferentes algoritmos de "aprendizaje automático", eligiendo finalmente "Regresión Logística" como
el más preciso.
}

\textit{
    \small
    \textbf{Palabras Clave:} Tenis, Web Scraping, Machine Learning
}
\end{center}

# Introducción

La rápida evolución de las redes de comunicación, infraestructura, protocolos y estándares que dan
forma a Internet, nos permiten el acceso a grandes recursos de información disponible públicamente.
Tanto organizada en forma de repositorios o bases de datos como presentada para su consumo humano
en formatos web, estos datos pueden ser obtenidos, clasificados e integrados entre si. Es nuestro
trabajo el conseguir comprender lo que dicen, y a través de su estudio, detectar tendencias y
patrones presentes, permitiendo la creación de modelos que nos permitan observar las relaciones
entre los diferentes aspectos, su valor e importancia.

Esto supone nuevos retos en el tratamiento de esta información, en tanto que todos esos recursos
deben ser obtenidos, procesados, almacenados y analizados para poder extraer conclusiones y
*aprender de ellos*. Grandes cantidades de datos son generadas en muchos campos de la sociedad, lo
que supone que todos los esfuerzos y avances realizados sobre los métodos que permiten su
procesamiento y posterior análisis pueden tener un impacto significativo.

En ese contexto, es por tanto el objetivo de este proyecto el desarrollar un sistema específico que
de una solución sencilla para un problema concreto en uno de estos campos, permitiendo aplicar las
técnicas disponibles para obtener esa información de manera distribuida y analizarla empleando
modelos estadísticos basados en el aprendizaje automático.

## Eventos deportivos

Los eventos deportivos generan grandes cantidades de información que se publica de forma continua.
Cada día se celebran en el mundo miles de eventos, torneos y partidos de toda clase de deportes.
Las competiciones deportivas profesionales tienen un impacto enorme en todo el mundo. Por ejemplo,
se estima que el evento de la *Super Bowl*, organizado anualmente por la  *National Football
League's (NFL)* en los Estados Unidos llega a más de 160 millones de personas, mientras que la
*Champions League* en Europa mejora ese número en más de 50 millones, sin contar que los cientos de
millones que ven la *Rugby World Cup*, la *Cricket World Cup* y la *NBA Finals*[^1.1] [@GamingWorld].

Dado que existe un campo que genera gran cantidad de información de dominio público, de interés
para gran parte de la población, que además mantiene asociado un creciente negocio millonario
basado en la interpretación de estos datos, se consideró como idóneo para el estudio de este tipo
de sistemas predictivos basados en grandes colecciones de datos.

## Aprendizaje Automático

El *Aprendizaje Estadístico* juega un papel clave en muchas áreas de la ciencia y la industria,
aportando a los campos de la Estadística, Inteligencia Artificial, Minería de Datos así como a
diferentes áreas de la ingeniería y otras disciplinas. Típicamente el sistema consiste en realizar
*predicciones* basadas en un conjunto de *propiedades* que tienen los sujetos de estudio; aparte se
cuenta con un conjunto de datos de *entrenamiento*, a partir del cual se construye un modelo
predictivo que permite predecir el resultado de sujetos desconocidos [@Elements].

Las características de estos modelos los hacen especialmente aptos para abordar el reto de predecir
resultados de eventos aún no sucedidos, de los cuales se tiene poca información en el momento pero
del que se cuenta con un amplio histórico. Es por tanto en esta familia de la estadística en la que
nos enfocaremos a la hora de realizar el análisis de los datos obtenidos de competiciones y
eventos.

## Objetivos

Con el desarrollo de este proyecto, comprendiendo la obtención de datos, su análisis y procesado se
pretende:

1. **Evaluar datos públicos**: Como hemos mencionado anteriormente, se generan y publican gran
   cantidad de datos relacionados con las actividades deportivas, sin embargo, inicialmente no
   estamos seguros de su calidad, fiabilidad y completitud. Es por tanto nuestro objetivo el
   construir una base de datos de datos con un mínimo de calidad sobre los que poder trabajar.

1. **Plantear un modelo predictivo**: ¿Es posible obtener información sobre eventos aún no
   realizados? ¿Predecir el resultado de las competiciones? ¿Con qué margen de error? Creación de
   un modelo estadístico que aprenda a partir de los datos obtenidos.

1. **Sistema continuo**: Evitar limitar la obtención de datos y el análisis a una única ocasión,
   sino que el proyecto debe estar preparado para funcionar de manera indefinida, obteniendo nuevos
   datos y proveyendo predicciones para los eventos próximos, mejorando el modelo.

1. **Multipropósito**: El sistema resultante debe encontrarse preparado para trabajar con más de un
   deporte, o debe ser sencillo ampliarlo para que lo haga sin necesidad de modificar la
   arquitectura de la aplicación.

Es por tanto la intención el realizar una herramienta de posible utilidad, a la que le sea posible
seguir funcionando sin intervención, más que un simple análisis puntual o la creación de un modelo
que carezca de uso.

## Alcance

Es enorme la cantidad de diferentes deportes que se practican profesionalmente, cada uno con sus
múltiples y distintas competiciones, normativas y reglas, en ocasiones cambiantes, diferentes
mecánicas, datos asociados, y eventos que se repiten en todo el mundo de manera regular y regulada,
o únicamente en momentos puntuales del año.

A pesar de plantear un sistema multipropósito que cubra diferentes deportes y eventos, esto no es
posible dadas las restricciones de tiempo y trabajo impuestas sobre este proyecto. Es necesario,
por tanto, elegir. Realizar una selección de qué deporte o deportes son los más asequibles para
poder realizar un análisis sencillo, dadas sus propiedades, reglas, datos de calidad disponibles y
frecuencia de competición.

## Tenis

El *tenis* es un deporte que se juega individualmente, o por parejas, muy extendido en la mayoría
de los países, incluso fuera de Europa y Estados Unidos. Se encuentra regulado por la Federación
Internacional de Tenis [@ITF], la cual organiza periódicamente multitud de competiciones en
distintas categorías con mucha frecuencia por todo el mundo.

El tenis, en su modalidad individual, más sencilla, enfrenta a dos jugadores que juegan partidos
acumulando puntos hasta que uno de los dos contrincantes alcanza una diferencia suficiente como
para declararlo ganador. Los jugadores mantienen una carrera estable a lo largo de toda su vida,
jugando bajo su nombre y no incluyéndose en equipos. Existe la figura del entrenador, pero esta
tiene poca relevancia[^1.9].

Los resultados del tenis son dos, gana el primer jugador o el segundo[^1.10] [@TennisRules].
Resultando sencillo determinar la carrera de un jugador en base a su tasa de éxitos. Cada jugador
tiene uno o más clasificaciones según su puntuación asociada. Estos *rankings* evolucionan a lo largo del
tiempo según los partidos que vayan ganando/perdiendo y permiten realizar comparaciones entre
diferentes jugadores en un punto dado.

# Extracción de datos

Podríamos dividir el proyecto conceptualmente en las siguientes partes o componentes:

- **Modelo de datos**: Diseño de la base de datos, implementación, modelo de transacciones,
  estrategia de migraciones.

- **Arañas**: Sistema de obtención de datos, software de red, limpiado de la información,
  distribución de tráfico, tratamiento de las fuentes.

- **Modelo predictivo**: Selección de datos, homogeneización y preparación, entrenamiento y
  herramientas de análisis, modelo final.

Se conoce como *araña* a una pieza de software que recorre páginas Web a través de sus hiper
enlaces, extrayendo información de estos sitios de manera estructurada en el camino.

A la hora de obtener los datos necesarios para entrenar el modelo y realizar las predicciones, fué
necesario contar con varias fuentes de datos diferentes, cuya información exponían en formato web,
pensado originalmente para consumo humano.

A partir de esos datos, obtenidos de manera distribuida, se conforma una base de datos consolidada
sobre la cual se realizan los análisis como puede verse en la figura
\ref{scraper-distribution-diagram}. En el diagrama pueden distinguirse los siguientes elementos:

1. *Scrapers*: Se trata de workers distribuidos que realizan la comunicación por red, el análisis y
    parseo de la información y la distribuyen de forma estructurada al resto de la aplicación.
    Existen diferentes tipos de araña, las cuales obtienen diferentes tipos de datos necesarios
    para la generación del modelo y la predicción. Cada proceso de *Araña* se encuentra compuesto
    por al menos un *crawler*, el cual contiene una serie de *scrapers*, que emplean *loaders* para
    parsear fragmentos de los documentos que obtienen de los servidores, y los persisten y
    distribuyen via un *pipeline* a través de colas, como puede verse en la figura
    \ref{scraper-distribution-diagram}.

2. *Updater*: Recibe los datos de las Arañas, los pone en contexto, resuelve referencias y persiste
    esta información debidamente relacionada en la base de datos. La labor de este componente es
    limpiar y homogeneizar todo lo que el *pipeline* de arañas no haya sido capaz de realizar ya
    así como evitar la duplicación de información.

3. *Base de Datos*: La capa de base de datos permite persistir y relacionar todos los datos en el
   esquema provisto, evitando la duplicación y relacionando todos los registros de manera que sean
   útiles posteriormente. Sus contenidos pueden verse en la figura \ref{data-model-diagram} en la
   página \pageref{data-model-diagram}.

4. *Predicción*: El análisis de datos que obtiene un modelo funcional listo para empezar a predecir
   partidos.

![Esquema general de distribución de los crawlers, scrapers y su comunicación entre ellos. Los crawlers pueden ser procesos o hilos independientes, cada uno conteniendo una serie de scrapers no bloqueantes para un sitio web concreto. Los resultados son procesados por un pipeline que los limpia y organiza, los distribuye entre otros scrapers y los persiste pasándolos a una cola general que los lleva al Updater, donde se almacenan. \label{scraper-distribution-diagram}](source/figures/scraper-distribution-diagram.pdf){ width=100% }


## Fuentes de datos

Las competiciones de tenis se dividen en varias ligas o circuitos, contando con la liga [@ITF], en
categorías Junior, Senior, Playa y Silla de Ruedas, la liga [@ATP] que aglutina multitud de torneos
específicos como *Wimbledon* o el *US Open* en un único circuito.

La totalidad de la literatura existente se centra en el estudio de los resultados de los grandes
torneos los del *Grand Slam* y sobre la parte más alta del ranking de la [@ATP].

Es por esto que se consideró particularmente interesante enfocar este proyecto a la predicción de
partidos pequeños, con jugadores noveles, en un contexto de carencia casi absoluta de datos. Esta
restricción creaba condiciones muy especiales.

- Un *número de parámetros limitado*. Mucha información no está disponible históricamente, o no se
  conoce en el momento de realizar la predicción.

- Los jugadores *tienen poco historial*, al haber jugado pocas veces o ninguna.

- Existe un conjunto de *incentivos* completamente diferente; aunque los premios son menores, pueden
  representar mucho más para jugadores que viven profesionalmente de ello.

- Probable existencia de *patrones distintos* a los de las altas ligas. Al ser el nivel de los
  jugadores *muy poco homogéneo*, existe la posibilidad de encontrar sesgos fuertes o patrones que
  permitan mejorar mucho la predicción. 

- Aunque la *información es pobre* para los resultados de estos estos torneos, no incluyendo en
  ningún caso la información del *Ojo de Halcón* ni un desglose de los juegos ganados, al haber una
  gran cantidad de torneos y jugadores, existe una *gran cantidad de registros*.

La principal fuente es naturalmente la página oficial de [@ITFProCircuit], la cual mantiene una
sección de torneos, donde estos se listan temporalmente[^4.1].  Cada torneo incluye información muy
detallada sobre el tipo de superficia, los jugadores y su orden de juego (visible en la imagen de
la figura \ref{itf-tournament-detail-page} en la página \pageref{itf-tournament-detail-page}). Cada
jugador además incluye una ficha potencialmente muy detallada especificando su edad, sexo,
nacionalidad, ranking, e incluso algunos datos personales como sus aficiones y hobbies o su
superficie favorita.

Debido a múltiples dificultades con la extracción y mantenimiento de los datos en la página
oficial, se inició la búsqueda de otras fuentes de datos alternativas, que ofrecise la misma
información. Estas se dedican a recopilar y ofrecer de forma homogénea el histórico de partidos de
estas ligas a un nivel de detalle mucho menor, y a costa de mayor complejidad, al ser necesario
recopilar información desde varios sitios a la vez.

![Información general y detalles del torneo Canada F5 Futures. Puede apreciarse el orden de juego del día 3 de Septiembre, donde se enfrentaron Filip Peliwo y Ulises Blanch. \label{itf-tournament-detail-page}](source/figures/itf-tournament-detail-page.jpg){ width=90% }

# Predicción

A partir del análisis de los datos que pueden obtenerse referidos a los partidos jugados en
competiciones y torneos, debería sernos posible construir un modelo estadístico que permita
predecir el resultado de un evento no acontecido en base a las características del mismo, con
relativamente poco conocimiento sobre el mismo, basándonos en el entrenamiento sobre datos
históricos.

## Estado de la cuestión

El estudio sistemático del *Tenis* desde un punto de vista científico con vistas a la predicción de
resultados deportivos ha producido una pequeña cantidad de literatura, de cuya lectura y estudio
hemos podido obtener ideas muy valiosas sobre cómo aproximarnos al problema.

Podríamos dividir estos artículos en dos grandes categorías, basándonos fundamentalmente en el tipo
de información que emplean para realizar las predicciones.

Por un lado tendríamos una serie de estudios que emplean *información muy detallada sobre los
partidos*, sobre la cual pretenden modelar el estilo y el desempeño de los distintos jugadores
durante su juego. Basándose en la idea de que cada jugador posee una serie de rasgos
característicos que dominan su forma de competir, y que estos pueden categorizarse y medirse
cuantitativamente, se suponen los jugadores comparables entre si. Estos artículos crean modelos
jerárquicos basados en *Cadenas de Markov* [@Barnett05] [@OMalley08] [@Knottenbelt12].

Otra aproximación es una más *desinformada*, en la que se tienen pocos o ningún dato sobre la
evolución del partido, y se plantea la predicción sobre el conjunto de datos del torneo, el jugador
y su historial, particularmente su efectividad contra el otro contrincante. Esta aproximación
pretende que es posible predecir el resultado de un jugador en base a su nivel de rendimiento
histórico y características físicas, basándose más en la detección de patrones que indiquen cómo
unos tipos de jugadores desempeñan contra otros en el contexto de un torneo determinado [@Clarke00]
[@Ma13] [@Spiko15] [@Amornchai09] [@Kovalchik16].

## Análisis

La extracción del cuerpo de datos histórico que compone la base de datos a partir de la cual se han
hecho las primeras labores de exploración y minado de datos, así como sobre la que se ha entrenado
el modelo posterior tras entrenar todos los algoritmos, se recopiló durante Agosto de 2017 de forma
permanente, siendo 15 días el tiempo total empleado hasta adquirir una base de datos de más de un
millón de registros. Todos estos valores obtenidos fueron insertados en una base de datos
normalizada[^4.4] como puede verse en el modelo de datos de la figura \ref{data-model-diagram}.

Esto nos obligó a realizar primero una criba sobre la cantidad de información que iba a extraerse y
almacenarse, según su disponibilidad. Es necesario además, que los registros sean lo más homogéneos
posible, de forma que sea posible el realizar el entrenamiento, todos deben ser comparables este
si. Viendo esta dificultad, analizamos los datos recopilados, buscando el conjunto mínimo de datos
disponible que nos daba una buena cantidad de partidos.

![Modelo de datos para la base de conocimiento. En el pueden verse las principales entidades distinguidas, sus atributos y relaciones. \label{data-model-diagram}](source/figures/data-model-diagram.pdf){ width=100% }

Estos valores, además, no se encuentran concentrados en unos pocos partidos, sino que a la mayoría
le falta algún dato de una manera u otra. Como puede verse en la figura \ref{data-null-fields} en
la página \pageref{data-null-fields}, si consideramos `prize` como un campo necesario, solo una
pequeña proporción de los partidos recopilados nos sirve de algo. De los datos recabados, no todos
tienen sentido para la predicción.  Por ejemplo, en los datos históricos contamos con entradas muy
antiguas de alrededor de 1900[^4.3] como puede verse en la figura \ref{data-all-matches-per-year}
en la página \ref{data-all-matches-per-year}.

![Número de partidos recopilados por año. Puede observarse como los datos históricos se remontan hasta 1900, pero con una cantidad anecdótica de casos. \label{data-all-matches-per-year}](source/figures/data-all-matches-per-year.pdf)

Ante la pregunta de qué campos son realmente determinantes y ayudan a realizar una predicción,
aplicamos herramientas estadísticas básicas para comprobar si nuestras pequeñas hipótesis se
cumplen, y cuales son las relaciones entre los datos. Aunque todas estas correlaciones nos aporten
ciertas nociones intuitivas sobre cómo se relacionan los datos entre ellos, lo que realmente
queremos saber es si nos sirven para predecir el resultado (`wonp1`). 

![Proporción de valores nulos o no disponibles para cada partido registrado. Mayor es peor.  \label{data-null-fields}](source/figures/data-null-fields.pdf)

Por las características del problema, y por la base de datos que contamos, ya sabemos que queremos
un *algoritmo de aprendizaje supervisado*, que sea rápido en realizar la predicción, ya que debe
formar parte de un sistema dinámico, y que generalice correctamente, es decir, que sea resistente
al sobreentrenamiento, ya que a partir de los partidos conocidos debe ser capaz de interpretar
nuevos partidos también.

Al comparar los modelos, no nos fijamos únicamente en su precisión, sino que generamos una serie de
métricas auxiliares que nos ayudan a entender cómo de bien o mal predice un modelo concreto. Estas
métricas se basan en tres conceptos: *precision*, *recall* y *support*.

En la gráfica de la figura \ref{model-classification-report} podemos observar matrices donde se
comparan las métricas de todos los algoritmos probados. Podemos observar cómo el algoritmo de
**Regresión logística** es el que mantiene mejores resultados para cada uno de los aspectos.
Podemos concluir entonces, a la vista del rendimiendo de los diferentes modelos, que este es el que
mejor se adapta al problema y el que debemos usar para realizar el predictor.

![Métricas de efectividad de los distintos modelos generados. \label{model-classification-report}](source/figures/model-classification-report.pdf)


# Trabajo futuro

Dado el estado del proyecto, se dejan abiertas las siguientes líneas de avance para futuras
modificaciones:

- Inclusión de recopilación de datos para otros eventos deportivos: El diseño de la aplicación
  debería permitir el encaje de varios tipos de deportes muy distintos, habiéndose realizado este
  de la manera más genérica posible, evitando las asunciones propias del deporte elegido para la
  primera implementación, esto es, el *Tenis*.

- Modelado de otros deportes: El análisis exploratorio y el modelado estadístico realizado para el
  *Tenis* es difícilmente válido para otros tipos de deportes, incluso de tipo individual. Es por
  tanto interesante la inclusión de modelado para otros deportes, empleando un enfoque diferente
  para admitir comparaciones de equipos y otros tipos de resultados no binarios.

- Visualización de datos: El proyecto se centra en la recopilación, procesado y análisis
  estadístico de los datos existentes de *Tenis*, sin embargo no realiza grandes esfuerzos en
  visualizar esos datos y en facilitar el acceso a los mismos, ya que se entiende que no es la
  finalidad última del proyecto, a parte de existir otros proyectos[^1.13] que ya realizan esa
  tarea de manera muy efectiva.

- Visualización de algoritmos: Los algoritmos de aprendizaje automático empleados para modelar la
  predicción de resultados pueden ser visualizados para su mejor comprensión. Esta labor excede el
  alcance del proyecto, ya que, aunque se compara entre diferentes algoritmos, no se realiza un
  trabajo exhaustivo en este sentido, evaluando simplemente su eficacia en términos de error
  relativo, y no en su rendimiento computacional.


[^1.1]: Ver [@GamingWorld] capitulo primero.
[^1.2]: Ver el informe financiero elaborado por Deloitte [@FootballReview2017] figura *Big five European league club's reneuves - 2015/16*.
[^1.3]: Ver el informe de la Comisión Europea [@OnlineGamblingStudy] apartado *Online gambling: an emerging policy issue*.
[^1.4]: Ver [@FCoachPerf] donde se analiza el efecto de los entrenadores sobre el rendimiento de equipos de fútbol universitarios.
[^1.5]: Salvo en cierto tipo de competiciones, donde no se permite el empate, forzando un ganador al final del tiempo.
[^1.6]: Según las reglas de la [@FIBA].
[^1.7]: Según las reglas de la [@NBA].
[^1.8]: Salvo en caso de fase final ida/vuelta, en que puede haber empate en el partido de ida o en
        el de vuelta, decidiéndose el ganador por la suma de puntos de ambos partidos.
[^1.9]: No se conoce estudio sistemático que analice la influencia del entrenador o falta de ella
        en los resultados de competición en tenis, se trata esto, por tanto, de una opinión
        personal.
[^1.10]: En caso de que el partido rebase cierta duración sin que ninguno de los dos jugadores
         obtenga una diferencia significativa en puntuación que determine el resultado, se
         establece un procedimiento denominado *Tie-Break* o *Muerte Súbita*. Ver [@TennisRules].
[^1.11]: Ignorando la categoría de dobles.
[^1.12]: Incluyendo periódicos escándalos de dopaje y tráfico de influencias en equipos y
         federaciones.
[^1.13]: Ver el proyecto colaborativo de datos abiertos [@TennisAbstract]. Este proyecto recopila,
         organiza, limpia y ofrece una base de datos de partidos de tenis de forma libre.

[^4.1]: Los torneos permanecen en la página web de manera indefinida, pero muchos de sus datos,
        incluyendo el orden de juego y resultados, son retirados tras unas pocas semanas.
[^4.2]: Para más información sobre cómo se disponen los datos en esta página y como fueron
        extraidos, consultar el capítulo *Implementación*.
[^4.3]: Esto es debido a que, al analizar el historial de un jugador, también se recopilan todos
        sus partidos jugados, y en ellos, se obtiene a su vez el historial de sus contrincantes.
        El efecto encadenado hace que se incorporen muchos partidos no directamente relacionados a
        la base de datos, llegando muy atrás, hasta 1900.
[^4.4]: Ver capítulo *Análisis y Diseño* para obtener una idea más precisa sobre el esquema de la
        base de datos.
[^4.5]: Las gráficas de este capítulo han sido generadas empleando `python`, empleando las
        librerías `pandas`, `matplotlib` y `seaborn`. Su generación se encuentra automatizada en el
        script `explore.py`, incluido junto al resto del código de aplicación. Ver el capítulo
        *Implementación* para más información.

\footnotesize

<!-- 
Do not edit this page.

References are automatically generated from the BibTex file (References.bib)

...which you should create using your reference manager.
-->

# Referencias
