import os
import random
import argparse
import collections

import patsy
import numpy as np
import pandas as pd

from sqlalchemy.orm import joinedload, aliased

from sklearn import metrics
from sklearn.svm import SVC
from sklearn.cluster import KMeans
from sklearn.tree import DecisionTreeClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LogisticRegression, LinearRegression

from pronos.db import *
from pronos import values


url = os.getenv('URL') or 'sqlite:///bench.sqlite'
create_all(url)
session = make_session(url)

def numerize(values):
    return {value: n for n, value in enumerate(values, 1)}


hands = numerize(values.hands)
rounds = numerize(values.rounds)
genders = numerize(values.genders)
surfaces = numerize(values.surfaces)


def age_at(player, match):
    return (match.date - player.born).days // 365


def make_query():
    player1 = aliased(Player)
    player2 = aliased(Player)

    return session.query(Match)\
        .options(
            joinedload(Match.event),
            joinedload(Match.player1),
            joinedload(Match.player2),
            joinedload(Match.result)
        )\
        .join(Match.event)\
        .join(Match.result)\
        .join(player1, Match.player1)\
        .join(player2, Match.player2)\
        .filter(Match.date != None)\
        .filter(Event.prize != None)\
        .filter(Match.surface != None)\
        .filter(player1.born != None)\
        .filter(player1.ranking != None)\
        .filter(player2.born != None)\
        .filter(player2.ranking != None)\
        .filter(Result.match_id == Match.id)


def iter_matches(query=None):
    for m in query or make_query():
        # Randomize player position for the matrix
        # as most matches display player1 as the winner
        players = [m.player1, m.player2]
        random.shuffle(players)
        player1, player2 = players

        yield dict(
            date=m.date,
            round=rounds[m.round],
            prize=m.event.prize,
            gender=genders[m.gender],
            surface=surfaces[m.surface],
            p1age=player1.age_at(m.date),
            p1ranking=player1.ranking,
            p2age=player2.age_at(m.date),
            p2ranking=player2.ranking,
            wonp1=int(m.result.winner == player1)
        )


def get_matches():
    return list(iter_matches())


def training_set(matches):
    d = pd.DataFrame(matches)

    y, X = patsy.dmatrices(
        'wonp1 ~ prize + p1age + p1ranking + p2age + '
        'p2ranking + C(surface) + C(gender) + C(round)',
        d, return_type='dataframe'
    )
    y = np.ravel(y)

    Datasets = collections.namedtuple(
        'Dataset', 'x_train x_test y_train y_test'
    )
    return Datasets(*train_test_split(X, y, test_size=0.3, random_state=0))


def train_model(datasets):
    model = LogisticRegression()
    model.fit(datasets.x_train, datasets.y_train)
    return model


def test_model(model, datasets):
    prediction = model.predict(datasets.x_test)
    print('Accuracy', metrics.accuracy_score(datasets.y_test, prediction))
    print(metrics.classification_report(datasets.y_test, prediction))
    #print(model.predict(X_test))
    #print(model.predict_proba(X_test))


def create_model(name, datasets):
    print('Training model {} using {} matches'.format(name, len(datasets.x_train)))
    model = algorithms[name]()
    model.fit(datasets.x_train, datasets.y_train)
    test_model(model, datasets)
    return model


def train_models(algorithms, matches):
    datasets = training_set(matches)
    models = {name: create_model(name, datasets) for name in algorithms}
    return models


algorithms = {
    'decision-tree': DecisionTreeClassifier,
    'k-neighbors': KNeighborsClassifier,
    'k-means': KMeans,
#    'linear-regression': LinearRegression,
    'logistic-regression': LogisticRegression,
    'random-forest': RandomForestClassifier,
    'support-vector': SVC,
}
algorithm_names = tuple(algorithms)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '-a', '--algorithm', action='append', default=[],
        choices=algorithm_names + ('all',)
    )
    args = parser.parse_args()
    if 'all' in args.algorithm:
        args.algorithm = algorithm_names


    print('Querying matches')
    all_matches = get_matches()
    models = train_models(args.algorithm, all_matches)

    from IPython import embed
    embed()
