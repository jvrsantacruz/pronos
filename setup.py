from setuptools import setup, find_packages


setup(
    name='pronos',
    description='Tennis pronostics',
    version='0.0.1',
    author='Javier Santacruz',
    author_email='javier.santacruz.lc@gmail.com',
    url='https://gitlab/jvrsantacruz/pronos',
    install_requires=open('requirements.txt').read().splitlines(),
    license='Propietary',
    include_package_data=True,
    #package_data={'pronos': ['default.master.toml', 'default.replica.toml']},
    entry_points={'console_scripts': ['pronos = pronos.cli:main']},
    packages=find_packages()
)
