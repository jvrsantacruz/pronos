"""Add upcoming table

Revision ID: 68e5f391f8cd
Revises: cd4a905fb796
Create Date: 2017-08-30 21:15:27.126849

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '68e5f391f8cd'
down_revision = 'cd4a905fb796'
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        'upcoming',
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('match_id', sa.Integer(), nullable=True),
        sa.Column('date', sa.DateTime(), nullable=True),
        sa.ForeignKeyConstraint(['match_id'], ['matches.id'], ),
        sa.PrimaryKeyConstraint('id')
    )


def downgrade():
    op.drop_table('upcoming')
