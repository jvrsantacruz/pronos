"""empty message

Revision ID: cd4a905fb796
Revises:
    Create Date: 2017-08-30 21:06:56.882217

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'cd4a905fb796'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        'events',
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('name', sa.String(), nullable=False),
        sa.Column('code', sa.String(), nullable=True),
        sa.Column('gender', sa.String(), nullable=True),
        sa.Column('date', sa.DateTime(), nullable=True),
        sa.Column('place', sa.String(), nullable=True),
        sa.Column('url', sa.String(), nullable=True),
        sa.Column('prize', sa.Integer(), nullable=True),
        sa.Column('surface', sa.String(), nullable=True),
        sa.PrimaryKeyConstraint('id')
    )
    op.create_table(
        'players',
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('name', sa.String(), nullable=False),
        sa.Column('country', sa.String(), nullable=True),
        sa.Column('ranking', sa.Integer(), nullable=True),
        sa.Column('gender', sa.String(), nullable=True),
        sa.Column('born', sa.DateTime(), nullable=True),
        sa.Column('url', sa.String(), nullable=True),
        sa.Column('weight', sa.Integer(), nullable=True),
        sa.Column('height', sa.Integer(), nullable=True),
        sa.Column('hand', sa.String(), nullable=True),
        sa.PrimaryKeyConstraint('id'),
        sa.UniqueConstraint('name')
    )
    op.create_table(
        'matches',
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('code', sa.String(), nullable=True),
        sa.Column('round', sa.String(), nullable=False),
        sa.Column('gender', sa.String(), nullable=False),
        sa.Column('event_id', sa.Integer(), nullable=True),
        sa.Column('date', sa.DateTime(), nullable=True),
        sa.Column('surface', sa.String(), nullable=True),
        sa.Column('player1_ranking', sa.Integer(), nullable=True),
        sa.Column('player2_ranking', sa.Integer(), nullable=True),
        sa.Column('player1_id', sa.Integer(), nullable=True),
        sa.Column('player2_id', sa.Integer(), nullable=True),
        sa.ForeignKeyConstraint(['event_id'], ['events.id'], ),
        sa.ForeignKeyConstraint(['player1_id'], ['players.id'], ),
        sa.ForeignKeyConstraint(['player2_id'], ['players.id'], ),
        sa.PrimaryKeyConstraint('id')
    )
    op.create_table(
        'results',
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('match_id', sa.Integer(), nullable=True),
        sa.Column('winner_id', sa.Integer(), nullable=True),
        sa.Column('loser_id', sa.Integer(), nullable=True),
        sa.Column('score', sa.String(), nullable=False),
        sa.Column('odds', sa.String(), nullable=True),
        sa.ForeignKeyConstraint(['loser_id'], ['players.id'], ),
        sa.ForeignKeyConstraint(['match_id'], ['matches.id'], ),
        sa.ForeignKeyConstraint(['winner_id'], ['players.id'], ),
        sa.PrimaryKeyConstraint('id')
    )


def downgrade():
    op.drop_table('results')
    op.drop_table('matches')
    op.drop_table('players')
    op.drop_table('events')
