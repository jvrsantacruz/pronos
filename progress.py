import os

from pronos.db import *
from explore import make_query

url = os.getenv('URL') or 'sqlite:///bench.sqlite'
create_all(url)
session = make_session(url)

import time
import datetime


counts = {}
initials = {}
entities = {
    'matches': make_query(),
    'players': session.query(Player)\
    .filter(Player.born != None, Player.ranking != None),
}

start = datetime.datetime.now()

def per_minute(count):
    return count / (datetime.datetime.now() - start).total_seconds() * 60


while True:
    for entity in sorted(entities):
        previous = counts.get(entity, 0)
        count = entities[entity].count()
        print("{}: {} {} with full info ({} more)"
              .format(datetime.datetime.now(), count, entity, count - previous))
        if entity in initials:
            print("Total {:.2f} {} per min".format(
                per_minute(count - initials[entity]), entity)
            )
        else:
            initials[entity] = count
        counts[entity] = count

    print()
    time.sleep(30)
